angular
		.module("app")
		.controller(
				"produtosController",
				function($scope, $http) {
					$scope.pageToGet = 0;

					$scope.state = 'busy';

					$scope.lastAction = '';

					$scope.url = "/Nex2You/protected/produtos/";

					$scope.errorOnSubmit = false;
					$scope.errorIllegalAccess = false;
					$scope.displayMessageToUser = false;
					$scope.displayValidationError = false;
					$scope.displaySearchMessage = false;
					$scope.displaySearchButton = false;
					$scope.displayCreateContactButton = false;

					$scope.contact = {}

					$scope.searchFor = ""

					$scope.getCategorias = function() {
						$http.get($scope.url + 'listCategorias').success(
								function(data, status, headers, config) {
									$scope.listaCategorias = data;
								}).error(
								function(data, status, header, config) {
									$scope.state = 'error';
									$scope.displayCreateContactButton = false;
								});
					};

					$scope.getContactList = function() {
						var url = $scope.url;
						$scope.lastAction = 'list';

						$scope.startDialogAjaxRequest();

						var config = {
							params : {
								page : $scope.pageToGet
							}
						};
						$http.get(url, config).success(function(data) {
							$scope.finishAjaxCallOnSuccess(data, null, false);
						}).error(function() {
							$scope.state = 'error';
							$scope.displayCreateContactButton = false;
						});
					}

					$scope.populateTable = function(data) {
						if (data.pagesCount > 0) {
							$scope.state = 'list';

							$scope.page = {
								source : data.contacts,
								currentPage : $scope.pageToGet,
								pagesCount : data.pagesCount,
								totalContacts : data.totalContacts
							};

							if ($scope.page.pagesCount <= $scope.page.currentPage) {
								$scope.pageToGet = $scope.page.pagesCount - 1;
								$scope.page.currentPage = $scope.page.pagesCount - 1;
							}

							$scope.displayCreateContactButton = true;
							$scope.displaySearchButton = true;
						} else {
							$scope.state = 'noresult';
							$scope.displayCreateContactButton = true;

							if (!$scope.searchFor) {
								$scope.displaySearchButton = false;
							}
						}

						if (data.actionMessage || data.searchMessage) {
							$scope.displayMessageToUser = $scope.lastAction != 'search';

							$scope.page.actionMessage = data.actionMessage;
							$scope.page.searchMessage = data.searchMessage;
						} else {
							$scope.displayMessageToUser = false;
						}
					}

					$scope.changePage = function(page) {
						$scope.pageToGet = page;

						if ($scope.searchFor) {
							$scope.searchContact($scope.searchFor, true);
						} else {
							$scope.getContactList();
						}
					};

					$scope.exit = function(modalId) {
						$(modalId).modal('hide');

						$scope.contact = {};
						$scope.errorOnSubmit = false;
						$scope.errorIllegalAccess = false;
						$scope.displayValidationError = false;
					}

					$scope.finishAjaxCallOnSuccess = function(data, modalId,
							isPagination) {
						$scope.populateTable(data);
						$("#loadingModal").modal('hide');

						if (!isPagination) {
							if (modalId) {
								$scope.exit(modalId);
							}
						}

						$scope.lastAction = '';
					}

					$scope.startDialogAjaxRequest = function() {
						$scope.displayValidationError = false;
						$("#loadingModal").modal('show');
						$scope.previousState = $scope.state;
						$scope.state = 'busy';
					}

					$scope.handleErrorInDialogs = function(status) {
						$("#loadingModal").modal('hide');
						$scope.state = $scope.previousState;

						// illegal access
						if (status == 403) {
							$scope.errorIllegalAccess = true;
							return;
						}

						$scope.errorOnSubmit = true;
						$scope.lastAction = '';
					}

					$scope.addSearchParametersIfNeeded = function(config,
							isPagination) {
						if (!config.params) {
							config.params = {};
						}

						config.params.page = $scope.pageToGet;

						if ($scope.searchFor) {
							config.params.searchFor = $scope.searchFor;
						}
					}

					$scope.resetContact = function() {
						$scope.contact = {};
					};

					$scope.createContact = function(newContactForm) {
						$scope.contact.precoVenda = $scope.contact.precoVenda.replace(',','.');
						$scope.contact.precoCompra = $scope.contact.precoCompra.replace(',','.');
						if (!newContactForm.$valid) {
							$scope.displayValidationError = true;

							return;
						}

						console.log(newContactForm.contact);

						$scope.lastAction = 'create';

						var url = $scope.url;

						var config = {
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
							}
						};

						$scope.addSearchParametersIfNeeded(config, false);

						$scope.startDialogAjaxRequest();

						$http({
							method : 'POST',
							url : url,
							headers : {
								'Content-Type' : 'application/json'
							},
							data : $scope.contact
						}).success(
								function(data) {
									$scope.finishAjaxCallOnSuccess(data,
											"#addContactsModal", false);
								}).error(function(data) {
							$scope.handleErrorInDialogs(status);
						});
					};

					$scope.selectedContact = function(contact) {
						var selectedContact = angular.copy(contact);
						$scope.contact = selectedContact;
					}

					$scope.deleteImage = function(id) {
						$scope.lastAction = 'delete';
						var url = $scope.url + "deleteProdutoImagem/" + id
								+ "," + $scope.contact.id;

						var params = {
							searchFor : $scope.searchFor,
							page : $scope.pageToGet
						};

						$http({
							method : 'DELETE',
							url : url,
							params : params
						}).success(function(data) {
							$scope.listaImagensProduto = data;
						}).error(function(data, status, headers, config) {
							$scope.handleErrorInDialogs(status);
						});
					};

					$scope.buscarImagensDepoimentos = function() {
						$http.get($scope.url + 'getImagensProduto').success(
								function(data, status, headers, config) {
									$scope.listaImagensProduto = data;
								}).error(
								function(data, status, header, config) {
									$scope.state = 'error';
									$scope.displayCreateContactButton = false;
								});
					};

					$scope.selectedContactImage = function(contact) {
						var selectedContact = angular.copy(contact);
						$scope.contact = selectedContact;
						$http({
							method : 'POST',
							url : $scope.url + 'idImage',
							headers : {
								'Content-Type' : 'application/json'
							},
							data : $scope.contact
						}).success(
								function(data) {
									$scope.finishAjaxCallOnSuccess(data,
											"#addContactsModal", false);
								}).error(function(data) {
							$scope.handleErrorInDialogs(status);
						});

					}

					$scope.updateContact = function(updateContactForm) {

						if (!updateContactForm.$valid) {
							$scope.displayValidationError = true;

							return;
						}

						$scope.lastAction = 'update';

						var url = $scope.url + $scope.contact.id;

						$scope.startDialogAjaxRequest();

						var config = {}

						$scope.addSearchParametersIfNeeded(config, false);

						$http.put(url, $scope.contact, config).success(
								function(data) {
									$scope.finishAjaxCallOnSuccess(data,
											"#updateContactsModal", false);
								}).error(
								function(data, status, headers, config) {
									$scope.handleErrorInDialogs(status);
								});
					};

					$scope.searchContact = function(searchContactForm,
							isPagination) {
						if (!($scope.searchFor) && (!searchContactForm.$valid)) {
							$scope.displayValidationError = true;
							return;
						}

						$scope.lastAction = 'search';

						var url = $scope.url + $scope.searchFor;

						$scope.startDialogAjaxRequest();

						var config = {};

						if ($scope.searchFor) {
							$scope.addSearchParametersIfNeeded(config,
									isPagination);
						}

						$http.get(url, config).success(
								function(data) {
									$scope.finishAjaxCallOnSuccess(data,
											"#searchContactsModal",
											isPagination);
									$scope.displaySearchMessage = true;
								}).error(
								function(data, status, headers, config) {
									$scope.handleErrorInDialogs(status);
								});
					};

					$scope.deleteContact = function() {
						$scope.lastAction = 'delete';

						var url = $scope.url + $scope.contact.id;

						$scope.startDialogAjaxRequest();

						var params = {
							searchFor : $scope.searchFor,
							page : $scope.pageToGet
						};

						$http({
							method : 'DELETE',
							url : url,
							params : params
						}).success(
								function(data) {
									$scope.resetContact();
									$scope.finishAjaxCallOnSuccess(data,
											"#deleteContactsModal", false);
								}).error(
								function(data, status, headers, config) {
									$scope.handleErrorInDialogs(status);
								});
					};

					$scope.resetSearch = function() {
						$scope.searchFor = "";
						$scope.pageToGet = 0;
						$scope.getContactList();
						$scope.displaySearchMessage = false;
					}

					$scope.getContactList();
				});