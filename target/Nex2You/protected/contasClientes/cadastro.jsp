<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div id="addContactsModal"
	class="modal hide fade in centering insertAndUpdateDialogsPerfis"
	role="dialog" aria-labelledby="addContactsModalLabel"
	aria-hidden="true">
	<div class="modal-header">
		<h3 id="addContactsModalLabel" class="displayInLine">
			<spring:message code="create" />
			&nbsp;
			<spring:message code="pedido" />
		</h3>
	</div>
	<div class="modal-body">
		<form name="newContactForm" novalidate>
			<div class="pull-left" style="width: 100%">
				<!-- Nome -->
				<div style="float: left">
					<div class="input-append">
						<label>* <spring:message code="pedidos.cliente.nome" />:
						</label>
					</div>
					<br />
					<div class="input-append">
						<input type="text" required="required" autofocus
							ng-model="contact.cliente.nome" name="name"
							placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='pedido.nome'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='pedido'/>  " />
					</div>
					<br />
					<div class="input-append">
						<label> <span class="alert alert-error"
							ng-show="displayValidationError && updateContactForm.name.$error.required">
								<spring:message code="required" />
						</span>
						</label>
					</div>
				</div>
			</div>
			<div id="gridContainer"
				ng-class="{'': state == 'list', 'none': state != 'list'}">
				<div class="input-append">
					<label>* <spring:message code="itens" />:
					</label>
				</div>
				<div align="left">
					<a href="#addItemPedidoModal" role="button"
						ng-click="resetItemPedido();"
						title="<spring:message code='create'/>&nbsp;<spring:message code='pedidos'/>"
						class="btn btn-inverse" data-toggle="modal"> <i
						class="icon-plus"></i> &nbsp;&nbsp;<spring:message code="create" />&nbsp;<spring:message
							code="pedidos" />
					</a>
				</div>
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th scope="col"><spring:message code="pedidos.quantidade" /></th>
							<th scope="col"><spring:message code="pedidos.produto.nome" /></th>
							<th scope="col"><spring:message code="pedidos.valor" /></th>
							<th scope="col"></th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="pedidos in listaItens">
							<td class="tdContactsCentered">{{pedidos.quantidade}}</td>
							<td class="tdContactsCentered">{{pedidos.produto.nome}}</td>
							<td class="tdContactsCentered">{{pedidos.valor}}</td>
							<td class="width15">
								<div class="text-center">
									<input type="hidden" value="{{pedidos.id}}" /> <a
										href="#updateItemPedidoModal"
										ng-click="selectedItemPedido(pedidos);getProdutos();"
										role="button"
										title="<spring:message code="update"/>&nbsp;<spring:message code="pedidos"/>"
										class="btn btn-inverse" data-toggle="modal"> <i
										class="icon-pencil"></i>
									</a> <a href="#deleteItemPedidoModal"
										ng-click="selectedItemPedido(pedidos);" role="button"
										title="<spring:message code="delete"/>&nbsp;<spring:message code="pedidos"/>"
										class="btn btn-inverse" data-toggle="modal"> <i
										class="icon-minus"></i>
									</a>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div style="width: 100%">
				<div class="pull-left">
					<input type="submit" class="btn btn-inverse"
						ng-click="createContact(newContactForm);"
						value='<spring:message code="create"/>' />
					<button class="btn btn-inverse" data-dismiss="modal"
						ng-click="exit('#addContactsModal');" aria-hidden="true">
						<spring:message code="cancel" />
					</button>
				</div>
			</div>
		</form>
	</div>
	<span class="alert alert-error dialogErrorMessage"
		ng-show="errorOnSubmit"> <spring:message code="request.error" />
	</span>
</div>

<div id="updateContactsModal"
	class="modal hide fade in centering insertAndUpdateDialogsPerfis"
	role="dialog" aria-labelledby="updateContactsModalLabel"
	aria-hidden="true">
	<div class="modal-header">
		<h3 id="updateContactsModalLabel" class="displayInLine">
			<spring:message code="update" />
			&nbsp;
			<spring:message code="pedido" />
		</h3>
	</div>
	<div class="modal-body">
		<form name="updateContactForm" novalidate>
			<input type="hidden" ng-model="contact.id" name="id"
				value="{{contact.id}}" />
			<div class="pull-left" style="width: 100%">
				<!-- Nome -->
				<div style="float: left">
					<div class="input-append">
						<label>* <spring:message code="pedidos.cliente.nome" />:
						</label>
					</div>
					<br />
					<div class="input-append">
						<input type="text" required="required" autofocus
							ng-model="contact.cliente.nome" name="name"
							placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='pedido.nome'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='pedido'/>  " />
					</div>
					<br />
					<div class="input-append">
						<label> <span class="alert alert-error"
							ng-show="displayValidationError && updateContactForm.name.$error.required">
								<spring:message code="required" />
						</span>
						</label>
					</div>
				</div>
			</div>
			<div id="gridContainer"
				ng-class="{'': state == 'list', 'none': state != 'list'}">
				<div class="input-append">
					<label>* <spring:message code="itens" />:
					</label>
				</div>
				<div align="left">
					<a href="#addItemPedidoModal" role="button"
						ng-click="resetItemPedido();"
						title="<spring:message code='create'/>&nbsp;<spring:message code='pedidos'/>"
						class="btn btn-inverse" data-toggle="modal"> <i
						class="icon-plus"></i> &nbsp;&nbsp;<spring:message code="create" />&nbsp;<spring:message
							code="pedidos" />
					</a>
				</div>
				<table class="table table-bordered table-striped">
					<thead>
						<tr>
							<th scope="col"><spring:message code="pedidos.quantidade" /></th>
							<th scope="col"><spring:message code="pedidos.produto.nome" /></th>
							<th scope="col"><spring:message code="pedidos.valor" /></th>
							<th scope="col"></th>
						</tr>
					</thead>
					<tbody>
						<tr ng-repeat="pedidos in listaItens">
							<td class="tdContactsCentered">{{pedidos.quantidade}}</td>
							<td class="tdContactsCentered">{{pedidos.produto.nome}}</td>
							<td class="tdContactsCentered">{{pedidos.valor}}</td>
							<td class="width15">
								<div class="text-center">
									<input type="hidden" value="{{pedidos.id}}" /> <a
										href="#updateItemPedidoModal"
										ng-click="selectedItemPedido(pedidos);getProdutos();"
										role="button"
										title="<spring:message code="update"/>&nbsp;<spring:message code="pedidos"/>"
										class="btn btn-inverse" data-toggle="modal"> <i
										class="icon-pencil"></i>
									</a> <a href="#deleteItemPedidoModal"
										ng-click="selectedItemPedido(pedidos);" role="button"
										title="<spring:message code="delete"/>&nbsp;<spring:message code="pedidos"/>"
										class="btn btn-inverse" data-toggle="modal"> <i
										class="icon-minus"></i>
									</a>
								</div>
							</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div style="width: 100%">
				<div class="pull-left">
					<input type="submit" class="btn btn-inverse"
						ng-click="updateContact(updateContactForm);"
						value='<spring:message code="update"/>' />
					<button class="btn btn-inverse" data-dismiss="modal"
						ng-click="exit('#updateContactsModal');" aria-hidden="true">
						<spring:message code="cancel" />
					</button>
				</div>
			</div>
		</form>
	</div>
	<span class="alert alert-error dialogErrorMessage"
		ng-show="errorOnSubmit"> <spring:message code="request.error" />
	</span>
</div>

<div id="deleteContactsModal" class="modal hide fade in centering"
	role="dialog" aria-labelledby="searchContactsModalLabel"
	aria-hidden="true">
	<div class="modal-header">
		<h3 id="deleteContactsModalLabel" class="displayInLine">
			<spring:message code="delete" />
			&nbsp;
			<spring:message code="sindicante" />
		</h3>
	</div>
	<div class="modal-body">
		<form name="deleteContactForm" novalidate>
			<p>
				<spring:message code="delete.confirm" />
				:&nbsp;{{contact.cliente.nome}}?
			</p>

			<input type="submit" class="btn btn-inverse"
				ng-click="deleteContact();" value='<spring:message code="delete"/>' />
			<button class="btn btn-inverse" data-dismiss="modal"
				ng-click="exit('#deleteContactsModal');" aria-hidden="true">
				<spring:message code="cancel" />
			</button>
		</form>
	</div>
	<span class="alert alert-error dialogErrorMessage"
		ng-show="errorOnSubmit"> <spring:message code="request.error" />
	</span> <span class="alert alert-error dialogErrorMessage"
		ng-show="errorIllegalAccess"> <spring:message
			code="request.illegal.access" />
	</span>
</div>

<div id="searchContactsModal" class="modal hide fade in centering"
	role="dialog" aria-labelledby="searchContactsModalLabel"
	aria-hidden="true">
	<div class="modal-header">
		<h3 id="searchContactsModalLabel" class="displayInLine">
			<spring:message code="search" />
		</h3>
	</div>
	<div class="modal-body">
		<form name="searchContactForm" novalidate>
			<label><spring:message code="search.for" /></label>

			<div>
				<br />
				<div class="input-append">
					<input type="text" autofocus ng-model="searchFor" name="searchFor"
						placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='pedido.nome'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='pedido'/>  " />
				</div>
				<div class="input-append displayInLine">
					<label class="displayInLine"> <span
						class="alert alert-error"
						ng-show="displayValidationError && searchContactForm.searchFor.$error.required">
							<spring:message code="required" />
					</span>
					</label>
				</div>
			</div>
			<input type="submit" class="btn btn-inverse"
				ng-click="searchContact(searchContactForm, false);"
				value='<spring:message code="search"/>' />
			<button class="btn btn-inverse" data-dismiss="modal"
				ng-click="exit('#searchContactsModal');" aria-hidden="true">
				<spring:message code="cancel" />
			</button>
		</form>
	</div>
	<span class="alert alert-error dialogErrorMessage"
		ng-show="errorOnSubmit"> <spring:message code="request.error" />
	</span>
</div>
<!-- Novo Item -->
<div id="addItemPedidoModal"
	class="modal hide fade in centering insertAndUpdateDialogsBancos"
	role="dialog" aria-labelledby="addContactsModalLabel"
	aria-hidden="true">
	<div class="modal-header">
		<h3 id="addContactsModalLabel" class="displayInLine">
			<spring:message code="create" />
			&nbsp;
			<spring:message code="item" />
		</h3>
	</div>
	<div class="modal-body">
		<form name="newContactForm" novalidate>
			<div class="pull-left" style="width: 100%">
				<!-- Categoria -->
				<div class="input-append" data-ng-init="getCategorias()">
					<div class="input-append">
						<label> <spring:message code="pedidos.categoria" />
						</label>
					</div>
					<br />
					<div class="input-append">
						<select ng-model="itemPedido.produto.categoria.id"
							ng-change="getProdutos();">
							<option data-ng-repeat="item in listaCategorias"
								value="{{item.id}}"
								ng-selected="itemPedido.produto.categoria.id==item.id">{{item.nome}}</option>
						</select>
					</div>
				</div>
				<!-- Produto -->
				<div class="input-append">
					<div class="input-append">
						<label> <spring:message code="pedidos.produto.nome" />
						</label>
					</div>
					<br />
					<div class="input-append">
						<select ng-model="itemPedido.produto.id"
							ng-change="updateDadosProduto();">
							<option data-ng-repeat="item in listaProdutos"
								value="{{item.id}}" ng-selected="itemPedido.produto.id==item.id">{{item.nome}}</option>
						</select>
					</div>
				</div>
				<!-- Quantidade -->
				<div style="float: left">
					<div class="input-append">
						<label>* <spring:message code="pedidos.quantidade" />&nbsp;<spring:message
								code="pedidos.estoqueAtual" />
							<spring:message code="{{estoque}}" />):
						</label>
					</div>
					<br />
					<div class="input-append">
						<input type="number" autofocus ng-model="itemPedido.quantidade"
							name="name" required="required" min="0" max="{{estoque}}"
							ng-change="atualizaValor();"
							placeholder="<spring:message code='contact'/>&nbsp;<spring:message code='sindicancia.sinistro'/> " />
					</div>
					<br />
					<div class="input-append">
						<label> <span class="alert alert-error"
							ng-show="displayValidationError && updateContactForm.name.$error.required">
								<spring:message code="required" />
						</span>
						</label>
					</div>
				</div>
				<!-- Valor -->
				<div style="float: left">
					<div class="input-append">
						<label>* <spring:message code="pedidos.valor" />&nbsp;<spring:message
								code="pedidos.valorUnitario" />&nbsp;<spring:message
								code="{{valorUnitario}}" />):
						</label>
					</div>
					<br />
					<div class="input-append">
						<input type="text" autofocus ng-model="itemPedido.valor"
							disabled="disabled" name="name" required="required"
							placeholder="<spring:message code='contact'/>&nbsp;<spring:message code='sindicancia.sinistro'/> " />
					</div>
					<br />
					<div class="input-append">
						<label> <span class="alert alert-error"
							ng-show="displayValidationError && updateContactForm.name.$error.required">
								<spring:message code="required" />
						</span>
						</label>
					</div>
				</div>
			</div>
			<div style="width: 100%">
				<div class="pull-left">
					<input type="submit" class="btn btn-inverse"
						ng-click="createItemPedido(newContactForm);"
						value='<spring:message code="create"/>' />
					<button class="btn btn-inverse" data-dismiss="modal"
						ng-click="exit('#addItemPedidoModal');" aria-hidden="true">
						<spring:message code="cancel" />
					</button>
				</div>
			</div>
		</form>
	</div>
	<span class="alert alert-error dialogErrorMessage"
		ng-show="errorOnSubmit"> <spring:message code="request.error" />
	</span>
</div>
<!-- Editar Item -->
<div id="updateItemPedidoModal"
	class="modal hide fade in centering insertAndUpdateDialogsBancos"
	role="dialog" aria-labelledby="addContactsModalLabel"
	aria-hidden="true">
	<div class="modal-header">
		<h3 id="addContactsModalLabel" class="displayInLine">
			<spring:message code="update" />
			&nbsp;
			<spring:message code="itemPedido" />
		</h3>
	</div>
	<div class="modal-body">
		<form name="newContactForm" novalidate>
			<div class="pull-left" style="width: 100%">
				<input type="hidden" ng-model="itemPedido.id" name="id"
					value="{{itemPedido.id}}" />
				<!-- Categoria -->
				<div class="input-append" data-ng-init="getCategorias()">
					<div class="input-append">
						<label> <spring:message code="pedidos.categoria" />
						</label>
					</div>
					<br />
					<div class="input-append">
						<select ng-model="itemPedido.produto.categoria.id"
							ng-change="getProdutos();">
							<option data-ng-repeat="item in listaCategorias"
								value="{{item.id}}"
								ng-selected="itemPedido.produto.categoria.id==item.id">{{item.nome}}</option>
						</select>
					</div>
				</div>
				<!-- Produto -->
				<div class="input-append">
					<div class="input-append">
						<label> <spring:message code="pedidos.produto.nome" />
						</label>
					</div>
					<br />
					<div class="input-append">
						<select ng-model="itemPedido.produto.id">
							<option data-ng-repeat="item in listaProdutos"
								value="{{item.id}}" ng-selected="itemPedido.produto.id==item.id">{{item.nome}}</option>
						</select>
					</div>
				</div>
				<!-- Quantidade -->
				<div style="float: left">
					<div class="input-append">
						<label>* <spring:message code="pedidos.quantidade" />:
						</label>
					</div>
					<br />
					<div class="input-append">
						<input type="number" autofocus ng-model="itemPedido.quantidade"
							name="name" required="required" min="0"
							ng-change="atualizaValor();"
							placeholder="<spring:message code='contact'/>&nbsp;<spring:message code='sindicancia.sinistro'/> " />
					</div>
					<br />
					<div class="input-append">
						<label> <span class="alert alert-error"
							ng-show="displayValidationError && updateContactForm.name.$error.required">
								<spring:message code="required" />
						</span>
						</label>
					</div>
				</div>
				<!-- Valor -->
				<div style="float: left">
					<div class="input-append">
						<label>* <spring:message code="pedidos.valor" />:
						</label>
					</div>
					<br />
					<div class="input-append">
						<input type="text" autofocus ng-model="itemPedido.valor"
							disabled="disabled" name="name" required="required"
							placeholder="<spring:message code='contact'/>&nbsp;<spring:message code='sindicancia.sinistro'/> " />
					</div>
					<br />
					<div class="input-append">
						<label> <span class="alert alert-error"
							ng-show="displayValidationError && updateContactForm.name.$error.required">
								<spring:message code="required" />
						</span>
						</label>
					</div>
				</div>
			</div>
			<div style="width: 100%">
				<div class="pull-left">
					<input type="submit" class="btn btn-inverse"
						ng-click="updateItemPedido(newContactForm);"
						value='<spring:message code="update"/>' />
					<button class="btn btn-inverse" data-dismiss="modal"
						ng-click="exit('#updateItemPedidoModal');" aria-hidden="true">
						<spring:message code="cancel" />
					</button>
				</div>
			</div>
		</form>
	</div>
	<span class="alert alert-error dialogErrorMessage"
		ng-show="errorOnSubmit"> <spring:message code="request.error" />
	</span>
</div>
<div id="deleteItemPedidoModal" class="modal hide fade in centering"
	role="dialog" aria-labelledby="searchContactsModalLabel"
	aria-hidden="true">
	<div class="modal-header">
		<h3 id="deleteContactsModalLabel" class="displayInLine">
			<spring:message code="delete" />
			&nbsp;
			<spring:message code="itemPedido" />
		</h3>
	</div>
	<div class="modal-body">
		<form name="deleteContactForm" novalidate>
			<p>
				<spring:message code="delete.confirm" />
				:&nbsp;{{itemPedido.produto.nome}}?
			</p>

			<input type="submit" class="btn btn-inverse" ng-click="deleteItem();"
				value='<spring:message code="delete"/>' />
			<button class="btn btn-inverse" data-dismiss="modal"
				ng-click="exit('#deleteItemPedidoModal');" aria-hidden="true">
				<spring:message code="cancel" />
			</button>
		</form>
	</div>
	<span class="alert alert-error dialogErrorMessage"
		ng-show="errorOnSubmit"> <spring:message code="request.error" />
	</span> <span class="alert alert-error dialogErrorMessage"
		ng-show="errorIllegalAccess"> <spring:message
			code="request.illegal.access" />
	</span>
</div>