<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row-fluid" ng-controller="contasClientesController">
	<h2>
		<p class="text-center">
			<spring:message code='pedidos' />
			<a href="#searchContactsModal" id="contactsHeaderButton"
				role="button"
				ng-class="{'': displaySearchButton == true, 'none': displaySearchButton == false}"
				title="<spring:message code="search"/>&nbsp;<spring:message code="pedidos"/>"
				class="btn btn-inverse" data-toggle="modal"> <i
				class="icon-search"></i>
			</a>
		</p>
	</h2>
	<h4>
		<div ng-class="{'': state == 'list', 'none': state != 'list'}">
			<p class="text-center">
				<spring:message code="message.total.records.found" />
				:&nbsp;{{page.totalContacts}}
			</p>
		</div>
	</h4>

	<div>
		<div id="loadingModal" class="modal hide fade in centering"
			role="dialog" aria-labelledby="deleteContactsModalLabel"
			aria-hidden="true">
			<div id="divLoadingIcon" class="text-center">
				<div class="icon-align-center loading"></div>
			</div>
		</div>

		<div
			ng-class="{'alert badge-inverse': displaySearchMessage == true, 'none': displaySearchMessage == false}">
			<h4>
				<p class="messageToUser">
					<i class="icon-info-sign"></i>&nbsp;{{page.searchMessage}}
				</p>
			</h4>
			<a href="#" role="button" ng-click="resetSearch();"
				ng-class="{'': displaySearchMessage == true, 'none': displaySearchMessage == false}"
				title="<spring:message code='search.reset'/>"
				class="btn btn-inverse" data-toggle="modal"> <i
				class="icon-remove"></i> <spring:message code="search.reset" />
			</a>
		</div>

		<div
			ng-class="{'alert badge-inverse': displayMessageToUser == true, 'none': displayMessageToUser == false}">
			<h4 class="displayInLine">
				<p class="messageToUser displayInLine">
					<i class="icon-info-sign"></i>&nbsp;{{page.actionMessage}}
				</p>
			</h4>
		</div>

		<div
			ng-class="{'alert alert-block alert-error': state == 'error', 'none': state != 'error'}">
			<h4>
				<i class="icon-info-sign"></i>
				<spring:message code="error.generic.header" />
			</h4>
			<br />

			<p>
				<spring:message code="error.generic.text" />
			</p>
		</div>

		<div
			ng-class="{'alert alert-info': state == 'noresult', 'none': state != 'noresult'}">
			<h4>
				<i class="icon-info-sign"></i>
				<spring:message code="contacts.emptyData" />
			</h4>
			<br />

			<p>
				<spring:message code="contacts.emptyData.text" />
			</p>
		</div>

		<div align="left">
			<a href="#addContactsModal" role="button" ng-click="resetContact();"
				title="<spring:message code='create'/>&nbsp;<spring:message code='pedidos'/>"
				class="btn btn-inverse" data-toggle="modal"> <i
				class="icon-plus"></i> &nbsp;&nbsp;<spring:message code="create" />&nbsp;<spring:message
					code="pedidos" />
			</a>
		</div>
		<div id="gridContainer"
			ng-class="{'': state == 'list', 'none': state != 'list'}">
			<table class="table table-bordered table-striped">
				<thead>
					<tr>
						<th scope="col"><spring:message code="pedidos.cliente.nome" /></th>
						<th scope="col"><spring:message code="pedidos.valor" /></th>
						<th scope="col"></th>
					</tr>
				</thead>
				<tbody>
					<tr ng-repeat="pedidos in page.source">
						<td class="tdContactsCentered">{{pedidos.cliente.nome}}</td>
						<td class="tdContactsCentered">{{pedidos.valor}}</td>
						<td class="width15">
							<div class="text-center">
								<input type="hidden" value="{{pedidos.id}}" /> <a
									href="#updateContactsModal"
									ng-click="selectedContact(pedidos);getItens();" role="button"
									title="<spring:message code="update"/>&nbsp;<spring:message code="pedidos"/>"
									class="btn btn-inverse" data-toggle="modal"> <i
									class="icon-pencil"></i>
								</a> <a href="#deleteContactsModal"
									ng-click="selectedContact(pedidos);" role="button"
									title="<spring:message code="delete"/>&nbsp;<spring:message code="pedidos"/>"
									class="btn btn-inverse" data-toggle="modal"> <i
									class="icon-minus"></i>
								</a>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>

		<jsp:include page="cadastro.jsp" />

	</div>
</div>

<script src="<c:url value="/resources/js/pages/contasClientes.js" />"></script>