package br.com.nex2you.vo;

import java.util.List;

import br.com.nex2you.model.ContaCliente;

public class ContaClienteListVO {
	private int pagesCount;
	private long totalContacts;

	private String actionMessage;
	private String searchMessage;

	private List<ContaCliente> contacts;

	public ContaClienteListVO() {
	}

	public ContaClienteListVO(int pages, long totalContacts, List<ContaCliente> contacts) {
		this.pagesCount = pages;
		this.contacts = contacts;
		this.totalContacts = totalContacts;
	}

	public int getPagesCount() {
		return pagesCount;
	}

	public void setPagesCount(int pagesCount) {
		this.pagesCount = pagesCount;
	}

	public List<ContaCliente> getContacts() {
		return contacts;
	}

	public void setContacts(List<ContaCliente> contacts) {
		this.contacts = contacts;
	}

	public long getTotalContacts() {
		return totalContacts;
	}

	public void setTotalContacts(long totalContacts) {
		this.totalContacts = totalContacts;
	}

	public String getActionMessage() {
		return actionMessage;
	}

	public void setActionMessage(String actionMessage) {
		this.actionMessage = actionMessage;
	}

	public String getSearchMessage() {
		return searchMessage;
	}

	public void setSearchMessage(String searchMessage) {
		this.searchMessage = searchMessage;
	}
}