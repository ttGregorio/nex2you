package br.com.nex2you.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.nex2you.model.Empresa;
import br.com.nex2you.model.Usuario;
import br.com.nex2you.repository.UserRepository;
import br.com.nex2you.repository.UsuarioRepository;
import br.com.nex2you.vo.UsuarioListVO;

@Service
@Transactional
public class UsuarioService {

	@Autowired
	private UsuarioRepository usuarioRepository;

	@Autowired
	private UserRepository userRepository;

	@Transactional(readOnly = true)
	public UsuarioListVO findAll(int page, int maxResults, boolean tipo,
			Empresa empresa) {
		Page<Usuario> result = executeQueryFindAllAtivo(page, maxResults, tipo,
				empresa);

		if (shouldExecuteSameQueryInLastPage(page, result)) {
			int lastPage = result.getTotalPages() - 1;
			result = executeQueryFindAllAtivo(lastPage, maxResults, tipo,
					empresa);
		}

		return buildResult(result);
	}

	public void save(Usuario usuario) {
		usuarioRepository.save(usuario);
	}

	public void delete(int usuarioId) {
		Usuario usuario = usuarioRepository.findOne(usuarioId);
		usuario.setAtivo(false);
		usuarioRepository.save(usuario);
	}

	@Transactional(readOnly = true)
	public UsuarioListVO findByNomeLike(int page, int maxResults, String name,
			Empresa empresa) {
		Page<Usuario> result = executeQueryFindByNome(page, maxResults, name,
				empresa);

		if (shouldExecuteSameQueryInLastPage(page, result)) {
			int lastPage = result.getTotalPages() - 1;
			result = executeQueryFindByNome(lastPage, maxResults, name, empresa);
		}

		return buildResult(result);
	}

	private boolean shouldExecuteSameQueryInLastPage(int page,
			Page<Usuario> result) {
		return isUserAfterOrOnLastPage(page, result)
				&& hasDataInDataBase(result);
	}

	private Page<Usuario> executeQueryFindAllAtivo(int page, int maxResults,
			boolean tipo, Empresa empresa) {
		final PageRequest pageRequest = new PageRequest(page, maxResults,
				sortByNomeASC());

		return usuarioRepository.findByEmpresaAndAtivo(pageRequest, empresa,
				true);
	}

	private Sort sortByNomeASC() {
		return new Sort(Sort.Direction.ASC, "nome");
	}

	private UsuarioListVO buildResult(Page<Usuario> result) {
		return new UsuarioListVO(result.getTotalPages(),
				result.getTotalElements(), result.getContent());
	}

	private Page<Usuario> executeQueryFindByNome(int page, int maxResults,
			String nome, Empresa empresa) {
		final PageRequest pageRequest = new PageRequest(page, maxResults,
				sortByNomeASC());

		return usuarioRepository.findByNomeLikeAndEmpresa(pageRequest, "%"
				+ nome + "%", empresa);
	}

	private boolean isUserAfterOrOnLastPage(int page, Page<Usuario> result) {
		return page >= result.getTotalPages() - 1;
	}

	private boolean hasDataInDataBase(Page<Usuario> result) {
		return result.getTotalElements() > 0;
	}

	public Usuario findByLogin(String login) {
		return usuarioRepository.findByLogin(login);
	}

	public Usuario findOne(int coordenador) {
		return usuarioRepository.findOne(coordenador);
	}

	public Usuario findByEmail(String email) {
		return usuarioRepository.findByEmail(email);
	}
}
