package br.com.nex2you.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.nex2you.model.Cliente;
import br.com.nex2you.model.ContaCliente;
import br.com.nex2you.model.Empresa;
import br.com.nex2you.repository.ClienteRepository;
import br.com.nex2you.repository.ContaClienteRepository;
import br.com.nex2you.vo.ContaClienteListVO;

@Service
public class ContaClienteService {

	@Autowired
	private ContaClienteRepository contaClienteRepository;

	@Autowired
	private ClienteRepository clienteRepository;

	@Transactional(readOnly = true)
	public ContaClienteListVO findAll(int page, int maxResults, Empresa empresa) {
		List<ContaCliente> result = executeQueryFindAll(page, maxResults,
				empresa);

		return buildResult(result);
	}

	public ContaCliente findById(int id) {
		return contaClienteRepository.findOne(id);
	}

	public List<ContaCliente> findAll() {
		return (List<ContaCliente>) contaClienteRepository.findAll();
	}

	public void save(ContaCliente usuario) {

		List<Cliente> listaClientes = clienteRepository
				.findByEmpresaAndNomeLike(usuario.getEmpresa(), usuario
						.getCliente().getNome());
		if (listaClientes.size() > 0) {
			usuario.setCliente(listaClientes.get(0));
		} else {
			usuario.getCliente().setEmpresa(usuario.getEmpresa());
			clienteRepository.save(usuario.getCliente());
			listaClientes = clienteRepository.findByEmpresaAndNomeLike(
					usuario.getEmpresa(), usuario.getCliente().getNome());
			usuario.setCliente(listaClientes.get(0));
		}
		usuario.setPedidoAberto(true);
		
		contaClienteRepository.save(usuario);
	}

	public void delete(int usuarioId) {
		contaClienteRepository.delete(usuarioId);
	}

	private List<ContaCliente> executeQueryFindAll(int page, int maxResults,
			Empresa empresa) {
		return contaClienteRepository.findByEmpresaAndPedidoAberto(empresa,
				true);
	}

	private ContaClienteListVO buildResult(List<ContaCliente> result) {
		return new ContaClienteListVO(1, result.size(), result);
	}

	public ContaCliente findByClienteAndEmpresa(Cliente cliente2,
			Empresa empresa) {
		return contaClienteRepository.findByEmpresaAndClienteAndPedidoAberto(empresa, cliente2, true);
	}

	public List<ContaCliente> findByCliente(Cliente cliente) {
		return contaClienteRepository.findByCliente(cliente);
	}

}
