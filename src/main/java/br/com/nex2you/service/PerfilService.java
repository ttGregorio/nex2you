package br.com.nex2you.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.nex2you.model.Empresa;
import br.com.nex2you.model.Perfil;
import br.com.nex2you.repository.PerfilRepository;
import br.com.nex2you.vo.PerfilListVO;

@Service
public class PerfilService {

	@Autowired
	private PerfilRepository perfilRepository;

	@Transactional(readOnly = true)
	public PerfilListVO findAll(int page, int maxResults) {
		Page<Perfil> result = executeQueryFindAll(page, maxResults);

		if (shouldExecuteSameQueryInLastPage(page, result)) {
			int lastPage = result.getTotalPages() - 1;
			result = executeQueryFindAll(lastPage, maxResults);
		}

		return buildResult(result);
	}

	@Transactional(readOnly = true)
	public PerfilListVO findAllAtivo(int page, int maxResults, Empresa empresa) {
		Page<Perfil> result = executeQueryFindAllAtivo(page, maxResults,
				empresa);

		if (shouldExecuteSameQueryInLastPage(page, result)) {
			int lastPage = result.getTotalPages() - 1;
			result = executeQueryFindAllAtivo(lastPage, maxResults, empresa);
		}

		return buildResult(result);
	}

	public Perfil findById(int id) {
		return perfilRepository.findOne(id);
	}

	public List<Perfil> findAll() {
		return (List<Perfil>) perfilRepository.findAll();
	}

	public void save(Perfil usuario) {
		perfilRepository.save(usuario);
	}

	public void delete(int usuarioId) {
		perfilRepository.delete(usuarioId);
	}

	@Transactional(readOnly = true)
	public PerfilListVO findByNomeLike(int page, int maxResults, String name, Empresa empresa) {
		Page<Perfil> result = executeQueryFindByNome(page, maxResults, name,empresa);

		if (shouldExecuteSameQueryInLastPage(page, result)) {
			int lastPage = result.getTotalPages() - 1;
			result = executeQueryFindByNome(lastPage, maxResults, name,empresa);
		}

		return buildResult(result);
	}

	private boolean shouldExecuteSameQueryInLastPage(int page,
			Page<Perfil> result) {
		return isUserAfterOrOnLastPage(page, result)
				&& hasDataInDataBase(result);
	}

	private Page<Perfil> executeQueryFindAll(int page, int maxResults) {
		final PageRequest pageRequest = new PageRequest(page, maxResults,
				sortByNomeASC());

		return perfilRepository.findAll(pageRequest);
	}

	private Page<Perfil> executeQueryFindAllAtivo(int page, int maxResults,
			Empresa empresa) {
		final PageRequest pageRequest = new PageRequest(page, maxResults,
				sortByNomeASC());

		return perfilRepository.findByAtivoAndEmpresa(pageRequest, true,
				empresa);
	}

	private Sort sortByNomeASC() {
		return new Sort(Sort.Direction.ASC, "nome");
	}

	private PerfilListVO buildResult(Page<Perfil> result) {
		return new PerfilListVO(result.getTotalPages(),
				result.getTotalElements(), result.getContent());
	}

	private Page<Perfil> executeQueryFindByNome(int page, int maxResults,
			String nome, Empresa empresa) {
		final PageRequest pageRequest = new PageRequest(page, maxResults,
				sortByNomeASC());

		return perfilRepository.findByNomeLikeAndEmpresa(pageRequest, "%" + nome + "%",empresa);
	}

	private boolean isUserAfterOrOnLastPage(int page, Page<Perfil> result) {
		return page >= result.getTotalPages() - 1;
	}

	private boolean hasDataInDataBase(Page<Perfil> result) {
		return result.getTotalElements() > 0;
	}

	public List<Perfil> findByEmpresa(Empresa empresa) {
		return perfilRepository.findByAtivoAndEmpresa(true, empresa);
	}
}
