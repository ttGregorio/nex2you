package br.com.nex2you.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.nex2you.model.CategoriaEmpresa;
import br.com.nex2you.repository.CategoriaEmpresaRepository;

@Service
public class CategoriaEmpresaService {

	@Autowired
	private CategoriaEmpresaRepository categoriaEmpresaRepository;

	public List<CategoriaEmpresa> findAll() {
		return (List<CategoriaEmpresa>) categoriaEmpresaRepository
				.findByIdGreaterThan(0);
	}

	public CategoriaEmpresa findById(int id) {
		return categoriaEmpresaRepository.findOne(id);
	}
}
