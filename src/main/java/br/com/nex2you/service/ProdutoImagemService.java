package br.com.nex2you.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.nex2you.model.Produto;
import br.com.nex2you.model.ProdutoImagem;
import br.com.nex2you.repository.ProdutoImagemRepository;

@Service
public class ProdutoImagemService {

	@Autowired
	private ProdutoImagemRepository produtoRepository;

	@Transactional(readOnly = true)
	public List<ProdutoImagem> findAll(Produto produto) {
		return produtoRepository.findByProduto(produto);
	}

	public void save(ProdutoImagem usuario) {
		produtoRepository.save(usuario);
	}

	public void delete(int usuarioId) {
		produtoRepository.delete(usuarioId);
	}
}
