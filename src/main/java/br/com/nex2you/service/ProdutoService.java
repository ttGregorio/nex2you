package br.com.nex2you.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.nex2you.model.CategoriaProduto;
import br.com.nex2you.model.Produto;
import br.com.nex2you.model.Empresa;
import br.com.nex2you.repository.ProdutoRepository;
import br.com.nex2you.vo.ProdutoListVO;

@Service
public class ProdutoService {

	@Autowired
	private ProdutoRepository produtoRepository;

	@Transactional(readOnly = true)
	public ProdutoListVO findAll(int page, int maxResults, Empresa empresa) {
		Page<Produto> result = executeQueryFindAll(page, maxResults, empresa);

		if (shouldExecuteSameQueryInLastPage(page, result)) {
			int lastPage = result.getTotalPages() - 1;
			result = executeQueryFindAll(lastPage, maxResults, empresa);
		}

		return buildResult(result);
	}

	public Produto findById(int id) {
		return produtoRepository.findOne(id);
	}

	public List<Produto> findAll() {
		return (List<Produto>) produtoRepository.findAll();
	}

	public List<Produto> findByCategoriaProduto( CategoriaProduto categoriaProduto) {
		return (List<Produto>) produtoRepository.findByCategoria(categoriaProduto);
	}

	public void save(Produto usuario) {
		produtoRepository.save(usuario);
	}

	public void delete(int usuarioId) {
		produtoRepository.delete(usuarioId);
	}

	@Transactional(readOnly = true)
	public ProdutoListVO findByNomeLike(int page, int maxResults, String name,
			Empresa empresa) {
		Page<Produto> result = executeQueryFindByNome(page, maxResults, name,
				empresa);

		if (shouldExecuteSameQueryInLastPage(page, result)) {
			int lastPage = result.getTotalPages() - 1;
			result = executeQueryFindByNome(lastPage, maxResults, name, empresa);
		}

		return buildResult(result);
	}

	private boolean shouldExecuteSameQueryInLastPage(int page,
			Page<Produto> result) {
		return isUserAfterOrOnLastPage(page, result)
				&& hasDataInDataBase(result);
	}

	private Page<Produto> executeQueryFindAll(int page, int maxResults,
			Empresa empresa) {
		final PageRequest pageRequest = new PageRequest(page, maxResults,
				sortByNomeASC());

		return produtoRepository.findByEmpresaAndAtivo(pageRequest, empresa,
				true);
	}

	private Sort sortByNomeASC() {
		return new Sort(Sort.Direction.ASC, "nome");
	}

	private ProdutoListVO buildResult(Page<Produto> result) {
		return new ProdutoListVO(result.getTotalPages(),
				result.getTotalElements(), result.getContent());
	}

	private Page<Produto> executeQueryFindByNome(int page, int maxResults,
			String nome, Empresa empresa) {
		final PageRequest pageRequest = new PageRequest(page, maxResults,
				sortByNomeASC());

		return produtoRepository.findByNomeLikeAndEmpresa(pageRequest, "%"
				+ nome + "%", empresa);
	}

	private boolean isUserAfterOrOnLastPage(int page, Page<Produto> result) {
		return page >= result.getTotalPages() - 1;
	}

	private boolean hasDataInDataBase(Page<Produto> result) {
		return result.getTotalElements() > 0;
	}
}
