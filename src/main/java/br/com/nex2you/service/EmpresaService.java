package br.com.nex2you.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.nex2you.model.Cliente;
import br.com.nex2you.model.Empresa;
import br.com.nex2you.model.Usuario;
import br.com.nex2you.repository.ClienteRepository;
import br.com.nex2you.repository.EmpresaRepository;
import br.com.nex2you.vo.EmpresaListVO;

@Service
public class EmpresaService {

	@Autowired
	private EmpresaRepository empresaRepository;

	@Autowired
	private ClienteRepository clienteRepository;
	
	@Transactional(readOnly = true)
	public EmpresaListVO findAll(int page, int maxResults) {
		Page<Empresa> result = executeQueryFindAll(page, maxResults);

		if (shouldExecuteSameQueryInLastPage(page, result)) {
			int lastPage = result.getTotalPages() - 1;
			result = executeQueryFindAll(lastPage, maxResults);
		}

		return buildResult(result);
	}

	@Transactional(readOnly = true)
	public EmpresaListVO findAllAtivo(int page, int maxResults) {
		Page<Empresa> result = executeQueryFindAllAtivo(page, maxResults);

		if (shouldExecuteSameQueryInLastPage(page, result)) {
			int lastPage = result.getTotalPages() - 1;
			result = executeQueryFindAllAtivo(lastPage, maxResults);
		}

		return buildResult(result);
	}

	@Transactional(readOnly = true)
	public Empresa findByEmpresa(int page, int maxResults, Usuario usuario) {
		return empresaRepository.findOne(usuario.getEmpresa().getId());
	}

	public Empresa findById(int id) {
		return empresaRepository.findOne(id);
	}

	public List<Empresa> findAll() {
		return (List<Empresa>) empresaRepository.findAll();
	}

	public void save(Empresa usuario) {
		empresaRepository.save(usuario);
	}

	public void delete(int usuarioId) {
		empresaRepository.delete(usuarioId);
	}

	@Transactional(readOnly = true)
	public EmpresaListVO findByNomeLike(int page, int maxResults, String name) {
		Page<Empresa> result = executeQueryFindByNome(page, maxResults, name);

		if (shouldExecuteSameQueryInLastPage(page, result)) {
			int lastPage = result.getTotalPages() - 1;
			result = executeQueryFindByNome(lastPage, maxResults, name);
		}

		return buildResult(result);
	}

	private boolean shouldExecuteSameQueryInLastPage(int page,
			Page<Empresa> result) {
		return isUserAfterOrOnLastPage(page, result)
				&& hasDataInDataBase(result);
	}

	private Page<Empresa> executeQueryFindAll(int page, int maxResults) {
		final PageRequest pageRequest = new PageRequest(page, maxResults,
				sortByNomeASC());

		return empresaRepository.findByAtivo(pageRequest, true);
	}

	private Page<Empresa> executeQueryFindAllAtivo(int page, int maxResults) {
		final PageRequest pageRequest = new PageRequest(page, maxResults,
				sortByNomeASC());

		return empresaRepository.findByAtivo(pageRequest, true);
	}

	private Sort sortByNomeASC() {
		return new Sort(Sort.Direction.ASC, "nome");
	}

	private EmpresaListVO buildResult(Page<Empresa> result) {
		return new EmpresaListVO(result.getTotalPages(),
				result.getTotalElements(), result.getContent());
	}

	private Page<Empresa> executeQueryFindByNome(int page, int maxResults,
			String nome) {
		final PageRequest pageRequest = new PageRequest(page, maxResults,
				sortByNomeASC());

		return empresaRepository.findByNomeLike(pageRequest, "%" + nome + "%");
	}

	private boolean isUserAfterOrOnLastPage(int page, Page<Empresa> result) {
		return page >= result.getTotalPages() - 1;
	}

	private boolean hasDataInDataBase(Page<Empresa> result) {
		return result.getTotalElements() > 0;
	}

	public List<Empresa> findByCliente(int clienteID) {
		Cliente cliente = clienteRepository.findOne(clienteID);
		return empresaRepository.findByCliente(cliente);
	}
}
