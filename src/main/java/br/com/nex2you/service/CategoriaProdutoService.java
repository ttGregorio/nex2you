package br.com.nex2you.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.nex2you.model.CategoriaProduto;
import br.com.nex2you.model.Empresa;
import br.com.nex2you.repository.CategoriaProdutoRepository;
import br.com.nex2you.vo.CategoriaProdutoListVO;

@Service
public class CategoriaProdutoService {

	@Autowired
	private CategoriaProdutoRepository categoriaProdutoRepository;

	@Transactional(readOnly = true)
	public CategoriaProdutoListVO findAll(int page, int maxResults,
			Empresa empresa) {
		Page<CategoriaProduto> result = executeQueryFindAll(page, maxResults,
				empresa);

		if (shouldExecuteSameQueryInLastPage(page, result)) {
			int lastPage = result.getTotalPages() - 1;
			result = executeQueryFindAll(lastPage, maxResults, empresa);
		}

		return buildResult(result);
	}

	public CategoriaProduto findById(int id) {
		return categoriaProdutoRepository.findOne(id);
	}

	public List<CategoriaProduto> findAll() {
		return (List<CategoriaProduto>) categoriaProdutoRepository.findAll();
	}

	public void save(CategoriaProduto usuario) {
		categoriaProdutoRepository.save(usuario);
	}

	public void delete(int usuarioId) {
		categoriaProdutoRepository.delete(usuarioId);
	}

	@Transactional(readOnly = true)
	public CategoriaProdutoListVO findByNomeLike(int page, int maxResults,
			String name, Empresa empresa) {
		Page<CategoriaProduto> result = executeQueryFindByNome(page,
				maxResults, name, empresa);

		if (shouldExecuteSameQueryInLastPage(page, result)) {
			int lastPage = result.getTotalPages() - 1;
			result = executeQueryFindByNome(lastPage, maxResults, name, empresa);
		}

		return buildResult(result);
	}

	private boolean shouldExecuteSameQueryInLastPage(int page,
			Page<CategoriaProduto> result) {
		return isUserAfterOrOnLastPage(page, result)
				&& hasDataInDataBase(result);
	}

	private Page<CategoriaProduto> executeQueryFindAll(int page,
			int maxResults, Empresa empresa) {
		final PageRequest pageRequest = new PageRequest(page, maxResults,
				sortByNomeASC());

		return categoriaProdutoRepository.findByEmpresaAndAtivo(pageRequest,
				empresa, true);
	}

	private Sort sortByNomeASC() {
		return new Sort(Sort.Direction.ASC, "nome");
	}

	private CategoriaProdutoListVO buildResult(Page<CategoriaProduto> result) {
		return new CategoriaProdutoListVO(result.getTotalPages(),
				result.getTotalElements(), result.getContent());
	}

	private Page<CategoriaProduto> executeQueryFindByNome(int page,
			int maxResults, String nome, Empresa empresa) {
		final PageRequest pageRequest = new PageRequest(page, maxResults,
				sortByNomeASC());

		return categoriaProdutoRepository.findByNomeLikeAndEmpresa(pageRequest,
				"%" + nome + "%", empresa);
	}

	private boolean isUserAfterOrOnLastPage(int page,
			Page<CategoriaProduto> result) {
		return page >= result.getTotalPages() - 1;
	}

	private boolean hasDataInDataBase(Page<CategoriaProduto> result) {
		return result.getTotalElements() > 0;
	}

	public List<CategoriaProduto> findByEmpresa(Empresa empresa) {
		return (List<CategoriaProduto>) categoriaProdutoRepository
				.findByEmpresaAndAtivo(empresa, true);
	}
}
