package br.com.nex2you.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.nex2you.model.CategoriaEmpresa;
import br.com.nex2you.model.Cliente;
import br.com.nex2you.model.PreferenciaCliente;
import br.com.nex2you.repository.PreferenciaClienteRepository;

@Service
public class PreferenciaClienteService {

	@Autowired
	private PreferenciaClienteRepository preferenciaClienteRepository;

	public List<PreferenciaCliente> findByCliente(Cliente cliente) {
		return preferenciaClienteRepository.findByCliente(cliente);
	}

	public void save(PreferenciaCliente usuario) {
		preferenciaClienteRepository.save(usuario);
	}

	public void delete(int usuarioId) {
		preferenciaClienteRepository.delete(usuarioId);
	}

	public PreferenciaCliente findByClienteAndCategoria(Cliente cliente2,
			CategoriaEmpresa categoria) {
		// TODO Auto-generated method stub
		return preferenciaClienteRepository.findByClienteAndCategoria(cliente2,
				categoria);
	}

}
