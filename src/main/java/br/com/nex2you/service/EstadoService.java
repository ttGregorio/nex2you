package br.com.nex2you.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.nex2you.model.Estado;
import br.com.nex2you.repository.EstadoRepository;

@Service
public class EstadoService {

	@Autowired
	private EstadoRepository estadoRepository;

	public List<Estado> findAll() {
		return (List<Estado>) estadoRepository.findAll();
	}
	
	public Estado findById(int id){
		return estadoRepository.findOne(id);
	}
}
