package br.com.nex2you.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.nex2you.model.Cliente;
import br.com.nex2you.repository.ClienteRepository;

@Service
public class ClienteService {

	@Autowired
	private ClienteRepository clienteRepository;

	public Cliente findById(int id) {
		return clienteRepository.findOne(id);
	}

	public Cliente findByLoginSenha(String login, String senha) {
		return clienteRepository.findByLoginAndSenha(login, senha);
	}

	public List<Cliente> findAll() {
		return (List<Cliente>) clienteRepository.findAll();
	}

	public void save(Cliente usuario) {
		clienteRepository.save(usuario);
	}

	public void delete(int usuarioId) {
		clienteRepository.delete(usuarioId);
	}

}
