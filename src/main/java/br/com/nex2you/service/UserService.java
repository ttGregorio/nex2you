package br.com.nex2you.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.nex2you.model.User;
import br.com.nex2you.repository.UserRepository;

@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public User findByEmail(String email) {
		return userRepository.findByEmail(email);
	}

	public void save(User usuario) {
		userRepository.save(usuario);
	}

}
