package br.com.nex2you.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import br.com.nex2you.model.ContaCliente;
import br.com.nex2you.model.ContaClienteItem;
import br.com.nex2you.repository.ContaClienteItemRepository;
import br.com.nex2you.vo.ContaClienteItemListVO;

@Service
public class ContaClienteItemService {

	@Autowired
	private ContaClienteItemRepository contaClienteRepository;

	@Transactional(readOnly = true)
	public ContaClienteItemListVO findAll(int page, int maxResults,
			ContaCliente empresa) {
		List<ContaClienteItem> result = executeQueryFindAll(page, maxResults,
				empresa);

		return buildResult(result);
	}

	public ContaClienteItem findById(int id) {
		return contaClienteRepository.findOne(id);
	}

	public List<ContaClienteItem> findAll() {
		return (List<ContaClienteItem>) contaClienteRepository.findAll();
	}

	public void save(ContaClienteItem usuario) {
		contaClienteRepository.save(usuario);
	}

	public void delete(int usuarioId) {
		contaClienteRepository.delete(usuarioId);
	}

	private List<ContaClienteItem> executeQueryFindAll(int page,
			int maxResults, ContaCliente empresa) {
		return contaClienteRepository.findByContaCliente(empresa);
	}

	private ContaClienteItemListVO buildResult(List<ContaClienteItem> result) {
		return new ContaClienteItemListVO(1, result.size(), result);
	}

	public List<ContaClienteItem> findByContaCliente(ContaCliente contaCliente) {
		return contaClienteRepository.findByContaCliente(contaCliente);
	}

}
