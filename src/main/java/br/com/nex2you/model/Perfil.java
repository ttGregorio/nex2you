package br.com.nex2you.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.nex2you.utils.Nex2YouBean;

/**
 * Bean de dados referentes ao perfil do usu�rio.
 * 
 * @author Thiago Gregorio
 * 
 */
@Entity
@Table(name = "perfis")
public class Perfil extends Nex2YouBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Perfil() {
	}

	/**
	 * Id do perfil.
	 */
	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	private int id;

	/**
	 * Nome do perfil.
	 */
	@Column(name = "nome")
	private String nome;

	@OneToOne
	@JoinColumn(name = "empresa")
	private Empresa empresa;

	/**
	 * Flag indicando se o perfil est� ou n�o ativo.
	 */
	@Column(name = "ativo")
	private boolean ativo;

	@Column(name = "permite_usuarios")
	private boolean permiteUsuarios;

	@Column(name = "permite_perfis")
	private boolean permitePerfis;

	@Column(name = "permite_dados_empresa")
	private boolean permiteDadosEmpresa;

	@Column(name = "permite_categorias")
	private boolean permiteCategorias;

	@Column(name = "permite_produtos")
	private boolean permiteProdutos;

	@Column(name = "permite_contas")
	private boolean permiteContas;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Getter da variável empresa
	 * 
	 * @return Valor da variável empresa
	 */
	public Empresa getEmpresa() {
		return empresa;
	}

	/**
	 * Setter da variável empresa
	 * 
	 * @param Valor
	 *            a ser inserido em empresa
	 */
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	/**
	 * @return the ativo
	 */
	public boolean isAtivo() {
		return ativo;
	}

	/**
	 * @param ativo
	 *            the ativo to set
	 */
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	/**
	 * @return the permiteUsuarios
	 */
	public boolean isPermiteUsuarios() {
		return permiteUsuarios;
	}

	/**
	 * @param permiteUsuarios
	 *            the permiteUsuarios to set
	 */
	public void setPermiteUsuarios(boolean permiteUsuarios) {
		this.permiteUsuarios = permiteUsuarios;
	}

	/**
	 * @return the permitePerfis
	 */
	public boolean isPermitePerfis() {
		return permitePerfis;
	}

	/**
	 * @param permitePerfis
	 *            the permitePerfis to set
	 */
	public void setPermitePerfis(boolean permitePerfis) {
		this.permitePerfis = permitePerfis;
	}

	/**
	 * @return the permiteDadosEmpresa
	 */
	public boolean isPermiteDadosEmpresa() {
		return permiteDadosEmpresa;
	}

	/**
	 * @param permiteDadosEmpresa
	 *            the permiteDadosEmpresa to set
	 */
	public void setPermiteDadosEmpresa(boolean permiteDadosEmpresa) {
		this.permiteDadosEmpresa = permiteDadosEmpresa;
	}

	/**
	 * @return the permiteCategorias
	 */
	public boolean isPermiteCategorias() {
		return permiteCategorias;
	}

	/**
	 * @param permiteCategorias
	 *            the permiteCategorias to set
	 */
	public void setPermiteCategorias(boolean permiteCategorias) {
		this.permiteCategorias = permiteCategorias;
	}

	/**
	 * @return the permiteProdutos
	 */
	public boolean isPermiteProdutos() {
		return permiteProdutos;
	}

	/**
	 * @param permiteProdutos
	 *            the permiteProdutos to set
	 */
	public void setPermiteProdutos(boolean permiteProdutos) {
		this.permiteProdutos = permiteProdutos;
	}

	/**
	 * @return the permiteContas
	 */
	public boolean isPermiteContas() {
		return permiteContas;
	}

	/**
	 * @param permiteContas
	 *            the permiteContas to set
	 */
	public void setPermiteContas(boolean permiteContas) {
		this.permiteContas = permiteContas;
	}

}
