package br.com.nex2you.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * Bean de dados referentes ao estado.
 * 
 * @author Thiago Gregorio
 * 
 */
@Entity
@Table(name = "estados")
public class Estado implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Estado(){
		
	}
	/**
	 * Id do estado.
	 */
	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	private int id;

	/**
	 * Nome do estado.
	 */
	@Column(name = "nome")
	private String nome;

	/**
	 * Sigla do estado.
	 */
	@Column(name = "sigla")
	private String sigla;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * @param nome
	 *            the nome to set
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * @return the sigla
	 */
	public String getSigla() {
		return sigla;
	}

	/**
	 * @param sigla
	 *            the sigla to set
	 */
	public void setSigla(String sigla) {
		this.sigla = sigla;
	}

	@Override
	public int hashCode() {
		return getId();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Estado other = (Estado) obj;
		if (id == 0) {
			if (other.id == 0)
				return false;
		} else if (id == other.id)
			return true;
		return false;
	}
}
