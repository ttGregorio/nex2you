package br.com.nex2you.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.nex2you.utils.Nex2YouBean;

@Entity
@Table(name = "produtos_imagens")
public class ProdutoImagem extends Nex2YouBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	private int id;

	@Column(name = "imagem")
	private byte[] imagem;

	@Column(name = "descricao")
	private String descricao;

	@OneToOne
	@JoinColumn(name = "produto")
	private Produto produto;

	/**
	 * Getter da variável id
	 * 
	 * @return Valor da variável id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter da variável id
	 * 
	 * @param Valor
	 *            a ser inserido em id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter da variável imagem
	 * 
	 * @return Valor da variável imagem
	 */
	public byte[] getImagem() {
		return imagem;
	}

	/**
	 * Setter da variável imagem
	 * 
	 * @param Valor
	 *            a ser inserido em imagem
	 */
	public void setImagem(byte[] imagem) {
		this.imagem = imagem;
	}

	/**
	 * Getter da variável descricao
	 * 
	 * @return Valor da variável descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * Setter da variável descricao
	 * 
	 * @param Valor
	 *            a ser inserido em descricao
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * Getter da variável produto
	 * 
	 * @return Valor da variável produto
	 */
	public Produto getProduto() {
		return produto;
	}

	/**
	 * Setter da variável produto
	 * 
	 * @param Valor
	 *            a ser inserido em produto
	 */
	public void setProduto(Produto produto) {
		this.produto = produto;
	}

}
