/**
 * 
 */
package br.com.nex2you.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.nex2you.utils.Nex2YouBean;

/**
 * @author Thiago Tavares Gregorio
 * 
 */
@Entity
@Table(name = "preferencias_clientes")
public class PreferenciaCliente extends Nex2YouBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	private int id;

	@OneToOne
	@JoinColumn(name = "cliente")
	private Cliente cliente;

	@OneToOne
	@JoinColumn(name = "categoria")
	private CategoriaEmpresa categoria;

	/**
	 * Getter da variável id
	 * 
	 * @return Valor da variável id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter da variável id
	 * 
	 * @param Valor
	 *            a ser inserido em id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter da variável cliente
	 * 
	 * @return Valor da variável cliente
	 */
	public Cliente getCliente() {
		return cliente;
	}

	/**
	 * Setter da variável cliente
	 * 
	 * @param Valor
	 *            a ser inserido em cliente
	 */
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	/**
	 * Getter da variável categoria
	 * 
	 * @return Valor da variável categoria
	 */
	public CategoriaEmpresa getCategoria() {
		return categoria;
	}

	/**
	 * Setter da variável categoria
	 * 
	 * @param Valor
	 *            a ser inserido em categoria
	 */
	public void setCategoria(CategoriaEmpresa categoria) {
		this.categoria = categoria;
	}

}
