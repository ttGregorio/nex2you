/**
 * 
 */
package br.com.nex2you.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.nex2you.utils.Nex2YouBean;

/**
 * @author Thiago Tavares Gregorio
 * 
 */
@Entity
@Table(name = "produtos")
public class Produto extends Nex2YouBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	private int id;

	@OneToOne
	@JoinColumn(name = "empresa")
	private Empresa empresa;

	@OneToOne
	@JoinColumn(name = "categoria")
	private CategoriaProduto categoria;

	@Column(name = "nome")
	private String nome;

	@Column(name = "descricao")
	private String descricao;

	@Column(name = "ativo")
	private boolean ativo;

	@Column(name = "estoque")
	private int estoque;

	@Column(name = "preco_compra")
	private double precoCompra;

	@Column(name = "preco_venda")
	private double precoVenda;

	/**
	 * Getter da variável id
	 * 
	 * @return Valor da variável id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter da variável id
	 * 
	 * @param Valor
	 *            a ser inserido em id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter da variável empresa
	 * 
	 * @return Valor da variável empresa
	 */
	public Empresa getEmpresa() {
		return empresa;
	}

	/**
	 * Setter da variável empresa
	 * 
	 * @param Valor
	 *            a ser inserido em empresa
	 */
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	/**
	 * Getter da variável categoria
	 * 
	 * @return Valor da variável categoria
	 */
	public CategoriaProduto getCategoria() {
		return categoria;
	}

	/**
	 * Setter da variável categoria
	 * 
	 * @param Valor
	 *            a ser inserido em categoria
	 */
	public void setCategoria(CategoriaProduto categoria) {
		this.categoria = categoria;
	}

	/**
	 * Getter da variável nome
	 * 
	 * @return Valor da variável nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Setter da variável nome
	 * 
	 * @param Valor
	 *            a ser inserido em nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Getter da variável descricao
	 * 
	 * @return Valor da variável descricao
	 */
	public String getDescricao() {
		return descricao;
	}

	/**
	 * Setter da variável descricao
	 * 
	 * @param Valor
	 *            a ser inserido em descricao
	 */
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * Getter da variável ativo
	 * 
	 * @return Valor da variável ativo
	 */
	public boolean isAtivo() {
		return ativo;
	}

	/**
	 * Setter da variável ativo
	 * 
	 * @param Valor
	 *            a ser inserido em ativo
	 */
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	/**
	 * Getter da variável estoque
	 * 
	 * @return Valor da variável estoque
	 */
	public int getEstoque() {
		return estoque;
	}

	/**
	 * Setter da variável estoque
	 * 
	 * @param Valor
	 *            a ser inserido em estoque
	 */
	public void setEstoque(int estoque) {
		this.estoque = estoque;
	}

	/**
	 * Getter da variável precoCompra
	 * 
	 * @return Valor da variável precoCompra
	 */
	public double getPrecoCompra() {
		return precoCompra;
	}

	/**
	 * Setter da variável precoCompra
	 * 
	 * @param Valor
	 *            a ser inserido em precoCompra
	 */
	public void setPrecoCompra(double precoCompra) {
		this.precoCompra = precoCompra;
	}

	/**
	 * Getter da variável precoVenda
	 * 
	 * @return Valor da variável precoVenda
	 */
	public double getPrecoVenda() {
		return precoVenda;
	}

	/**
	 * Setter da variável precoVenda
	 * 
	 * @param Valor
	 *            a ser inserido em precoVenda
	 */
	public void setPrecoVenda(double precoVenda) {
		this.precoVenda = precoVenda;
	}

}
