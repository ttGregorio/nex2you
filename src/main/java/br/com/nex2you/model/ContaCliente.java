/**
 * 
 */
package br.com.nex2you.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.nex2you.utils.Nex2YouBean;

/**
 * @author Thiago Tavares Gregorio
 * 
 */
@Entity
@Table(name = "contas_clientes")
public class ContaCliente extends Nex2YouBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	private int id;

	@OneToOne
	@JoinColumn(name = "empresa")
	private Empresa empresa;

	@OneToOne
	@JoinColumn(name = "cliente")
	private Cliente cliente;

	@Column(name = "novo_pedido")
	private boolean pedidoAberto;

	@Column(name = "valor_conta")
	private double valor;

	/**
	 * Getter da variável id
	 * 
	 * @return Valor da variável id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter da variável id
	 * 
	 * @param Valor
	 *            a ser inserido em id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter da variável empresa
	 * 
	 * @return Valor da variável empresa
	 */
	public Empresa getEmpresa() {
		return empresa;
	}

	/**
	 * Setter da variável empresa
	 * 
	 * @param Valor
	 *            a ser inserido em empresa
	 */
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	/**
	 * Getter da variável cliente
	 * 
	 * @return Valor da variável cliente
	 */
	public Cliente getCliente() {
		return cliente;
	}

	/**
	 * Setter da variável cliente
	 * 
	 * @param Valor
	 *            a ser inserido em cliente
	 */
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	/**
	 * Getter da variável pedidoAberto
	 * 
	 * @return Valor da variável pedidoAberto
	 */
	public boolean isPedidoAberto() {
		return pedidoAberto;
	}

	/**
	 * Setter da variável pedidoAberto
	 * 
	 * @param Valor
	 *            a ser inserido em pedidoAberto
	 */
	public void setPedidoAberto(boolean pedidoAberto) {
		this.pedidoAberto = pedidoAberto;
	}

	/**
	 * Getter da variável valor
	 * 
	 * @return Valor da variável valor
	 */
	public double getValor() {
		return valor;
	}

	/**
	 * Setter da variável valor
	 * 
	 * @param Valor
	 *            a ser inserido em valor
	 */
	public void setValor(double valor) {
		this.valor = valor;
	}

}
