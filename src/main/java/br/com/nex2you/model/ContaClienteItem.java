/**
 * 
 */
package br.com.nex2you.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.nex2you.utils.Nex2YouBean;

/**
 * @author Thiago Tavares Gregorio
 * 
 */
@Entity
@Table(name = "contas_clientes_itens")
public class ContaClienteItem extends Nex2YouBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	private int id;

	@OneToOne
	@JoinColumn(name = "conta_cliente")
	private ContaCliente contaCliente;

	@OneToOne
	@JoinColumn(name = "produto")
	private Produto produto;

	@Column(name = "quantidade")
	private int quantidade;

	@Column(name = "valor")
	private double valor;

	/**
	 * Getter da variável id
	 * 
	 * @return Valor da variável id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter da variável id
	 * 
	 * @param Valor
	 *            a ser inserido em id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter da variável contaCliente
	 * 
	 * @return Valor da variável contaCliente
	 */
	public ContaCliente getContaCliente() {
		return contaCliente;
	}

	/**
	 * Setter da variável contaCliente
	 * 
	 * @param Valor
	 *            a ser inserido em contaCliente
	 */
	public void setContaCliente(ContaCliente contaCliente) {
		this.contaCliente = contaCliente;
	}

	/**
	 * Getter da variável produto
	 * 
	 * @return Valor da variável produto
	 */
	public Produto getProduto() {
		return produto;
	}

	/**
	 * Setter da variável produto
	 * 
	 * @param Valor
	 *            a ser inserido em produto
	 */
	public void setProduto(Produto produto) {
		this.produto = produto;
	}

	/**
	 * Getter da variável quantidade
	 * 
	 * @return Valor da variável quantidade
	 */
	public int getQuantidade() {
		return quantidade;
	}

	/**
	 * Setter da variável quantidade
	 * 
	 * @param Valor
	 *            a ser inserido em quantidade
	 */
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}

	/**
	 * Getter da variável valor
	 * 
	 * @return Valor da variável valor
	 */
	public double getValor() {
		return valor;
	}

	/**
	 * Setter da variável valor
	 * 
	 * @param Valor
	 *            a ser inserido em valor
	 */
	public void setValor(double valor) {
		this.valor = valor;
	}

}
