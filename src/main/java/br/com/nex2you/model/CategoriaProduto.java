/**
 * 
 */
package br.com.nex2you.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.nex2you.utils.Nex2YouBean;

/**
 * 
 * @author Thiago Tavares Gregorio
 * 
 */
@Entity
@Table(name = "categorias_produtos")
public class CategoriaProduto extends Nex2YouBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	private int id;

	@OneToOne
	@JoinColumn(name = "empresa")
	private Empresa empresa;

	@Column(name = "nome")
	private String nome;

	@Column(name = "ativo")
	private boolean ativo;

	/**
	 * Getter da variável id
	 * 
	 * @return Valor da variável id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter da variável id
	 * 
	 * @param Valor
	 *            a ser inserido em id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter da variável empresa
	 * 
	 * @return Valor da variável empresa
	 */
	public Empresa getEmpresa() {
		return empresa;
	}

	/**
	 * Setter da variável empresa
	 * 
	 * @param Valor
	 *            a ser inserido em empresa
	 */
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	/**
	 * Getter da variável nome
	 * 
	 * @return Valor da variável nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Setter da variável nome
	 * 
	 * @param Valor
	 *            a ser inserido em nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Getter da variável ativo
	 * 
	 * @return Valor da variável ativo
	 */
	public boolean isAtivo() {
		return ativo;
	}

	/**
	 * Setter da variável ativo
	 * 
	 * @param Valor
	 *            a ser inserido em ativo
	 */
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

}
