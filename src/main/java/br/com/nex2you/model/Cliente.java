/**
 * 
 */
package br.com.nex2you.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.nex2you.utils.Nex2YouBean;

/**
 * @author Thiago Tavares Gregorio
 * 
 */
@Entity
@Table(name = "clientes")
public class Cliente extends Nex2YouBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	private int id;

	@Column(name = "nome")
	private String nome;

	@Column(name = "email")
	private String email;

	@Column(name = "login")
	private String login;

	@Column(name = "senha")
	private String senha;

	@OneToOne
	@JoinColumn(name = "empresa")
	private Empresa empresa;

	/**
	 * Getter da variável id
	 * @return Valor da variável id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter da variável id
	 * @param Valor a ser inserido em id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter da variável nome
	 * @return Valor da variável nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Setter da variável nome
	 * @param Valor a ser inserido em nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Getter da variável email
	 * @return Valor da variável email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Setter da variável email
	 * @param Valor a ser inserido em email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Getter da variável login
	 * @return Valor da variável login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * Setter da variável login
	 * @param Valor a ser inserido em login
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * Getter da variável senha
	 * @return Valor da variável senha
	 */
	public String getSenha() {
		return senha;
	}

	/**
	 * Setter da variável senha
	 * @param Valor a ser inserido em senha
	 */
	public void setSenha(String senha) {
		this.senha = senha;
	}

	/**
	 * Getter da variável empresa
	 * @return Valor da variável empresa
	 */
	public Empresa getEmpresa() {
		return empresa;
	}

	/**
	 * Setter da variável empresa
	 * @param Valor a ser inserido em empresa
	 */
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}


}
