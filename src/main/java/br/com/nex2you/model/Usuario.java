package br.com.nex2you.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import br.com.nex2you.utils.Nex2YouBean;

/**
 * Bean de dados do usuário.
 * 
 * @author Thiago Gregorio
 * 
 */
@Entity
@Table(name = "usuarios")
public class Usuario extends Nex2YouBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Usuario() {
		setPerfil(new Perfil());
		setEstado(new Estado());
	}

	/**
	 * Id do usuário.
	 */
	@Id
	@GenericGenerator(name = "increment", strategy = "increment")
	private int id;

	/**
	 * Nome do usuário.
	 */
	@Column(name = "nome")
	private String nome;

	/**
	 * Email do usuário.
	 */
	@Column(name = "email")
	private String email;

	/**
	 * Flag indicando se o usuário está ou não ativo.
	 */
	@Column(name = "ativo")
	private boolean ativo;

	/**
	 * Login do usuário.
	 */
	@Column(name = "login")
	private String login;

	/**
	 * Senha do usuário.
	 */
	@Column(name = "senha")
	private String senha;

	/**
	 * Perfil do usuário.
	 */
	@OneToOne
	@JoinColumn(name = "perfil")
	private Perfil perfil;

	@OneToOne
	@JoinColumn(name = "empresa")
	private Empresa empresa;

	/**
	 * Número de RG do usuário.
	 */
	@Column(name = "rg")
	private String rg;

	/**
	 * Número de CPF do usuário.
	 */
	@Column(name = "cpf")
	private String cpf;

	/**
	 * Endereço do usuário.
	 */
	@Column(name = "endereco")
	private String endereco;

	/**
	 * Bairro onde reside o usuário.
	 */
	@Column(name = "bairro")
	private String bairro;

	/**
	 * Cidade onde reside o usuário.
	 */
	@Column(name = "cidade")
	private String cidade;

	/**
	 * Estado onde reside o usuário.
	 */
	@OneToOne
	@JoinColumn(name = "estado")
	private Estado estado;

	@Enumerated(EnumType.STRING)
	@Column(name = "user_role")
	private Role role;

	/**
	 * Getter de id
	 * 
	 * @return Valor de id
	 */
	public int getId() {
		return id;
	}

	/**
	 * Setter de id
	 * 
	 * @param Valor
	 *            a ser inserido em id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * Getter de nome
	 * 
	 * @return Valor de nome
	 */
	public String getNome() {
		return nome;
	}

	/**
	 * Setter de nome
	 * 
	 * @param Valor
	 *            a ser inserido em nome
	 */
	public void setNome(String nome) {
		this.nome = nome;
	}

	/**
	 * Getter de email
	 * 
	 * @return Valor de email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * Setter de email
	 * 
	 * @param Valor
	 *            a ser inserido em email
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * Getter de ativo
	 * 
	 * @return Valor de ativo
	 */
	public boolean isAtivo() {
		return ativo;
	}

	/**
	 * Setter de ativo
	 * 
	 * @param Valor
	 *            a ser inserido em ativo
	 */
	public void setAtivo(boolean ativo) {
		this.ativo = ativo;
	}

	/**
	 * Getter de login
	 * 
	 * @return Valor de login
	 */
	public String getLogin() {
		return login;
	}

	/**
	 * Setter de login
	 * 
	 * @param Valor
	 *            a ser inserido em login
	 */
	public void setLogin(String login) {
		this.login = login;
	}

	/**
	 * Getter de senha
	 * 
	 * @return Valor de senha
	 */
	public String getSenha() {
		return senha;
	}

	/**
	 * Setter de senha
	 * 
	 * @param Valor
	 *            a ser inserido em senha
	 */
	public void setSenha(String senha) {
		this.senha = senha;
	}

	/**
	 * Getter de perfil
	 * 
	 * @return Valor de perfil
	 */
	public Perfil getPerfil() {
		return perfil;
	}

	/**
	 * Setter de perfil
	 * 
	 * @param Valor
	 *            a ser inserido em perfil
	 */
	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	/**
	 * @return the empresa
	 */
	public Empresa getEmpresa() {
		return empresa;
	}

	/**
	 * @param empresa
	 *            the empresa to set
	 */
	public void setEmpresa(Empresa empresa) {
		this.empresa = empresa;
	}

	/**
	 * Getter de rg
	 * 
	 * @return Valor de rg
	 */
	public String getRg() {
		return rg;
	}

	/**
	 * Setter de rg
	 * 
	 * @param Valor
	 *            a ser inserido em rg
	 */
	public void setRg(String rg) {
		this.rg = rg;
	}

	/**
	 * Getter de cpf
	 * 
	 * @return Valor de cpf
	 */
	public String getCpf() {
		return cpf;
	}

	/**
	 * Setter de cpf
	 * 
	 * @param Valor
	 *            a ser inserido em cpf
	 */
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	/**
	 * Getter de endereco
	 * 
	 * @return Valor de endereco
	 */
	public String getEndereco() {
		return endereco;
	}

	/**
	 * Setter de endereco
	 * 
	 * @param Valor
	 *            a ser inserido em endereco
	 */
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	/**
	 * Getter de bairro
	 * 
	 * @return Valor de bairro
	 */
	public String getBairro() {
		return bairro;
	}

	/**
	 * Setter de bairro
	 * 
	 * @param Valor
	 *            a ser inserido em bairro
	 */
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	/**
	 * Getter de cidade
	 * 
	 * @return Valor de cidade
	 */
	public String getCidade() {
		return cidade;
	}

	/**
	 * Setter de cidade
	 * 
	 * @param Valor
	 *            a ser inserido em cidade
	 */
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	/**
	 * Getter de estado
	 * 
	 * @return Valor de estado
	 */
	public Estado getEstado() {
		return estado;
	}

	/**
	 * Setter de estado
	 * 
	 * @param Valor
	 *            a ser inserido em estado
	 */
	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	/**
	 * @return the role
	 */
	public Role getRole() {
		return role;
	}

	/**
	 * @param role
	 *            the role to set
	 */
	public void setRole(Role role) {
		this.role = role;
	}

	@Override
	public int hashCode() {
		return getId();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (id == 0) {
			if (other.id == 0)
				return false;
		} else if (id == other.id)
			return true;
		return false;
	}
}
