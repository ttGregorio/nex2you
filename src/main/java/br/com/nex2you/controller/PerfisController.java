package br.com.nex2you.controller;

import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.nex2you.model.Empresa;
import br.com.nex2you.model.Perfil;
import br.com.nex2you.model.Usuario;
import br.com.nex2you.service.PerfilService;
import br.com.nex2you.utils.Nex2YouController;
import br.com.nex2you.vo.PerfilListVO;

@Controller
@RequestMapping(value = "/protected/perfis")
public class PerfisController extends Nex2YouController {

	@Autowired
	private PerfilService perfilService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView welcome() {
		return new ModelAndView("perfisList");
	}

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> listAll(HttpServletRequest request,
			@RequestParam int page, Locale locale) {
		HttpSession session = request.getSession();
		Usuario user = (Usuario) session.getAttribute("user");
		return createListAllResponse(page, locale, user.getEmpresa());
	}

	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> create(
			HttpServletRequest request,
			@RequestBody Perfil perfil,
			@RequestParam(required = false) String searchFor,
			@RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
			Locale locale) {
		HttpSession session = request.getSession();
		Usuario user = (Usuario) session.getAttribute("user");
		perfil.setAtivo(true);
		perfil.setEmpresa(user.getEmpresa());

		perfilService.save(perfil);

		if (isSearchActivated(searchFor)) {
			return search(searchFor, page, locale, "message.create.success",
					user.getEmpresa());
		}

		return createListAllResponse(page, locale, "message.create.success",
				user.getEmpresa());
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<?> update(
			HttpServletRequest request,
			@PathVariable("id") int perfilId,
			@RequestBody Perfil perfil,
			@RequestParam(required = false) String searchFor,
			@RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
			Locale locale) {
		HttpSession session = request.getSession();
		Usuario user = (Usuario) session.getAttribute("user");
		perfil.setEmpresa(user.getEmpresa());
		if (perfilId != perfil.getId()) {
			return new ResponseEntity<String>("Bad Request",
					HttpStatus.BAD_REQUEST);
		}

		perfilService.save(perfil);

		if (isSearchActivated(searchFor)) {
			return search(searchFor, page, locale, "message.update.success",
					user.getEmpresa());
		}

		return createListAllResponse(page, locale, "message.update.success",
				user.getEmpresa());
	}

	@RequestMapping(value = "/{perfilId}", method = RequestMethod.DELETE, produces = "application/json")
	public ResponseEntity<?> delete(
			HttpServletRequest request,
			@PathVariable("perfilId") int perfilId,
			@RequestParam(required = false) String searchFor,
			@RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
			Locale locale) {
		HttpSession session = request.getSession();
		Usuario user = (Usuario) session.getAttribute("user");

		try {
			perfilService.delete(perfilId);
		} catch (AccessDeniedException e) {
			return new ResponseEntity<Object>(HttpStatus.FORBIDDEN);
		}

		if (isSearchActivated(searchFor)) {
			return search(searchFor, page, locale, "message.delete.success",
					user.getEmpresa());
		}

		return createListAllResponse(page, locale, "message.delete.success",
				user.getEmpresa());
	}

	@RequestMapping(value = "/{name}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> search(
			HttpServletRequest request,
			@PathVariable("name") String name,
			@RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
			Locale locale) {
		HttpSession session = request.getSession();
		Usuario user = (Usuario) session.getAttribute("user");
		return search(name, page, locale, null, user.getEmpresa());
	}

	private ResponseEntity<?> search(String name, int page, Locale locale,
			String actionMessageKey, Empresa empresa) {
		PerfilListVO perfilListVO = perfilService.findByNomeLike(page,
				maxResults, name, empresa);

		if (!StringUtils.isEmpty(actionMessageKey)) {
			addActionMessageToVO(perfilListVO, locale, actionMessageKey, null);
		}

		Object[] args = { name };

		addSearchMessageToVO(perfilListVO, locale, "message.search.for.active",
				args);

		return new ResponseEntity<PerfilListVO>(perfilListVO, HttpStatus.OK);
	}

	private PerfilListVO listAll(int page, Empresa empresa) {
		return perfilService.findAllAtivo(page, maxResults, empresa);
	}

	private ResponseEntity<PerfilListVO> returnListToUser(
			PerfilListVO perfilList) {
		return new ResponseEntity<PerfilListVO>(perfilList, HttpStatus.OK);
	}

	private ResponseEntity<?> createListAllResponse(int page, Locale locale,
			Empresa empresa) {
		return createListAllResponse(page, locale, null, empresa);
	}

	private ResponseEntity<?> createListAllResponse(int page, Locale locale,
			String messageKey, Empresa empresa) {
		PerfilListVO perfilListVO = listAll(page, empresa);

		addActionMessageToVO(perfilListVO, locale, messageKey, null);

		return returnListToUser(perfilListVO);
	}

	private PerfilListVO addActionMessageToVO(PerfilListVO perfilListVO,
			Locale locale, String actionMessageKey, Object[] args) {
		if (StringUtils.isEmpty(actionMessageKey)) {
			return perfilListVO;
		}

		perfilListVO.setActionMessage(messageSource.getMessage(
				actionMessageKey, args, null, locale));

		return perfilListVO;
	}

	private PerfilListVO addSearchMessageToVO(PerfilListVO perfilListVO,
			Locale locale, String actionMessageKey, Object[] args) {
		if (StringUtils.isEmpty(actionMessageKey)) {
			return perfilListVO;
		}

		perfilListVO.setSearchMessage(messageSource.getMessage(
				actionMessageKey, args, null, locale));

		return perfilListVO;
	}
}