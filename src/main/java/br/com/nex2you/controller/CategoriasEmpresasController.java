package br.com.nex2you.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.nex2you.model.CategoriaEmpresa;
import br.com.nex2you.model.Cliente;
import br.com.nex2you.model.PreferenciaCliente;
import br.com.nex2you.service.CategoriaEmpresaService;
import br.com.nex2you.service.ClienteService;
import br.com.nex2you.service.PreferenciaClienteService;
import br.com.nex2you.utils.Nex2YouController;

@Controller
@RequestMapping(value = "/protected/categoriasEmpresas")
public class CategoriasEmpresasController extends Nex2YouController {

	@Autowired
	private ClienteService clienteService;

	@Autowired
	private CategoriaEmpresaService categoriaService;

	@Autowired
	private PreferenciaClienteService preferenciaClienteService;

	@RequestMapping(value = "/{cliente}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getPreferencia(@PathVariable int cliente) {
		List<CategoriaEmpresa> lista = categoriaService.findAll();

		for (CategoriaEmpresa categoriaEmpresa : lista) {
			Cliente cliente2 = clienteService.findById(cliente);
			PreferenciaCliente preferenciaCliente = preferenciaClienteService
					.findByClienteAndCategoria(cliente2, categoriaEmpresa);
			if (preferenciaCliente != null) {
				categoriaEmpresa.setChecked(true);
			}
		}
		ResponseEntity<List<CategoriaEmpresa>> resposta = new ResponseEntity<List<CategoriaEmpresa>>(
				lista, HttpStatus.OK);
		return resposta;
	}
}