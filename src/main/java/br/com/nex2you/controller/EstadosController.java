package br.com.nex2you.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.nex2you.model.Estado;
import br.com.nex2you.service.EstadoService;
import br.com.nex2you.utils.Nex2YouController;

@Controller
@RequestMapping(value = "/protected/estados")
public class EstadosController extends Nex2YouController{

	@Autowired
	private EstadoService estadoService;

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> listEstados() {
		ResponseEntity<List<Estado>> resposta = new ResponseEntity<List<Estado>>(
				estadoService.findAll(), HttpStatus.OK);
		return resposta;
	}

}