package br.com.nex2you.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.nex2you.model.CategoriaEmpresa;
import br.com.nex2you.model.Cliente;
import br.com.nex2you.model.PreferenciaCliente;
import br.com.nex2you.service.CategoriaEmpresaService;
import br.com.nex2you.service.ClienteService;
import br.com.nex2you.service.PreferenciaClienteService;
import br.com.nex2you.utils.Nex2YouController;

;

@Controller
@RequestMapping(value = "/protected/clientes")
public class ClientesController extends Nex2YouController {

	@Autowired
	private ClienteService clienteService;

	@Autowired
	private PreferenciaClienteService preferenciaClienteService;

	@Autowired
	private CategoriaEmpresaService categoriaService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView welcome() {
		return new ModelAndView("clientesList");
	}

	@RequestMapping(value = "/{login}/{senha}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getUsuario(@PathVariable("login") String login,
			@PathVariable("senha") String senha) {
		Cliente cliente = clienteService.findByLoginSenha(login, senha);
		return new ResponseEntity<Cliente>(cliente, HttpStatus.OK);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getById(@PathVariable("id") int id) {
		Cliente cliente = clienteService.findById(id);
		return new ResponseEntity<Cliente>(cliente, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> create(@RequestBody Cliente cliente) {
		clienteService.save(cliente);
		Cliente clienteCadastrado = clienteService.findByLoginSenha(
				cliente.getLogin(), cliente.getSenha());

		List<CategoriaEmpresa> listaCategorias = categoriaService.findAll();
		for (CategoriaEmpresa categoriaEmpresa : listaCategorias) {
			PreferenciaCliente preferenciaCliente = new PreferenciaCliente();
			preferenciaCliente.setCategoria(categoriaEmpresa);
			preferenciaCliente.setCliente(clienteCadastrado);
			preferenciaClienteService.save(preferenciaCliente);
		}
		return getUsuario(cliente.getLogin(), cliente.getSenha());
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<?> update(@RequestBody Cliente perfil) {

		clienteService.save(perfil);

		return new ResponseEntity<Cliente>(perfil, HttpStatus.OK);
	}

}