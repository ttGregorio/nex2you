package br.com.nex2you.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.nex2you.model.CategoriaProduto;
import br.com.nex2you.model.Cliente;
import br.com.nex2you.model.ContaCliente;
import br.com.nex2you.model.ContaClienteItem;
import br.com.nex2you.model.Empresa;
import br.com.nex2you.model.Produto;
import br.com.nex2you.model.Usuario;
import br.com.nex2you.service.CategoriaProdutoService;
import br.com.nex2you.service.ClienteService;
import br.com.nex2you.service.ContaClienteItemService;
import br.com.nex2you.service.ContaClienteService;
import br.com.nex2you.service.ProdutoService;
import br.com.nex2you.utils.Nex2YouController;
import br.com.nex2you.vo.ContaClienteItemListVO;
import br.com.nex2you.vo.ContaClienteListVO;

;

@Controller
@RequestMapping(value = "/protected/contasClientes")
public class ContasClientesController extends Nex2YouController {

	@Autowired
	private CategoriaProdutoService categoriaProdutoService;

	@Autowired
	private ProdutoService produtoService;

	@Autowired
	private ContaClienteService contaClienteService;

	@Autowired
	private ContaClienteItemService contaClienteItemService;

	@Autowired
	private ClienteService clienteService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView welcome() {
		return new ModelAndView("contasClientesList");
	}

	@RequestMapping(value = "/buscarProduto/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getProduto(@PathVariable("id") int id,
			@RequestParam int page, Locale locale) {
		Produto produto = produtoService.findById(id);

		return new ResponseEntity<Produto>(produto, HttpStatus.OK);
	}

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> listAll(HttpServletRequest request,
			@RequestParam int page, Locale locale) {
		HttpSession session = request.getSession();

		Usuario user = (Usuario) session.getAttribute("user");

		return createListAllResponse(page, locale, user.getEmpresa());
	}

	@RequestMapping(value = "/listProdutos/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> listProdutos(@PathVariable int id,
			@RequestParam int page, Locale locale) {

		CategoriaProduto categoriaProduto = categoriaProdutoService
				.findById(id);
		return new ResponseEntity<List<Produto>>(
				produtoService.findByCategoriaProduto(categoriaProduto),
				HttpStatus.OK);
	}

	@RequestMapping(value = "/findByCliente/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> listByCliente(@PathVariable int id) {
		return new ResponseEntity<List<ContaCliente>>(
				contaClienteService.findByCliente(clienteService.findById(id)),
				HttpStatus.OK);
	}

	@RequestMapping(value = "/findByContaCliente/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> listItensByConta(@PathVariable int id) {
		return new ResponseEntity<List<ContaClienteItem>>(
				contaClienteItemService.findByContaCliente(contaClienteService
						.findById(id)), HttpStatus.OK);
	}

	@RequestMapping(value = "/sumProdutos/{id},{produto},{quantidade}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getValorSomaProdutos(@PathVariable int id,
			@PathVariable int produto, @PathVariable int quantidade,
			@RequestParam int page, Locale locale) {

		ContaCliente contaCliente = contaClienteService.findById(id);

		Produto produtoLocalizado = produtoService.findById(produto);

		ContaClienteItem contaClienteItem = new ContaClienteItem();
		contaClienteItem.setContaCliente(contaCliente);
		contaClienteItem.setProduto(produtoLocalizado);
		contaClienteItem.setQuantidade(quantidade);
		contaClienteItem.setValor(contaClienteItem.getQuantidade()
				* contaClienteItem.getProduto().getPrecoVenda());

		return new ResponseEntity<ContaClienteItem>(contaClienteItem,
				HttpStatus.OK);
	}

	@RequestMapping(value = "/listCategorias", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> listCategoriasProdutos(HttpServletRequest request,
			@RequestParam int page, Locale locale) {
		HttpSession session = request.getSession();

		Usuario user = (Usuario) session.getAttribute("user");

		return new ResponseEntity<List<CategoriaProduto>>(
				categoriaProdutoService.findAll(0, 1000, user.getEmpresa())
						.getContacts(), HttpStatus.OK);
	}

	@RequestMapping(value = "/listItens/{id}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> listAllItens(
			@PathVariable("id") int contaClienteId, @RequestParam int page,
			Locale locale) {
		ContaCliente contaCliente = contaClienteService
				.findById(contaClienteId);
		return createListAllResponseItens(page, locale, null, contaCliente);
	}

	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> create(
			@RequestBody ContaCliente contaCliente,
			@RequestParam(required = false) String searchFor,
			@RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
			Locale locale, HttpServletRequest request) {
		HttpSession session = request.getSession();

		Usuario user = (Usuario) session.getAttribute("user");
		contaCliente.setEmpresa(user.getEmpresa());

		contaClienteService.save(contaCliente);
		/*
		 * if (isSearchActivated(searchFor)) { return search(searchFor, page,
		 * locale, "message.create.success", user.getEmpresa()); }
		 */
		return listAll(request, page, locale);

	}

	@RequestMapping(value = "/insertItem", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> createItem(
			@RequestBody ContaClienteItem contaClienteItem,
			@RequestParam(required = false) String searchFor,
			@RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
			Locale locale, HttpServletRequest request) {
		contaClienteItemService.save(contaClienteItem);

		Produto produto = produtoService.findById(contaClienteItem.getProduto()
				.getId());
		produto.setEstoque(produto.getEstoque()
				- contaClienteItem.getQuantidade());
		produtoService.save(produto);

		atualizarValor(
				contaClienteItem.getContaCliente(),
				contaClienteItemService.findAll(0, 10000,
						contaClienteItem.getContaCliente()).getContacts());
		return listAllItens(contaClienteItem.getContaCliente().getId(), page,
				locale);

	}

	@RequestMapping(value = "/insertItem/{cliente}/{produto}/{quantidade}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> createItemCliente(@PathVariable int cliente,
			@PathVariable int produto, @PathVariable int quantidade) {
		Cliente cliente2 = clienteService.findById(cliente);
		ContaClienteItem contaClienteItem = new ContaClienteItem();
		contaClienteItem.setProduto(produtoService.findById(produto));
		contaClienteItem.setQuantidade(quantidade);
		contaClienteItem.setValor(contaClienteItem.getQuantidade()
				* contaClienteItem.getProduto().getPrecoVenda());

		ContaCliente contaCliente = contaClienteService
				.findByClienteAndEmpresa(cliente2, contaClienteItem
						.getProduto().getEmpresa());

		if (contaCliente == null) {
			contaCliente = new ContaCliente();
			contaCliente.setCliente(cliente2);
			contaCliente.setEmpresa(contaClienteItem.getProduto().getEmpresa());
			contaCliente.setPedidoAberto(true);
			contaCliente.setValor(contaClienteItem.getQuantidade()
					* contaClienteItem.getProduto().getPrecoVenda());
			contaClienteService.save(contaCliente);
			contaCliente = contaClienteService.findByClienteAndEmpresa(
					cliente2, contaClienteItem.getProduto().getEmpresa());
		} else {
			contaCliente.setValor(contaCliente.getValor()
					+ (contaClienteItem.getQuantidade() * contaClienteItem
							.getProduto().getPrecoVenda()));
			contaCliente.setPedidoAberto(true);
			contaClienteService.save(contaCliente);
		}
		contaClienteItem.setContaCliente(contaCliente);

		contaClienteItemService.save(contaClienteItem);

		contaClienteItem.getProduto().setEstoque(
				contaClienteItem.getProduto().getEstoque()
						- contaClienteItem.getQuantidade());
		produtoService.save(contaClienteItem.getProduto());
		return new ResponseEntity<ContaClienteItem>(contaClienteItem,
				HttpStatus.OK);
	}

	@RequestMapping(value = "/updateItem/{id}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<?> updateItem(
			@PathVariable("id") int contaClienteId,
			@RequestBody ContaClienteItem contaCliente,
			@RequestParam(required = false) String searchFor,
			@RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
			Locale locale, HttpServletRequest request) {
		contaClienteItemService.save(contaCliente);
		atualizarValor(contaCliente.getContaCliente(), contaClienteItemService
				.findAll(0, 10000, contaCliente.getContaCliente())
				.getContacts());
		return listAllItens(contaCliente.getContaCliente().getId(), page,
				locale);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<?> update(
			@PathVariable("id") int contaClienteId,
			@RequestBody ContaCliente contaCliente,
			@RequestParam(required = false) String searchFor,
			@RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
			Locale locale, HttpServletRequest request) {
		HttpSession session = request.getSession();

		Usuario user = (Usuario) session.getAttribute("user");
		if (contaClienteId != contaCliente.getId()) {
			return new ResponseEntity<String>("Bad Request",
					HttpStatus.BAD_REQUEST);
		}

		contaClienteService.save(contaCliente);

		return createListAllResponse(page, locale, "message.update.success",
				user.getEmpresa());
	}

	@RequestMapping(value = "/deleteItem/{contaClienteId}", method = RequestMethod.DELETE, produces = "application/json")
	public ResponseEntity<?> deleteItem(
			@PathVariable("contaClienteId") int contaClienteId,
			@RequestParam(required = false) String searchFor,
			@RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
			Locale locale, HttpServletRequest request) {
		ContaClienteItem contaClienteItem = contaClienteItemService
				.findById(contaClienteId);
		try {
			ContaClienteItem contaCliente = contaClienteItemService
					.findById(contaClienteId);
			contaClienteItemService.delete(contaClienteId);
			atualizarValor(
					contaCliente.getContaCliente(),
					contaClienteItemService.findAll(0, 10000,
							contaCliente.getContaCliente()).getContacts());
		} catch (AccessDeniedException e) {
			return new ResponseEntity<Object>(HttpStatus.FORBIDDEN);
		}

		return listAllItens(contaClienteItem.getContaCliente().getId(), page,
				locale);
	}

	@RequestMapping(value = "/{contaClienteId}", method = RequestMethod.DELETE, produces = "application/json")
	public ResponseEntity<?> delete(
			@PathVariable("contaClienteId") int contaClienteId,
			@RequestParam(required = false) String searchFor,
			@RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
			Locale locale, HttpServletRequest request) {
		HttpSession session = request.getSession();

		Usuario user = (Usuario) session.getAttribute("user");

		try {
			contaClienteService.delete(contaClienteId);
		} catch (AccessDeniedException e) {
			return new ResponseEntity<Object>(HttpStatus.FORBIDDEN);
		}

		return createListAllResponse(page, locale, "message.delete.success",
				user.getEmpresa());
	}

	private ContaClienteListVO listAll(int page, Empresa empresa) {
		return contaClienteService.findAll(page, maxResults, empresa);
	}

	private ContaClienteItemListVO listAllItens(int page,
			ContaCliente contaCliente) {
		return contaClienteItemService.findAll(page, maxResults, contaCliente);
	}

	private ResponseEntity<ContaClienteListVO> returnListToUser(
			ContaClienteListVO contaClienteList) {
		return new ResponseEntity<ContaClienteListVO>(contaClienteList,
				HttpStatus.OK);
	}

	private ResponseEntity<List<ContaClienteItem>> returnListToUserItem(
			ContaClienteItemListVO contaClienteList) {
		return new ResponseEntity<List<ContaClienteItem>>(
				contaClienteList.getContacts(), HttpStatus.OK);
	}

	private ResponseEntity<?> createListAllResponse(int page, Locale locale,
			Empresa empresa) {
		return createListAllResponse(page, locale, null, empresa);
	}

	private ResponseEntity<?> createListAllResponse(int page, Locale locale,
			String messageKey, Empresa empresa) {
		ContaClienteListVO contaClienteListVO = listAll(page, empresa);

		addActionMessageToVO(contaClienteListVO, locale, messageKey, null);

		return returnListToUser(contaClienteListVO);
	}

	private ResponseEntity<?> createListAllResponseItens(int page,
			Locale locale, String messageKey, ContaCliente contaCliente) {
		ContaClienteItemListVO contaClienteListVO = listAllItens(page,
				contaCliente);

		addActionMessageToVOItem(contaClienteListVO, locale, messageKey, null);

		return returnListToUserItem(contaClienteListVO);
	}

	private ContaClienteListVO addActionMessageToVO(
			ContaClienteListVO contaClienteListVO, Locale locale,
			String actionMessageKey, Object[] args) {
		if (StringUtils.isEmpty(actionMessageKey)) {
			return contaClienteListVO;
		}

		contaClienteListVO.setActionMessage(messageSource.getMessage(
				actionMessageKey, args, null, locale));

		return contaClienteListVO;
	}

	private ContaClienteItemListVO addActionMessageToVOItem(
			ContaClienteItemListVO contaClienteListVO, Locale locale,
			String actionMessageKey, Object[] args) {
		if (StringUtils.isEmpty(actionMessageKey)) {
			return contaClienteListVO;
		}

		contaClienteListVO.setActionMessage(messageSource.getMessage(
				actionMessageKey, args, null, locale));

		return contaClienteListVO;
	}

	private void atualizarValor(ContaCliente contaCliente,
			List<ContaClienteItem> lista) {
		contaCliente.setValor(0);
		for (ContaClienteItem contaClienteItem : lista) {
			contaCliente.setValor(contaCliente.getValor()
					+ contaClienteItem.getValor());
		}
		contaClienteService.save(contaCliente);
	}
}