package br.com.nex2you.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import br.com.nex2you.model.CategoriaEmpresa;
import br.com.nex2you.model.Cliente;
import br.com.nex2you.model.PreferenciaCliente;
import br.com.nex2you.service.CategoriaEmpresaService;
import br.com.nex2you.service.ClienteService;
import br.com.nex2you.service.PreferenciaClienteService;
import br.com.nex2you.utils.Nex2YouController;

@Controller
@RequestMapping(value = "/protected/preferenciasClientes")
public class PreferenciasClientesController extends Nex2YouController {

	@Autowired
	private PreferenciaClienteService preferenciaClienteService;

	@Autowired
	private ClienteService clienteService;

	@Autowired
	private CategoriaEmpresaService categoriaService;

	@RequestMapping(value = "/{cliente}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getPreferencia(@PathVariable int cliente) {
		Cliente cliente2 = clienteService.findById(cliente);
		ResponseEntity<List<PreferenciaCliente>> resposta = new ResponseEntity<List<PreferenciaCliente>>(
				preferenciaClienteService.findByCliente(cliente2),
				HttpStatus.OK);
		return resposta;
	}

	@RequestMapping(value = "/{cliente}/{preferencia}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> postPreferencia(@PathVariable int cliente,
			@PathVariable int preferencia) {
		PreferenciaCliente preferenciaCliente = preferenciaClienteService
				.findByClienteAndCategoria(clienteService.findById(cliente),
						categoriaService.findById(preferencia));
		if(preferenciaCliente!=null){
			preferenciaClienteService.delete(preferenciaCliente.getId());
		}else{
			preferenciaCliente = new PreferenciaCliente();
			preferenciaCliente.setCliente(clienteService.findById(cliente));
			preferenciaCliente.setCategoria(categoriaService.findById(preferencia));
			preferenciaClienteService.save(preferenciaCliente);			
		}
		return getPreferencia(cliente);
	}

	@RequestMapping(value = "/{user}/{preferencia}", method = RequestMethod.DELETE, produces = "application/json")
	public ResponseEntity<?> deletePreferencia(@PathVariable int cliente,
			@PathVariable int preferencia) {
		Cliente cliente2 = clienteService.findById(cliente);
		CategoriaEmpresa categoria = categoriaService.findById(preferencia);
		PreferenciaCliente preferenciaCliente = preferenciaClienteService
				.findByClienteAndCategoria(cliente2, categoria);
		preferenciaClienteService.delete(preferenciaCliente.getId());
		return getPreferencia(cliente);
	}

}