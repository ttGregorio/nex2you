package br.com.nex2you.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import br.com.nex2you.model.Empresa;
import br.com.nex2you.service.CategoriaEmpresaService;
import br.com.nex2you.service.ClienteService;
import br.com.nex2you.service.EmpresaService;
import br.com.nex2you.service.PreferenciaClienteService;
import br.com.nex2you.utils.Nex2YouController;

@Controller
@RequestMapping(value = "/protected/empresas")
public class EmpresasController extends Nex2YouController {

	@Autowired
	private EmpresaService empresaService;

	@Autowired
	private CategoriaEmpresaService categoriaEmpresaService;

	@Autowired
	private ClienteService clienteService;

	@Autowired
	private PreferenciaClienteService preferenciaClienteService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView welcome() {
		return new ModelAndView("minhaEmpresaList");
	}

	@RequestMapping(value = "/{cliente}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> listByCliente(@PathVariable int cliente) {

		List<Empresa> empresas = empresaService.findByCliente(cliente);

		ResponseEntity<List<Empresa>> resposta = new ResponseEntity<List<Empresa>>(
				empresas, HttpStatus.OK);
		return resposta;
	}

	@RequestMapping(value = "/{id}/id", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> findById(@PathVariable int id) {

		ResponseEntity<Empresa> resposta = new ResponseEntity<Empresa>(
				empresaService.findById(id), HttpStatus.OK);
		return resposta;
	}
}