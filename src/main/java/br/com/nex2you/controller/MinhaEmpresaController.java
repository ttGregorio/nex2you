package br.com.nex2you.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import br.com.nex2you.model.CategoriaEmpresa;
import br.com.nex2you.model.Empresa;
import br.com.nex2you.model.Usuario;
import br.com.nex2you.service.CategoriaEmpresaService;
import br.com.nex2you.service.EmpresaService;
import br.com.nex2you.utils.Nex2YouController;

@Controller
@RequestMapping(value = "/protected/minhaEmpresa")
public class MinhaEmpresaController extends Nex2YouController {

	@Autowired
	private EmpresaService empresaService;

	@Autowired
	private CategoriaEmpresaService categoriaEmpresaService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView welcome() {
		return new ModelAndView("minhaEmpresaList");
	}

	@RequestMapping(value = "/listCategorias", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> listCategorias() {
		ResponseEntity<List<CategoriaEmpresa>> resposta = new ResponseEntity<List<CategoriaEmpresa>>(
				categoriaEmpresaService.findAll(), HttpStatus.OK);
		return resposta;
	}

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> listAll(HttpServletRequest request,
			@RequestParam int page, Locale locale) {
		HttpSession session = request.getSession();

		Usuario user = (Usuario) session.getAttribute("user");

		return createListAllResponse(page, locale, user);
	}

	@RequestMapping(value = "/singleSave", method = RequestMethod.POST, consumes = { "multipart/form-data" })
	public ResponseEntity<?> singleSave(HttpServletRequest request,
			@RequestPart("file") MultipartFile file) {
		byte[] bytes = null;
		if (!file.isEmpty()) {
			try {
				bytes = file.getBytes();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		HttpSession session = request.getSession();
		session.setAttribute("logo", bytes);
		ResponseEntity<byte[]> resposta = new ResponseEntity<byte[]>(bytes,
				HttpStatus.OK);
		return resposta;
	}

	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> create(
			@RequestBody Empresa perfil,
			@RequestParam(required = false) String searchFor,
			@RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
			Locale locale, HttpServletRequest request) {
		HttpSession session = request.getSession();
		Usuario user = (Usuario) session.getAttribute("user");
		perfil.setAtivo(true);
		perfil.setLogo((byte[]) session.getAttribute("logo"));

		empresaService.save(perfil);

		return createListAllResponse(page, locale, "message.create.success",
				user);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<?> update(
			HttpServletRequest request,
			@PathVariable("id") int perfilId,
			@RequestBody Empresa perfil,
			@RequestParam(required = false) String searchFor,
			@RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
			Locale locale) {
		HttpSession session = request.getSession();

		Usuario user = (Usuario) session.getAttribute("user");

		if (perfilId != perfil.getId()) {
			return new ResponseEntity<String>("Bad Request",
					HttpStatus.BAD_REQUEST);
		}

		empresaService.save(perfil);

		return createListAllResponse(page, locale, "message.update.success",
				user);
	}

	private ResponseEntity<Empresa> returnListToUser(Empresa empresa) {
		return new ResponseEntity<Empresa>(empresa, HttpStatus.OK);
	}

	private ResponseEntity<?> createListAllResponse(int page, Locale locale,
			Usuario usuario) {
		return createListAllResponse(page, locale, null, usuario);
	}

	private ResponseEntity<?> createListAllResponse(int page, Locale locale,
			String messageKey, Usuario usuario) {
		Empresa empresa = empresaService.findById(usuario.getEmpresa().getId());

		return returnListToUser(empresa);
	}

}