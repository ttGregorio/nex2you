package br.com.nex2you.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import br.com.nex2you.model.CategoriaProduto;
import br.com.nex2you.model.Empresa;
import br.com.nex2you.model.Produto;
import br.com.nex2you.model.ProdutoImagem;
import br.com.nex2you.model.Usuario;
import br.com.nex2you.service.CategoriaProdutoService;
import br.com.nex2you.service.ProdutoImagemService;
import br.com.nex2you.service.ProdutoService;
import br.com.nex2you.utils.Nex2YouController;
import br.com.nex2you.vo.ProdutoListVO;

@Controller
@RequestMapping(value = "/protected/produtos")
public class ProdutosController extends Nex2YouController {

	@Autowired
	private ProdutoService produtoService;

	@Autowired
	private ProdutoImagemService produtoImagemService;

	@Autowired
	private CategoriaProdutoService categoriaProdutoService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView welcome() {
		return new ModelAndView("produtosList");
	}

	@RequestMapping(value = "/{categoria}/categoria", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> listByCategoria(@PathVariable int categoria) {
		CategoriaProduto categoriaProduto = categoriaProdutoService
				.findById(categoria);

		return new ResponseEntity<List<Produto>>(
				produtoService.findByCategoriaProduto(categoriaProduto),
				HttpStatus.OK);

	}

	@RequestMapping(value = "/{id}/id", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> listById(@PathVariable int id) {
		return new ResponseEntity<Produto>(produtoService.findById(id),
				HttpStatus.OK);

	}

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> listAll(HttpServletRequest request,
			@RequestParam int page, Locale locale) {
		HttpSession session = request.getSession();

		Usuario user = (Usuario) session.getAttribute("user");

		return createListAllResponse(page, locale, user.getEmpresa());
	}

	@RequestMapping(value = "/listCategorias", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> listCategorias(HttpServletRequest request) {
		HttpSession session = request.getSession();

		Usuario user = (Usuario) session.getAttribute("user");
		ResponseEntity<List<CategoriaProduto>> resposta = new ResponseEntity<List<CategoriaProduto>>(
				categoriaProdutoService.findAll(0, 10000, user.getEmpresa())
						.getContacts(), HttpStatus.OK);
		return resposta;
	}

	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> create(
			@RequestBody Produto produto,
			@RequestParam(required = false) String searchFor,
			@RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
			Locale locale, HttpServletRequest request) {
		HttpSession session = request.getSession();

		Usuario user = (Usuario) session.getAttribute("user");
		produto.setAtivo(true);
		produto.setEmpresa(user.getEmpresa());

		produtoService.save(produto);

		if (isSearchActivated(searchFor)) {
			return search(searchFor, page, locale, "message.create.success",
					user.getEmpresa());
		}

		return createListAllResponse(page, locale, "message.create.success",
				user.getEmpresa());
	}

	@RequestMapping(value = "/idImage", method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> createImage(
			@RequestBody Produto produto,
			@RequestParam(required = false) String searchFor,
			@RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
			Locale locale, HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.setAttribute("idProduto", produto);

		Usuario user = (Usuario) session.getAttribute("user");

		return createListAllResponse(page, locale, "message.create.success",
				user.getEmpresa());
	}

	@RequestMapping(value = "/getImagensProduto", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getImagensProduto(
			@RequestParam(required = false) String searchFor,
			@RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
			Locale locale, HttpServletRequest request) {
		HttpSession session = request.getSession();

		ResponseEntity<List<ProdutoImagem>> resposta = new ResponseEntity<List<ProdutoImagem>>(
				produtoImagemService.findAll((Produto) session
						.getAttribute("idProduto")), HttpStatus.OK);
		return resposta;
	}

	@RequestMapping(value = "/imagensProduto/{produtoID}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> getImagensByProduto(@PathVariable int produtoID) {
		Produto produto = produtoService.findById(produtoID);
		ResponseEntity<List<ProdutoImagem>> resposta = new ResponseEntity<List<ProdutoImagem>>(
				produtoImagemService.findAll(produto), HttpStatus.OK);
		return resposta;
	}

	@RequestMapping(value = "/deleteProdutoImagem/{produtoImagemId},{produto}", method = RequestMethod.DELETE, produces = "application/json")
	public ResponseEntity<?> deleteDepoimentoImagem(
			@PathVariable("produtoImagemId") int produtoImagemId,
			@PathVariable("produto") int produto,
			@RequestParam(required = false) String searchFor,
			@RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
			Locale locale, HttpServletRequest request) {

		try {
			produtoImagemService.delete(produtoImagemId);
		} catch (AccessDeniedException e) {
			return new ResponseEntity<Object>(HttpStatus.FORBIDDEN);
		}

		return getImagensProduto(searchFor, page, locale, request);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<?> update(
			@PathVariable("id") int perfilId,
			@RequestBody Produto perfil,
			@RequestParam(required = false) String searchFor,
			@RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
			Locale locale, HttpServletRequest request) {
		HttpSession session = request.getSession();

		Usuario user = (Usuario) session.getAttribute("user");
		if (perfilId != perfil.getId()) {
			return new ResponseEntity<String>("Bad Request",
					HttpStatus.BAD_REQUEST);
		}

		produtoService.save(perfil);

		if (isSearchActivated(searchFor)) {
			return search(searchFor, page, locale, "message.update.success",
					user.getEmpresa());
		}

		return createListAllResponse(page, locale, "message.update.success",
				user.getEmpresa());
	}

	@RequestMapping(value = "/{perfilId}", method = RequestMethod.DELETE, produces = "application/json")
	public ResponseEntity<?> delete(
			@PathVariable("perfilId") int perfilId,
			@RequestParam(required = false) String searchFor,
			@RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
			Locale locale, HttpServletRequest request) {
		HttpSession session = request.getSession();

		Usuario user = (Usuario) session.getAttribute("user");

		try {
			produtoService.delete(perfilId);
		} catch (AccessDeniedException e) {
			return new ResponseEntity<Object>(HttpStatus.FORBIDDEN);
		}

		if (isSearchActivated(searchFor)) {
			return search(searchFor, page, locale, "message.delete.success",
					user.getEmpresa());
		}

		return createListAllResponse(page, locale, "message.delete.success",
				user.getEmpresa());
	}

	@RequestMapping(value = "/{name}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> search(
			@PathVariable("name") String name,
			@RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
			Locale locale, HttpServletRequest request) {
		HttpSession session = request.getSession();

		Usuario user = (Usuario) session.getAttribute("user");

		return search(name, page, locale, null, user.getEmpresa());
	}

	@RequestMapping(value = "/produtoSave", method = RequestMethod.POST, consumes = { "multipart/form-data" })
	public ResponseEntity<?> singleSave(
			@RequestPart("file") MultipartFile file,
			@RequestParam("desc") String desc, HttpServletRequest request) {
		if (!file.isEmpty()) {
			try {
				HttpSession session = request.getSession();
				ProdutoImagem produtoImagem = new ProdutoImagem();
				produtoImagem.setDescricao(desc);
				produtoImagem.setImagem(file.getBytes());
				produtoImagem.setProduto((Produto) session
						.getAttribute("idProduto"));

				produtoImagemService.save(produtoImagem);

				session.setAttribute("idProduto", null);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		ResponseEntity<String> resposta = new ResponseEntity<String>("ok",
				HttpStatus.OK);
		return resposta;
	}

	private ResponseEntity<?> search(String name, int page, Locale locale,
			String actionMessageKey, Empresa empresa) {
		ProdutoListVO perfilListVO = produtoService.findByNomeLike(page,
				maxResults, name, empresa);

		if (!StringUtils.isEmpty(actionMessageKey)) {
			addActionMessageToVO(perfilListVO, locale, actionMessageKey, null);
		}

		Object[] args = { name };

		addSearchMessageToVO(perfilListVO, locale, "message.search.for.active",
				args);

		return new ResponseEntity<ProdutoListVO>(perfilListVO, HttpStatus.OK);
	}

	private ProdutoListVO listAll(int page, Empresa empresa) {
		return produtoService.findAll(page, maxResults, empresa);
	}

	private ResponseEntity<ProdutoListVO> returnListToUser(
			ProdutoListVO perfilList) {
		return new ResponseEntity<ProdutoListVO>(perfilList, HttpStatus.OK);
	}

	private ResponseEntity<?> createListAllResponse(int page, Locale locale,
			Empresa empresa) {
		return createListAllResponse(page, locale, null, empresa);
	}

	private ResponseEntity<?> createListAllResponse(int page, Locale locale,
			String messageKey, Empresa empresa) {
		ProdutoListVO perfilListVO = listAll(page, empresa);

		addActionMessageToVO(perfilListVO, locale, messageKey, null);

		return returnListToUser(perfilListVO);
	}

	private ProdutoListVO addActionMessageToVO(ProdutoListVO perfilListVO,
			Locale locale, String actionMessageKey, Object[] args) {
		if (StringUtils.isEmpty(actionMessageKey)) {
			return perfilListVO;
		}

		perfilListVO.setActionMessage(messageSource.getMessage(
				actionMessageKey, args, null, locale));

		return perfilListVO;
	}

	private ProdutoListVO addSearchMessageToVO(ProdutoListVO perfilListVO,
			Locale locale, String actionMessageKey, Object[] args) {
		if (StringUtils.isEmpty(actionMessageKey)) {
			return perfilListVO;
		}

		perfilListVO.setSearchMessage(messageSource.getMessage(
				actionMessageKey, args, null, locale));

		return perfilListVO;
	}
}