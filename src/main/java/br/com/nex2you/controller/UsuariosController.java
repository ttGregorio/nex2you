package br.com.nex2you.controller;

import java.util.List;
import java.util.Locale;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import br.com.nex2you.model.Empresa;
import br.com.nex2you.model.Perfil;
import br.com.nex2you.model.Role;
import br.com.nex2you.model.User;
import br.com.nex2you.model.Usuario;
import br.com.nex2you.service.EstadoService;
import br.com.nex2you.service.PerfilService;
import br.com.nex2you.service.UserService;
import br.com.nex2you.service.UsuarioService;
import br.com.nex2you.utils.Nex2YouController;
import br.com.nex2you.vo.UsuarioListVO;

/**
 * Controller da tela de gestão de usuários internos
 * 
 * @author Thiago Gregorio
 * 
 */
@Controller
@RequestMapping(value = "/protected/usuarios")
public class UsuariosController extends Nex2YouController {

	@Autowired
	private UsuarioService usuarioService;

	@Autowired
	private UserService userService;

	@Autowired
	private PerfilService perfilService;

	@Autowired
	private EstadoService estadoService;

	@RequestMapping(method = RequestMethod.GET)
	public ModelAndView welcome() {
		return new ModelAndView("usuariosList");
	}

	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> listAll(HttpServletRequest request,
			@RequestParam int page, Locale locale) {
		HttpSession session = request.getSession();
		Usuario user = (Usuario) session.getAttribute("user");
		return createListAllResponse(page, locale, user.getEmpresa());
	}

	@RequestMapping(value = "/listPerfis", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> listPerfis(HttpServletRequest request) {
		HttpSession session = request.getSession();
		Usuario user = (Usuario) session.getAttribute("user");
		ResponseEntity<List<Perfil>> resposta = new ResponseEntity<List<Perfil>>(
				perfilService.findByEmpresa(user.getEmpresa()), HttpStatus.OK);
		return resposta;
	}

	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseEntity<?> create(
			HttpServletRequest request,
			@RequestBody Usuario usuario,
			@RequestParam(required = false) String searchFor,
			@RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
			Locale locale) {
		HttpSession session = request.getSession();
		Usuario usuarioSistema = (Usuario) session.getAttribute("user");
		usuario.setAtivo(true);
		usuario.setEmpresa(usuarioSistema.getEmpresa());
		usuarioService.save(usuario);

		if (isSearchActivated(searchFor)) {
			return search(searchFor, page, locale, "message.create.success",
					usuarioSistema.getEmpresa());
		}

		User user = new User();
		user.setEmail(usuario.getLogin());
		user.setEmpresa(usuarioSistema.getEmpresa());
		user.setEnabled("1");
		user.setName(usuario.getNome());
		user.setPassword(usuario.getSenha());
		user.setRole(Role.ROLE_ADMIN);

		userService.save(user);

		return createListAllResponse(page, locale, "message.create.success",
				usuarioSistema.getEmpresa());
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json")
	public ResponseEntity<?> update(
			HttpServletRequest request,
			@PathVariable("id") int usuarioId,
			@RequestBody Usuario usuario,
			@RequestParam(required = false) String searchFor,
			@RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
			Locale locale) {
		HttpSession session = request.getSession();
		Usuario usuarioSistema = (Usuario) session.getAttribute("user");
		if (usuarioId != usuario.getId()) {
			return new ResponseEntity<String>("Bad Request",
					HttpStatus.BAD_REQUEST);
		}

		usuarioService.save(usuario);

		if (isSearchActivated(searchFor)) {
			return search(searchFor, page, locale, "message.update.success",
					usuarioSistema.getEmpresa());
		}

		User user = new User();
		user.setEmail(usuario.getLogin());
		user.setEmpresa(usuarioSistema.getEmpresa());
		user.setEnabled("1");
		user.setName(usuario.getNome());
		user.setPassword(usuario.getSenha());
		user.setRole(Role.ROLE_ADMIN);

		userService.save(user);

		return createListAllResponse(page, locale, "message.update.success",
				usuarioSistema.getEmpresa());
	}

	@RequestMapping(value = "/{usuarioId}", method = RequestMethod.DELETE, produces = "application/json")
	public ResponseEntity<?> delete(
			HttpServletRequest request,
			@PathVariable("usuarioId") int usuarioId,
			@RequestParam(required = false) String searchFor,
			@RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
			Locale locale) {
		HttpSession session = request.getSession();
		Usuario user = (Usuario) session.getAttribute("user");

		try {
			usuarioService.delete(usuarioId);
		} catch (AccessDeniedException e) {
			return new ResponseEntity<Object>(HttpStatus.FORBIDDEN);
		}

		if (isSearchActivated(searchFor)) {
			return search(searchFor, page, locale, "message.delete.success",
					user.getEmpresa());
		}

		return createListAllResponse(page, locale, "message.delete.success",
				user.getEmpresa());
	}

	@RequestMapping(value = "/{name}", method = RequestMethod.GET, produces = "application/json")
	public ResponseEntity<?> search(
			HttpServletRequest request,
			@PathVariable("name") String name,
			@RequestParam(required = false, defaultValue = DEFAULT_PAGE_DISPLAYED_TO_USER) int page,
			Locale locale) {
		HttpSession session = request.getSession();
		Usuario user = (Usuario) session.getAttribute("user");
		return search(name, page, locale, null, user.getEmpresa());
	}

	private ResponseEntity<?> search(String name, int page, Locale locale,
			String actionMessageKey, Empresa empresa) {
		UsuarioListVO usuarioListVO = usuarioService.findByNomeLike(page,
				maxResults, name, empresa);

		if (!StringUtils.isEmpty(actionMessageKey)) {
			addActionMessageToVO(usuarioListVO, locale, actionMessageKey, null);
		}

		Object[] args = { name };

		addSearchMessageToVO(usuarioListVO, locale,
				"message.search.for.active", args);

		return new ResponseEntity<UsuarioListVO>(usuarioListVO, HttpStatus.OK);
	}

	private UsuarioListVO listAll(int page, Empresa empresa) {
		return usuarioService.findAll(page, maxResults, true, empresa);
	}

	private ResponseEntity<UsuarioListVO> returnListToUser(
			UsuarioListVO usuarioList) {
		return new ResponseEntity<UsuarioListVO>(usuarioList, HttpStatus.OK);
	}

	private ResponseEntity<?> createListAllResponse(int page, Locale locale,
			Empresa empresa) {
		return createListAllResponse(page, locale, null, empresa);
	}

	private ResponseEntity<?> createListAllResponse(int page, Locale locale,
			String messageKey, Empresa empresa) {
		UsuarioListVO usuarioListVO = listAll(page, empresa);

		addActionMessageToVO(usuarioListVO, locale, messageKey, null);

		return returnListToUser(usuarioListVO);
	}

	private UsuarioListVO addActionMessageToVO(UsuarioListVO usuarioListVO,
			Locale locale, String actionMessageKey, Object[] args) {
		if (StringUtils.isEmpty(actionMessageKey)) {
			return usuarioListVO;
		}

		usuarioListVO.setActionMessage(messageSource.getMessage(
				actionMessageKey, args, null, locale));

		return usuarioListVO;
	}

	private UsuarioListVO addSearchMessageToVO(UsuarioListVO usuarioListVO,
			Locale locale, String actionMessageKey, Object[] args) {
		if (StringUtils.isEmpty(actionMessageKey)) {
			return usuarioListVO;
		}

		usuarioListVO.setSearchMessage(messageSource.getMessage(
				actionMessageKey, args, null, locale));

		return usuarioListVO;
	}
}