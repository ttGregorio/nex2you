package br.com.nex2you.utils;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class Nex2YouDAO {

	
	/**
	 * M�todo de execu��o de consultas.
	 * 
	 * @param query
	 *            Query da consulta.
	 * @param listaParametros
	 *            Lista de par�metros da consulta.
	 * @return ResultSet contendo os resultados da consulta.
	 */
	public static ResultSet executaConsulta(StringBuffer query,
			List<Object> listaParametros, Connection con) {
	
		ResultSet rs = null;

		if (listaParametros == null) {
			listaParametros = new ArrayList<Object>();
		}

		try {
			PreparedStatement pstmt = con.prepareStatement(query.toString());
			for (int i = 0; i < listaParametros.size(); i++) {
				pstmt.setObject(i + 1, listaParametros.get(i));
			}

			rs = pstmt.executeQuery();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {

		}

		return rs;
	}

}
