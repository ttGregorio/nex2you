/**
 * 
 */
package br.com.nex2you.utils;

import java.io.InputStream;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;

/**
 * @author Thiago Gregorio
 * 
 */
public class Nex2YouController {

	protected static final String DEFAULT_PAGE_DISPLAYED_TO_USER = "0";

	@Value("5")
	protected int maxResults;

	@Autowired
	protected MessageSource messageSource;

	/**
	 * URL do FTP para armazenamento de arquivos.
	 */
	public static final String URL_FTP = "ftp.nex2you.com.br";

	/**
	 * Usu�rio do FTP.
	 */
	public static final String USUARIO_FTP = "nex2you";

	/**
	 * Senha do FTP.
	 */
	public static final String SENHA_FTP = "nYe4x@2Xyou";

	private String diretorio;

	protected boolean isSearchActivated(String searchFor) {
		return !StringUtils.isEmpty(searchFor);
	}

	/**
	 * @return Getter da variável diretorio.
	 */
	public String getDiretorio() {
		return diretorio;
	}

	/**
	 * @param Setter
	 *            da variável diretorio.
	 */
	public void setDiretorio(String diretorio) {
		this.diretorio = diretorio;
	}

	/**
	 * M�todo de inser��o de dados no FTP.
	 * 
	 * @param inputStream2
	 *            Input Stream com o arquivo a ser inserido.
	 * @param nome
	 *            Nome do arquivo.
	 * @return String com o nome inserido.
	 */
	public String inserirDadosFTP(InputStream inputStream, String nome) {
		FTPClient ftpClient = new FTPClient();
		String nomeRetorno = Double
				.toString(
						Math.random() * 10000000 * Math.random()
								* Math.random() * 12365)
				.concat(nome.substring(nome.lastIndexOf("\\") + 1))
				.replace(".", "");
		try {
			ftpClient.connect(URL_FTP);
			ftpClient.login(USUARIO_FTP, SENHA_FTP);
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);
			ftpClient.enterLocalActiveMode();
			if (getDiretorio() == null) {
				ftpClient
						.changeWorkingDirectory("appservers/apache-tomcat-7.0.62/webapps/Imagens/Delphos");
			} else {
				ftpClient.changeWorkingDirectory(diretorio);
			}
			ftpClient.storeFile(nomeRetorno, inputStream);
			ftpClient.disconnect();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return nomeRetorno;
	}

}
