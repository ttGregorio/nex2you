/**
 * 
 */
package br.com.nex2you.utils;

import java.util.HashMap;
import java.util.Map;

import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

/**
 * Classe base de gera��o de relat�rios XLS..
 * 
 * @author Thiago Gregorio
 * 
 */
public abstract class RelatorioXLS {

	/**
	 * Mapa de estilos de fontes
	 */
	public Map<String, HSSFCellStyle> mapaEstilos;

	public RelatorioXLS() {
		mapaEstilos = new HashMap<String, HSSFCellStyle>();
	}

	@SuppressWarnings("deprecation")
	public void gerarEstilos(HSSFWorkbook workbook) {
		HSSFFont fonte = workbook.createFont();
		fonte.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);

		HSSFCellStyle estiloCabecalho = workbook.createCellStyle();
		estiloCabecalho.setFont(fonte);
		estiloCabecalho.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		estiloCabecalho.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		estiloCabecalho.setBorderRight(HSSFCellStyle.BORDER_THIN);
		estiloCabecalho.setBorderTop(HSSFCellStyle.BORDER_THIN);

		HSSFCellStyle estiloLinha = workbook.createCellStyle();
		estiloLinha.setBorderBottom(HSSFCellStyle.BORDER_THIN);
		estiloLinha.setBorderLeft(HSSFCellStyle.BORDER_THIN);
		estiloLinha.setBorderRight(HSSFCellStyle.BORDER_THIN);
		estiloLinha.setBorderTop(HSSFCellStyle.BORDER_THIN);

		mapaEstilos.put("cabecalho", estiloCabecalho);
		mapaEstilos.put("linha", estiloLinha);
	}

}
