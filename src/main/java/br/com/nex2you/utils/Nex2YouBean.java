package br.com.nex2you.utils;

import java.io.Serializable;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

import org.apache.log4j.Logger;

/**
 * Classe base dos beans do sistema.
 * 
 * @author Thiago Gregorio
 * 
 */
public class Nex2YouBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * Formatador de datas para o formato brasileiro de exibi��o.
	 */
	private SimpleDateFormat sdf;

	private NumberFormat formatter;

	/**
	 * Logger do DAO gen�rico.
	 */
	private static Logger logger;

	public Nex2YouBean() {
		sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm");
		formatter = NumberFormat.getCurrencyInstance();
		logger = Logger.getLogger(Nex2YouBean.class);
	}

	/**
	 * M�todo de convers�o de double par String.
	 * 
	 * @param valor
	 *            Valor em double.
	 * @return Valor em formato String.
	 */
	public String converterDoubleParaString(double valor) {
		logger.debug("Executando m�todo converterDoubleParaString");
		return formatter.format(valor).replace(".", ",").replace("R$", "").replace("$", "").trim();
	}

	/**
	 * M�todo de convers�o de String para double.
	 * 
	 * @param valor
	 *            Valor em String.
	 * @return Valor convertido em double, caso ele seja existente, 0, caso
	 *         nulo.
	 */
	public double converterStringParaDouble(String valor) {
		logger.debug("Executando m�todo converterStringParaDouble");
		if (valor != null && !valor.isEmpty()) {
			return Double.parseDouble(valor.replace(".", "").replace(",", "."));
		} else {
			return 0;
		}
	}

	/**
	 * M�todo de convers�o de datas em String.
	 * 
	 * @param data
	 *            Data em formato String.
	 * @return Data convertida em String.
	 */
	public String converterDateParaString(Date data) {
		logger.debug("Executando m�todo converterDateParaString");
		if (data != null) {
			return sdf.format(data);
		} else {
			return null;
		}
	}

	/**
	 * M�todo para comparar as das e retornar o numero de dias de diferen�a
	 * entre elas
	 * 
	 * Compare two date and return the difference between them in days.
	 * 
	 * @param dataLow
	 *            The lowest date
	 * @param dataHigh
	 *            The highest date
	 * 
	 * @return int
	 */
	public static int dataDiff(java.util.Date dataLow, java.util.Date dataHigh) {
		logger.debug("Executando m�todo dataDiff");

		GregorianCalendar startTime = new GregorianCalendar();
		GregorianCalendar endTime = new GregorianCalendar();

		GregorianCalendar curTime = new GregorianCalendar();
		GregorianCalendar baseTime = new GregorianCalendar();

		startTime.setTime(dataLow);
		endTime.setTime(dataHigh);

		int dif_multiplier = 1;

		// Verifica a ordem de inicio das datas
		if (dataLow.compareTo(dataHigh) < 0) {
			baseTime.setTime(dataHigh);
			curTime.setTime(dataLow);
			dif_multiplier = 1;
		} else {
			baseTime.setTime(dataLow);
			curTime.setTime(dataHigh);
			dif_multiplier = -1;
		}

		int result_years = 0;
		int result_months = 0;
		int result_days = 0;

		// Para cada mes e ano, vai de mes em mes pegar o ultimo dia para import
		// acumulando
		// no total de dias. Ja leva em consideracao ano bissesto
		while (curTime.get(GregorianCalendar.YEAR) < baseTime.get(GregorianCalendar.YEAR)
				|| curTime.get(GregorianCalendar.MONTH) < baseTime.get(GregorianCalendar.MONTH)) {

			int max_day = curTime.getActualMaximum(GregorianCalendar.DAY_OF_MONTH);
			result_months += max_day;
			curTime.add(GregorianCalendar.MONTH, 1);

		}

		// Marca que � um saldo negativo ou positivo
		result_months = result_months * dif_multiplier;

		// Retirna a diferenca de dias do total dos meses
		result_days += (endTime.get(GregorianCalendar.DAY_OF_MONTH) - startTime.get(GregorianCalendar.DAY_OF_MONTH));

		return result_years + result_months + result_days;
	}
}
