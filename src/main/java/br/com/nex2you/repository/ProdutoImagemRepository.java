package br.com.nex2you.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.nex2you.model.Produto;
import br.com.nex2you.model.ProdutoImagem;

public interface ProdutoImagemRepository extends
		CrudRepository<ProdutoImagem, Integer> {

	List<ProdutoImagem> findByProduto(Produto produto);
}
