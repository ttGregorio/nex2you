package br.com.nex2you.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.nex2you.model.Estado;

public interface EstadoRepository extends CrudRepository<Estado, Integer> {
}
