package br.com.nex2you.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.nex2you.model.ContaCliente;
import br.com.nex2you.model.ContaClienteItem;

public interface ContaClienteItemRepository extends
		CrudRepository<ContaClienteItem, Integer> {
	List<ContaClienteItem> findByContaCliente(ContaCliente contaCliente);
}
