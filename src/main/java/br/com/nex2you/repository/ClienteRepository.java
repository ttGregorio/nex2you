package br.com.nex2you.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.nex2you.model.Cliente;
import br.com.nex2you.model.Empresa;

public interface ClienteRepository extends CrudRepository<Cliente, Integer> {
	List<Cliente> findByEmpresaAndNomeLike(Empresa empresa, String nome);

	Cliente findByLoginAndSenha(String login, String senha);
}
