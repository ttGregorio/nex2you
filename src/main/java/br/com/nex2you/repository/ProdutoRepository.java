package br.com.nex2you.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import br.com.nex2you.model.CategoriaProduto;
import br.com.nex2you.model.Produto;
import br.com.nex2you.model.Empresa;

public interface ProdutoRepository extends
		CrudRepository<Produto, Integer> {
	Page<Produto> findByEmpresaAndAtivo(Pageable pageRequest, Empresa empresa, boolean ativo);

	Page<Produto> findByNomeLikeAndEmpresa(Pageable pageRequest,
			String string, Empresa empresa);

	List<Produto> findByCategoria(CategoriaProduto categoriaProduto);
}
