package br.com.nex2you.repository;

import org.springframework.data.repository.CrudRepository;

import br.com.nex2you.model.User;

public interface UserRepository extends CrudRepository<User, Integer> {
    User findByEmail(String email);
}
