package br.com.nex2you.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import br.com.nex2you.model.Empresa;
import br.com.nex2you.model.Perfil;

public interface PerfilRepository extends
		PagingAndSortingRepository<Perfil, Integer> {
	Page<Perfil> findByNomeLike(Pageable pageable, String nome);

	Page<Perfil> findByAtivo(Pageable pageRequest, boolean ativo);

	List<Perfil> findByAtivoAndEmpresa(boolean ativo, Empresa empresa);

	Page<Perfil> findByAtivoAndEmpresa(Pageable pageRequest, boolean ativo,
			Empresa empresa);

	Page<Perfil> findByNomeLikeAndEmpresa(Pageable pageRequest,
			String string, Empresa empresa);
}
