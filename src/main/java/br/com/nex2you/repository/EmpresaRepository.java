package br.com.nex2you.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

import br.com.nex2you.model.Cliente;
import br.com.nex2you.model.Empresa;

public interface EmpresaRepository extends
		PagingAndSortingRepository<Empresa, Integer> {

	Page<Empresa> findByAtivo(Pageable pageRequest, boolean ativo);

	Page<Empresa> findByNomeLike(Pageable pageRequest, String string);
	
	@Query("select e from Empresa e where e.categoria in (select c.categoria from PreferenciaCliente c where c.cliente = ?1)")
	List<Empresa>findByCliente(Cliente cliente);
}
