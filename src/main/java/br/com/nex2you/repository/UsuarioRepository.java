package br.com.nex2you.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

import br.com.nex2you.model.Empresa;
import br.com.nex2you.model.Usuario;

public interface UsuarioRepository extends
		PagingAndSortingRepository<Usuario, Integer> {

	Page<Usuario> findByNomeLikeAndEmpresa(Pageable pageable, String nome, Empresa empresa);

	Usuario findByLogin(String login);

	Usuario findByEmail(String email);

	Page<Usuario> findByAtivo(Pageable pageRequest, boolean ativo);

	Page<Usuario> findByEmpresaAndAtivo(Pageable pageRequest,
			Empresa empresa, boolean b);
}
