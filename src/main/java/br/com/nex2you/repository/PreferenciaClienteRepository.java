package br.com.nex2you.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.nex2you.model.CategoriaEmpresa;
import br.com.nex2you.model.Cliente;
import br.com.nex2you.model.PreferenciaCliente;

public interface PreferenciaClienteRepository extends CrudRepository<PreferenciaCliente, Integer> {
	List<PreferenciaCliente> findByCliente(Cliente cliente);

	PreferenciaCliente findByClienteAndCategoria(Cliente cliente,
			CategoriaEmpresa categoria);
}
