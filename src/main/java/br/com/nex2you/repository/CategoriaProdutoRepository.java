package br.com.nex2you.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;

import br.com.nex2you.model.CategoriaProduto;
import br.com.nex2you.model.Empresa;

public interface CategoriaProdutoRepository extends
		CrudRepository<CategoriaProduto, Integer> {
	Page<CategoriaProduto> findByEmpresaAndAtivo(Pageable pageRequest, Empresa empresa, boolean ativo);

	Page<CategoriaProduto> findByNomeLikeAndEmpresa(Pageable pageRequest,
			String string, Empresa empresa);

	List<CategoriaProduto> findByEmpresaAndAtivo(Empresa empresa, boolean ativo);
}
