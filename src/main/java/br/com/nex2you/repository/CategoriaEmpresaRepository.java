package br.com.nex2you.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.nex2you.model.CategoriaEmpresa;

public interface CategoriaEmpresaRepository extends CrudRepository<CategoriaEmpresa, Integer> {

	List<CategoriaEmpresa> findByIdGreaterThan(int i);
}
