package br.com.nex2you.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import br.com.nex2you.model.Cliente;
import br.com.nex2you.model.ContaCliente;
import br.com.nex2you.model.Empresa;

public interface ContaClienteRepository extends
		CrudRepository<ContaCliente, Integer> {
	List<ContaCliente> findByEmpresaAndPedidoAberto(Empresa empresa,
			boolean pedidoAberto);

	ContaCliente findByEmpresaAndClienteAndPedidoAberto(Empresa empresa,
			Cliente cliente, boolean aberto);

	List<ContaCliente> findByCliente(Cliente cliente);
}
