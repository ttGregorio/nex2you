<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div class="masthead">
	<h3 class="muted">
		<spring:message code='header.message' />
	</h3>

	<div class="navbar">
		<div class="navbar-inner">
			<div class="container">
				<ul class="nav" ng-controller="LocationController">
					<li
						ng-class="{'gray': activeURL == 'contacts', '': activeURL != 'contacts'}"><a
						title='<spring:message code="minhaEmpresa"/>'
						href="<c:url value='/protected/minhaEmpresa'/>"><p>
								<spring:message code="minhaEmpresa" />
							</p></a></li>
					<li
						ng-show="${user.perfil.permiteUsuarios || user.perfil.permitePerfis}"
						class="dropdown ng-class: settingsActive;"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown"><spring:message
								code="rh" /><b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li ng-show="${user.perfil.permiteUsuarios}"
								ng-class="{'gray': activeURL == 'contacts', '': activeURL != 'contacts'}"><a
								title='<spring:message code="rh.usuarios"/>'
								href="<c:url value='/protected/usuarios'/>"><p>
										<spring:message code="rh.usuarios" />
									</p></a></li>
							<li ng-show="${user.perfil.permitePerfis}"
								ng-class="{'gray': activeURL == 'contacts', '': activeURL != 'contacts'}"><a
								title='<spring:message code="rh.perfis"/>'
								href="<c:url value='/protected/perfis'/>"><p>
										<spring:message code="rh.perfis" />
									</p></a></li>
						</ul></li>
					<li class="dropdown ng-class: settingsActive;"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown"><spring:message
								code="produtos" /><b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li
								ng-class="{'gray': activeURL == 'contacts', '': activeURL != 'contacts'}"><a
								title='<spring:message code="produtos.categorias"/>'
								href="<c:url value='/protected/categoriasProdutos'/>"><p>
										<spring:message code="produtos.categorias" />
									</p></a></li>
							<li
								ng-class="{'gray': activeURL == 'contacts', '': activeURL != 'contacts'}"><a
								title='<spring:message code="produtos"/>'
								href="<c:url value='/protected/produtos'/>"><p>
										<spring:message code="produtos" />
									</p></a></li>
						</ul></li>
					<li class="dropdown ng-class: settingsActive;"><a href="#"
						class="dropdown-toggle" data-toggle="dropdown"><spring:message
								code="vendas" /><b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li
								ng-class="{'gray': activeURL == 'contacts', '': activeURL != 'contacts'}"><a
								title='<spring:message code="pedidos"/>'
								href="<c:url value='/protected/contasClientes'/>"><p>
										<spring:message code="pedidos" />
									</p></a></li>
						</ul></li>
				</ul>
				<ul class="nav pull-right">
					<li><a href="<c:url value='/logout' />"
						title='<spring:message code="header.logout"/>'><p
								class="displayInLine">
								<spring:message code="header.logout" />
								&nbsp;(${user.nome})
							</p></a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
