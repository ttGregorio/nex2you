angular
		.module("app")
		.controller(
				"contasClientesController",
				function($scope, $http) {
					$scope.pageToGet = 0;

					$scope.state = 'busy';

					$scope.lastAction = '';

					$scope.url = "/Nex2You/protected/contasClientes/";

					$scope.errorOnSubmit = false;
					$scope.errorIllegalAccess = false;
					$scope.displayMessageToUser = false;
					$scope.displayValidationError = false;
					$scope.displaySearchMessage = false;
					$scope.displaySearchButton = false;
					$scope.displayCreateContactButton = false;

					$scope.contact = {}
					$scope.itemPedido = {}

					$scope.searchFor = ""

					$scope.updateDadosProduto = function() {
						var url = $scope.url + '/buscarProduto/'
								+ $scope.itemPedido.produto.id;
						$scope.lastAction = 'list';

						var config = {
							params : {
								page : $scope.pageToGet
							}
						};
						$http.get(url, config).success(function(data) {
							$scope.estoque = data.estoque;
							$scope.valorUnitario = data.precoVenda;
						}).error(function() {
							$scope.state = 'error';
							$scope.displayCreateContactButton = false;
						});
					}
					$scope.atualizaValor = function() {
						if ($scope.contact.id == undefined) {
							$scope.contact.id = 0;
						}
						var url = $scope.url + '/sumProdutos/'
								+ $scope.contact.id + ','
								+ $scope.itemPedido.produto.id + ','
								+ $scope.itemPedido.quantidade;
						$scope.lastAction = 'list';

						var config = {
							params : {
								page : $scope.pageToGet
							}
						};
						$http.get(url, config).success(function(data) {
							$scope.itemPedido.valor = data.valor;
						}).error(function() {
							$scope.state = 'error';
							$scope.displayCreateContactButton = false;
						});
					}

					$scope.getCategorias = function() {
						var url = $scope.url + '/listCategorias';
						$scope.lastAction = 'list';

						var config = {
							params : {
								page : $scope.pageToGet
							}
						};
						$http.get(url, config).success(function(data) {
							$scope.listaCategorias = data;
						}).error(function() {
							$scope.state = 'error';
							$scope.displayCreateContactButton = false;
						});
					}

					$scope.getProdutos = function() {
						var url = $scope.url + '/listProdutos/'
								+ $scope.itemPedido.produto.categoria.id;
						$scope.lastAction = 'list';

						var config = {
							params : {
								page : $scope.pageToGet
							}
						};
						$http.get(url, config).success(function(data) {
							$scope.listaProdutos = data;
						}).error(function() {
							$scope.state = 'error';
							$scope.displayCreateContactButton = false;
						});
					}

					$scope.getContactList = function() {
						var url = $scope.url;
						$scope.lastAction = 'list';

						$scope.startDialogAjaxRequest();

						var config = {
							params : {
								page : $scope.pageToGet
							}
						};
						$http.get(url, config).success(function(data) {
							$scope.finishAjaxCallOnSuccess(data, null, false);
						}).error(function() {
							$scope.state = 'error';
							$scope.displayCreateContactButton = false;
						});
					}

					$scope.getItens = function() {
						var url = $scope.url + 'listItens/' + $scope.contact.id;
						$scope.lastAction = 'list';
						var config = {
							params : {
								page : $scope.pageToGet
							}
						};
						$http.get(url, config).success(function(data) {
							$scope.listaItens = data;
						}).error(function() {
							$scope.state = 'error';
							$scope.displayCreateContactButton = false;
						});
					}

					$scope.populateTable = function(data) {
						if (data.pagesCount > 0) {
							$scope.state = 'list';

							$scope.page = {
								source : data.contacts,
								currentPage : $scope.pageToGet,
								pagesCount : data.pagesCount,
								totalContacts : data.totalContacts
							};

							if ($scope.page.pagesCount <= $scope.page.currentPage) {
								$scope.pageToGet = $scope.page.pagesCount - 1;
								$scope.page.currentPage = $scope.page.pagesCount - 1;
							}

							$scope.displayCreateContactButton = true;
							$scope.displaySearchButton = true;
						} else {
							$scope.state = 'noresult';
							$scope.displayCreateContactButton = true;

							if (!$scope.searchFor) {
								$scope.displaySearchButton = false;
							}
						}

						if (data.actionMessage || data.searchMessage) {
							$scope.displayMessageToUser = $scope.lastAction != 'search';

							$scope.page.actionMessage = data.actionMessage;
							$scope.page.searchMessage = data.searchMessage;
						} else {
							$scope.displayMessageToUser = false;
						}
					}

					$scope.changePage = function(page) {
						$scope.pageToGet = page;

						if ($scope.searchFor) {
							$scope.searchContact($scope.searchFor, true);
						} else {
							$scope.getContactList();
						}
					};

					$scope.exit = function(modalId) {
						$(modalId).modal('hide');

						$scope.contact = {};
						$scope.errorOnSubmit = false;
						$scope.errorIllegalAccess = false;
						$scope.displayValidationError = false;
					}

					$scope.finishAjaxCallOnSuccess = function(data, modalId,
							isPagination) {
						$scope.populateTable(data);
						$("#loadingModal").modal('hide');

						if (!isPagination) {
							if (modalId) {
								$scope.exit(modalId);
							}
						}

						$scope.lastAction = '';
					}

					$scope.startDialogAjaxRequest = function() {
						$scope.displayValidationError = false;
						$("#loadingModal").modal('show');
						$scope.previousState = $scope.state;
						$scope.state = 'busy';
					}

					$scope.handleErrorInDialogs = function(status) {
						$("#loadingModal").modal('hide');
						$scope.state = $scope.previousState;

						// illegal access
						if (status == 403) {
							$scope.errorIllegalAccess = true;
							return;
						}

						$scope.errorOnSubmit = true;
						$scope.lastAction = '';
					}

					$scope.addSearchParametersIfNeeded = function(config,
							isPagination) {
						if (!config.params) {
							config.params = {};
						}

						config.params.page = $scope.pageToGet;

						if ($scope.searchFor) {
							config.params.searchFor = $scope.searchFor;
						}
					}

					$scope.resetContact = function() {
						$scope.contact = {};
						$scope.listaItens = [];
					};

					$scope.resetItemPedido = function() {
						$scope.itemPedido = {};
					};

					$scope.createItemPedido = function(newContactForm) {
						if ($scope.itemPedido.quantidade > 0) {
							if ($scope.contact.id == 0) {
								var config = {
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
									}
								};

								$scope.addSearchParametersIfNeeded(config,
										false);

								$http({
									method : 'POST',
									url : $scope.url,
									headers : {
										'Content-Type' : 'application/json'
									},
									data : $scope.contact
								})
										.success(
												function(data) {
													$scope.itemPedido.contaCliente = data.contacts[data.contacts.length - 1];
													if (!newContactForm.$valid) {
														$scope.displayValidationError = true;

														return;
													}

													console
															.log(newContactForm.contact);

													$scope.lastAction = 'create';

													var url = $scope.url
															+ '/insertItem';

													var config = {
														headers : {
															'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
														}
													};

													$scope
															.addSearchParametersIfNeeded(
																	config,
																	false);

													$http(
															{
																method : 'POST',
																url : url,
																headers : {
																	'Content-Type' : 'application/json'
																},
																data : $scope.itemPedido
															})
															.success(
																	function(
																			data) {
																		$scope.listaItens = data;
																		$scope
																				.getContactList();
																		$scope.valorUnitario = '';
																		$scope.estoque = '';
																		$(
																				"#addItemPedidoModal")
																				.modal(
																						'hide');
																	})
															.error(
																	function(
																			data) {
																		$scope
																				.handleErrorInDialogs(status);
																	});
												})
										.error(
												function(data) {
													$scope
															.handleErrorInDialogs(status);
												});
							} else {
								$scope.itemPedido.contaCliente = $scope.contact;
								if (!newContactForm.$valid) {
									$scope.displayValidationError = true;

									return;
								}

								console.log(newContactForm.contact);

								$scope.lastAction = 'create';

								var url = $scope.url + '/insertItem';

								var config = {
									headers : {
										'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
									}
								};

								$scope.addSearchParametersIfNeeded(config,
										false);

								$http({
									method : 'POST',
									url : url,
									headers : {
										'Content-Type' : 'application/json'
									},
									data : $scope.itemPedido
								}).success(function(data) {
									$scope.listaItens = data;
									$scope.getContactList();
									$scope.valorUnitario = '';
									$scope.estoque = '';
									$("#addItemPedidoModal").modal('hide');
								}).error(function(data) {
									$scope.handleErrorInDialogs(status);
								});
							}
							$("#addItemPedidoModal").modal('hide');
						}
					};

					$scope.createContact = function(newContactForm) {

						if (!newContactForm.$valid) {
							$scope.displayValidationError = true;

							return;
						}

						console.log(newContactForm.contact);

						$scope.lastAction = 'create';

						var url = $scope.url;

						var config = {
							headers : {
								'Content-Type' : 'application/x-www-form-urlencoded; charset=UTF-8'
							}
						};

						$scope.addSearchParametersIfNeeded(config, false);

						$scope.startDialogAjaxRequest();

						$http({
							method : 'POST',
							url : url,
							headers : {
								'Content-Type' : 'application/json'
							},
							data : $scope.contact
						}).success(
								function(data) {
									$scope.finishAjaxCallOnSuccess(data,
											"#addContactsModal", false);
								}).error(function(data) {
							$scope.handleErrorInDialogs(status);
						});
					};

					$scope.selectedContact = function(contact) {
						var selectedContact = angular.copy(contact);
						$scope.contact = selectedContact;
					}

					$scope.selectedItemPedido = function(itemPedido) {
						var item = angular.copy(itemPedido);
						$scope.itemPedido = item;
					}

					$scope.updateItemPedido = function(updateContactForm) {

						$scope.lastAction = 'update';

						var url = $scope.url + '/updateItem/'
								+ $scope.itemPedido.id;

						var config = {}

						$scope.addSearchParametersIfNeeded(config, false);

						$http.put(url, $scope.itemPedido, config).success(
								function(data) {
									$scope.listaItens = data;
									$scope.getContactList();
									$("#updateItemPedidoModal").modal('hide');
								}).error(
								function(data, status, headers, config) {
									$scope.handleErrorInDialogs(status);
								});
					};

					$scope.updateContact = function(updateContactForm) {

						if (!updateContactForm.$valid) {
							$scope.displayValidationError = true;

							return;
						}

						$scope.lastAction = 'update';

						var url = $scope.url + $scope.contact.id;

						$scope.startDialogAjaxRequest();

						var config = {}

						$scope.addSearchParametersIfNeeded(config, false);

						$http.put(url, $scope.contact, config).success(
								function(data) {
									$scope.finishAjaxCallOnSuccess(data,
											"#updateContactsModal", false);
								}).error(
								function(data, status, headers, config) {
									$scope.handleErrorInDialogs(status);
								});
					};

					$scope.searchContact = function(searchContactForm,
							isPagination) {
						if (!($scope.searchFor) && (!searchContactForm.$valid)) {
							$scope.displayValidationError = true;
							return;
						}

						$scope.lastAction = 'search';

						var url = $scope.url + $scope.searchFor;

						$scope.startDialogAjaxRequest();

						var config = {};

						if ($scope.searchFor) {
							$scope.addSearchParametersIfNeeded(config,
									isPagination);
						}

						$http.get(url, config).success(
								function(data) {
									$scope.finishAjaxCallOnSuccess(data,
											"#searchContactsModal",
											isPagination);
									$scope.displaySearchMessage = true;
								}).error(
								function(data, status, headers, config) {
									$scope.handleErrorInDialogs(status);
								});
					};

					$scope.deleteItem = function() {
						$scope.lastAction = 'delete';

						var url = $scope.url + '/deleteItem/'
								+ $scope.itemPedido.id;

						var params = {
							searchFor : $scope.searchFor,
							page : $scope.pageToGet
						};

						$http({
							method : 'DELETE',
							url : url,
							params : params
						}).success(function(data) {
							$scope.resetItemPedido();
							$scope.listaItens = data;
							$scope.getContactList();
							$("#deleteItemPedidoModal").modal('hide');

						}).error(function(data, status, headers, config) {
							$scope.handleErrorInDialogs(status);
						});
					};

					$scope.deleteContact = function() {
						$scope.lastAction = 'delete';

						var url = $scope.url + $scope.contact.id;

						$scope.startDialogAjaxRequest();

						var params = {
							searchFor : $scope.searchFor,
							page : $scope.pageToGet
						};

						$http({
							method : 'DELETE',
							url : url,
							params : params
						}).success(
								function(data) {
									$scope.resetContact();
									$scope.finishAjaxCallOnSuccess(data,
											"#deleteContactsModal", false);
								}).error(
								function(data, status, headers, config) {
									$scope.handleErrorInDialogs(status);
								});
					};

					$scope.resetSearch = function() {
						$scope.searchFor = "";
						$scope.pageToGet = 0;
						$scope.getContactList();
						$scope.displaySearchMessage = false;
					}

					$scope.getContactList();
				});