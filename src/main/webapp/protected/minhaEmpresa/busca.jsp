<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="row-fluid" ng-controller="minhaEmpresaController">
	<h2>
		<p class="text-center">
			<spring:message code='minhaEmpresa' />
		</p>
	</h2>
	<div>
		<div id="loadingModal" class="modal hide fade in centering"
			role="dialog" aria-labelledby="deleteContactsModalLabel"
			aria-hidden="true">
			<div id="divLoadingIcon" class="text-center">
				<div class="icon-align-center loading"></div>
			</div>
		</div>

		<div
			ng-class="{'alert badge-inverse': displaySearchMessage == true, 'none': displaySearchMessage == false}">
			<h4>
				<p class="messageToUser">
					<i class="icon-info-sign"></i>&nbsp;{{page.searchMessage}}
				</p>
			</h4>
			<a href="#" role="button" ng-click="resetSearch();"
				ng-class="{'': displaySearchMessage == true, 'none': displaySearchMessage == false}"
				title="<spring:message code='search.reset'/>"
				class="btn btn-inverse" data-toggle="modal"> <i
				class="icon-remove"></i> <spring:message code="search.reset" />
			</a>
		</div>

		<div
			ng-class="{'alert badge-inverse': displayMessageToUser == true, 'none': displayMessageToUser == false}">
			<h4 class="displayInLine">
				<p class="messageToUser displayInLine">
					<i class="icon-info-sign"></i>&nbsp;{{page.actionMessage}}
				</p>
			</h4>
		</div>

		<div
			ng-class="{'alert alert-block alert-error': state == 'error', 'none': state != 'error'}">
			<h4>
				<i class="icon-info-sign"></i>
				<spring:message code="error.generic.header" />
			</h4>
			<br />

			<p>
				<spring:message code="error.generic.text" />
			</p>
		</div>

		<div id="gridContainer">
			<div style="width: 100%">
				<div class="pull-left">
					<!-- Nome -->
					<div style="float: left; width: 100%">
						<div class="input-append">
							<label>* <spring:message code="empresa.nome" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" required="required" autofocus
								ng-model="contact.nome" name="name" style="width: 100%"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='perfil.nome'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='perfil'/>  " />
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.name.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>
					<!-- Email -->
					<div style="float: left; width: 100%">
						<div class="input-append">
							<label>* <spring:message code="empresa.email" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="email" required="required" autofocus
								ng-model="contact.email" name="name" style="width: 100%"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='perfil.nome'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='perfil'/>  " />
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.name.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>
					<!-- Telefone -->
					<div style="float: left; width: 100%">
						<div class="input-append">
							<label>* <spring:message code="empresa.telefone" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" required="required" autofocus
								onKeyPress="MascaraTelefone(this);" maxlength="15"
								ng-model="contact.telefone" name="name" style="width: 100%"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='perfil.nome'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='perfil'/>  " />
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.name.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>
					<!-- Logo -->
					<div style="float: left; width: 100%">
						<div style="width: 20%; height: 20%; float: left" align="left">
							<img style="width: 80%; height: 80%"
								ng-src="data:image/JPEG;base64,{{contact.logo}}">
						</div>
						<div class="modal-body">
							<iframe width="0" height="0" border="0" name="dummyframe"
								id="dummyframe"></iframe>
							<form method="post" enctype="multipart/form-data"
								action="singleSave" target="dummyframe">
								Imagem: <input type="file" name="file"> <br /> <br />
								<br /> <br /> <input type="submit" value="Upload"
									class="btn btn-inverse">
							</form>
						</div>

					</div>
					<!-- Categoria -->
					<div class="input-append" data-ng-init="getCategorias()">
						<div class="input-append">
							<label> <spring:message code="empresa.categoria" />
							</label>
						</div>
						<br />
						<div class="input-append">
							<select ng-model="contact.categoria.id">
								<option data-ng-repeat="item in listaCategorias"
									value="{{item.id}}" ng-selected="contact.categoria.id==item.id">{{item.nome}}</option>
							</select>
						</div>
					</div>
					<!-- Descrição -->
					<div style="float: left; width: 100%">
						<div class="input-append">
							<label>* <spring:message code="empresa.descricao" />:
							</label>
						</div>
						<br />
						<div class="input-append" style="width: 95%">
							<textarea type="text" autofocus ng-model="contact.descricao"
								style="width: 100%" name="name" rows="5" required="required"
								placeholder="<spring:message code='contact'/>&nbsp;<spring:message code='sindicancia.solicitacao'/> "></textarea>
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.name.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>

				</div>
			</div>
			<div class="pull-left" style="width: 100%">
				<input type="submit" class="btn btn-inverse"
					ng-click="createContact();" value='<spring:message code="gravar"/>' />
			</div>

		</div>
	</div>
</div>

<script src="<c:url value="/resources/js/pages/minhaEmpresa.js" />"></script>