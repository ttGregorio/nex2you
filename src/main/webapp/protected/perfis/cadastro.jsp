<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div id="addContactsModal"
	class="modal hide fade in centering insertAndUpdateDialogsPerfis"
	role="dialog" aria-labelledby="addContactsModalLabel"
	aria-hidden="true">
	<div class="modal-header">
		<h3 id="addContactsModalLabel" class="displayInLine">
			<spring:message code="create" />
			&nbsp;
			<spring:message code="perfil" />
		</h3>
	</div>
	<div class="modal-body">
		<form name="newContactForm" novalidate>
			<div style="width: 100%">
				<div class="pull-left">
					<!-- Nome -->
					<div style="float: left">
						<div class="input-append">
							<label>* <spring:message code="perfil.nome" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" required="required" autofocus
								ng-model="contact.nome" name="name"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='perfil.nome'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='perfil'/>  " />
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.name.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>
				</div>
			</div>
			<div class="pull-left" style="width: 100%">
				<div class="pull-left">
					<div style="float: left" align="center">
						<div class="input-append">
							<h3 id="addContactsModalLabel" class="displayInLine">
								<spring:message code="rh" />
							</h3>
						</div>
						<br />
						<div class="input-append">
							<input type="checkbox" ng-model="contact.permiteUsuarios"
								name="endereco" /> <label> <spring:message
									code="rh.usuarios" />
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="checkbox" ng-model="contact.permitePerfis"
								name="endereco" /> <label> <spring:message
									code="rh.perfis" />
							</label>
						</div>
					</div>
					<div style="float: left" align="center">
						<div class="input-append">
							<h3 id="addContactsModalLabel" class="displayInLine">
								<spring:message code="produtos" />
							</h3>
						</div>
						<br />
						<div class="input-append">
							<input type="checkbox" ng-model="contact.permiteCategorias"
								name="endereco" /> <label> <spring:message
									code="produtos.categorias" />
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="checkbox" ng-model="contact.permiteProdutos"
								name="endereco" /> <label> <spring:message
									code="produtos" />
							</label>
						</div>
					</div>
					<div style="float: left" align="center">
						<div class="input-append">
							<h3 id="addContactsModalLabel" class="displayInLine">
								<spring:message code="vendas" />
							</h3>
						</div>
						<br />
						<div class="input-append">
							<input type="checkbox" ng-model="contact.permiteContas"
								name="endereco" /> <label> <spring:message
									code="pedidos" />
							</label>
						</div>
					</div>
				</div>
			</div>
			<div class="pull-left" style="width: 100%">
				<input type="submit" class="btn btn-inverse"
					ng-click="createContact(newContactForm);"
					value='<spring:message code="create"/>' />
				<button class="btn btn-inverse" data-dismiss="modal"
					ng-click="exit('#addContactsModal');" aria-hidden="true">
					<spring:message code="cancel" />
				</button>
			</div>
		</form>
	</div>
	<span class="alert alert-error dialogErrorMessage"
		ng-show="errorOnSubmit"> <spring:message code="request.error" />
	</span>
</div>

<div id="updateContactsModal"
	class="modal hide fade in centering insertAndUpdateDialogsPerfis"
	role="dialog" aria-labelledby="updateContactsModalLabel"
	aria-hidden="true">
	<div class="modal-header">
		<h3 id="updateContactsModalLabel" class="displayInLine">
			<spring:message code="update" />
			&nbsp;
			<spring:message code="perfil" />
		</h3>
	</div>
	<div class="modal-body">
		<form name="newContactForm" novalidate>
			<div style="width: 100%">
				<div class="pull-left">
					<!-- Nome -->
					<div style="float: left">
						<div class="input-append">
							<label>* <spring:message code="perfil.nome" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" required="required" autofocus
								ng-model="contact.nome" name="name"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='perfil.nome'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='perfil'/>  " />
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.name.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>
				</div>
			</div>
			<div class="pull-left" style="width: 100%">
				<div class="pull-left">
					<div style="float: left" align="center">
						<div class="input-append">
							<h3 id="addContactsModalLabel" class="displayInLine">
								<spring:message code="rh" />
							</h3>
						</div>
						<br />
						<div class="input-append">
							<input type="checkbox" ng-model="contact.permiteUsuarios"
								name="endereco" /> <label> <spring:message
									code="rh.usuarios" />
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="checkbox" ng-model="contact.permitePerfis"
								name="endereco" /> <label> <spring:message
									code="rh.perfis" />
							</label>
						</div>
					</div>
					<div style="float: left" align="center">
						<div class="input-append">
							<h3 id="addContactsModalLabel" class="displayInLine">
								<spring:message code="produtos" />
							</h3>
						</div>
						<br />
						<div class="input-append">
							<input type="checkbox" ng-model="contact.permiteCategorias"
								name="endereco" /> <label> <spring:message
									code="produtos.categorias" />
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="checkbox" ng-model="contact.permiteProdutos"
								name="endereco" /> <label> <spring:message
									code="produtos" />
							</label>
						</div>
					</div>
					<div style="float: left" align="center">
						<div class="input-append">
							<h3 id="addContactsModalLabel" class="displayInLine">
								<spring:message code="vendas" />
							</h3>
						</div>
						<br />
						<div class="input-append">
							<input type="checkbox" ng-model="contact.permiteContas"
								name="endereco" /> <label> <spring:message
									code="pedidos" />
							</label>
						</div>
					</div>
				</div>
			</div>
			<div class="pull-left" style="width: 100%">
				<input type="submit" class="btn btn-inverse"
					ng-click="updateContact(updateContactForm);"
					value='<spring:message code="update"/>' />
				<button class="btn btn-inverse" data-dismiss="modal"
					ng-click="exit('#updateContactsModal');" aria-hidden="true">
					<spring:message code="cancel" />
				</button>
			</div>
		</form>
	</div>
	<span class="alert alert-error dialogErrorMessage"
		ng-show="errorOnSubmit"> <spring:message code="request.error" />
	</span>
</div>

<div id="deleteContactsModal" class="modal hide fade in centering"
	role="dialog" aria-labelledby="searchContactsModalLabel"
	aria-hidden="true">
	<div class="modal-header">
		<h3 id="deleteContactsModalLabel" class="displayInLine">
			<spring:message code="delete" />
			&nbsp;
			<spring:message code="sindicante" />
		</h3>
	</div>
	<div class="modal-body">
		<form name="deleteContactForm" novalidate>
			<p>
				<spring:message code="delete.confirm" />
				:&nbsp;{{contact.nome}}?
			</p>

			<input type="submit" class="btn btn-inverse"
				ng-click="deleteContact();" value='<spring:message code="delete"/>' />
			<button class="btn btn-inverse" data-dismiss="modal"
				ng-click="exit('#deleteContactsModal');" aria-hidden="true">
				<spring:message code="cancel" />
			</button>
		</form>
	</div>
	<span class="alert alert-error dialogErrorMessage"
		ng-show="errorOnSubmit"> <spring:message code="request.error" />
	</span> <span class="alert alert-error dialogErrorMessage"
		ng-show="errorIllegalAccess"> <spring:message
			code="request.illegal.access" />
	</span>
</div>

<div id="searchContactsModal" class="modal hide fade in centering"
	role="dialog" aria-labelledby="searchContactsModalLabel"
	aria-hidden="true">
	<div class="modal-header">
		<h3 id="searchContactsModalLabel" class="displayInLine">
			<spring:message code="search" />
		</h3>
	</div>
	<div class="modal-body">
		<form name="searchContactForm" novalidate>
			<label><spring:message code="search.for" /></label>

			<div>
				<br />
				<div class="input-append">
					<input type="text" autofocus ng-model="searchFor" name="searchFor"
						placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='perfil.nome'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='perfil'/> " />
				</div>
				<div class="input-append displayInLine">
					<label class="displayInLine"> <span
						class="alert alert-error"
						ng-show="displayValidationError && searchContactForm.searchFor.$error.required">
							<spring:message code="required" />
					</span>
					</label>
				</div>
			</div>
			<input type="submit" class="btn btn-inverse"
				ng-click="searchContact(searchContactForm, false);"
				value='<spring:message code="search"/>' />
			<button class="btn btn-inverse" data-dismiss="modal"
				ng-click="exit('#searchContactsModal');" aria-hidden="true">
				<spring:message code="cancel" />
			</button>
		</form>
	</div>
	<span class="alert alert-error dialogErrorMessage"
		ng-show="errorOnSubmit"> <spring:message code="request.error" />
	</span>
</div>
