<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div id="addContactsModal"
	class="modal hide fade in centering insertAndUpdateDialogs"
	role="dialog" aria-labelledby="addContactsModalLabel"
	aria-hidden="true">
	<div class="modal-header">
		<h3 id="addContactsModalLabel" class="displayInLine">
			<spring:message code="create" />
			&nbsp;
			<spring:message code="usuario" />
		</h3>
	</div>
	<div class="modal-body">
		<form name="newContactForm" novalidate>
			<div style="width: 100%">
				<div class="pull-left">
					<!-- Nome -->
					<div style="float: left">
						<div class="input-append">
							<label>* <spring:message code="sindicantes.nome" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" required="required" autofocus
								ng-model="contact.nome" name="name"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='sindicantes.nome'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='usuario'/> " />
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.name.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>
					<!-- Login -->
					<div style="float: left">
						<div class="input-append">
							<label>* <spring:message code="sindicantes.login" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" required="required" ng-model="contact.login"
								name="name"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='sindicantes.login'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='usuario'/>" />
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.name.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>
					<!-- Senha -->
					<div style="float: left">
						<div class="input-append">
							<label>* <spring:message code="sindicantes.senha" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" required="required" ng-model="contact.senha"
								name="name"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='a'/>&nbsp;<spring:message code='sindicantes.senha'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='usuario'/>" />
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.name.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>
					<!-- Email -->
					<div style="float: left">
						<div class="input-append">
							<label>* <spring:message code="contacts.email" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="email" required="required" ng-model="contact.email"
								name="email"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='sindicantes.email'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='usuario'/>" />
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.email.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>
				</div>
				<div class="pull-left">
					<!-- Endereço -->
					<div style="float: left">
						<div class="input-append">
							<label> <spring:message code="sindicantes.endereco" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" ng-model="contact.endereco" name="endereco"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='sindicantes.endereco'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='usuario'/>" />
						</div>
					</div>
					<!-- Perfil -->
					<div style="float: left; width: 100%" data-ng-init="getPerfis()">
						<div class="input-append">
							<label>* <spring:message code="sindicantes.perfil" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<select ng-model="contact.perfil.id">
								<option data-ng-repeat="item in listaPerfis" value="{{item.id}}"
									ng-selected="contact.seguradora.id==item.id">{{item.nome}}</option>
							</select>
						</div>
					</div>
					<!-- Bairro -->
					<div style="float: left">
						<div class="input-append">
							<label> <spring:message code="sindicantes.bairro" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" ng-model="contact.bairro" name="endereco"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='sindicantes.bairro'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='usuario'/>" />
						</div>
					</div>
					<!-- Cidade -->
					<div style="float: left">
						<div class="input-append">
							<label> <spring:message code="sindicantes.cidade" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" ng-model="contact.cidade" name="endereco"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='a'/>&nbsp;<spring:message code='sindicantes.cidade'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='usuario'/>" />
						</div>
					</div>
					<!-- Estado -->
					<div style="float: left; width: 100%" data-ng-init="getEstados()">
						<div class="input-append">
							<label>* <spring:message code="sindicantes.estado" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<select ng-model="contact.estado.id">
								<option data-ng-repeat="item in listaEstados"
									value="{{item.id}}" ng-selected="contact.estado.id==item.id">{{item.nome}}</option>
							</select>
						</div>
					</div>
					<!-- RG -->
					<div style="float: left">
						<div class="input-append">
							<label> <spring:message code="sindicantes.rg" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" ng-model="contact.rg" name="endereco"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='sindicantes.rg'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='usuario'/>" />
						</div>
					</div>
					<!-- CPF -->
					<div style="float: left">
						<div class="input-append">
							<label>* <spring:message code="sindicantes.cpf" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" required="required" ng-model="contact.cpf"
								name="endereco" onKeyPress="MascaraCPF(this);" maxlength="14"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='sindicantes.cpf'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='usuario'/>" />
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.email.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>
				</div>
				<div class="pull-left">
					<input type="submit" class="btn btn-inverse"
						ng-click="createContact(newContactForm);"
						value='<spring:message code="create"/>' />
					<button class="btn btn-inverse" data-dismiss="modal"
						ng-click="exit('#addContactsModal');" aria-hidden="true">
						<spring:message code="cancel" />
					</button>
				</div>
			</div>
		</form>
	</div>
	<span class="alert alert-error dialogErrorMessage"
		ng-show="errorOnSubmit"> <spring:message code="request.error" />
	</span>
</div>
<!-- Edição de usuários -->
<div id="updateContactsModal"
	class="modal hide fade in centering insertAndUpdateDialogs"
	role="dialog" aria-labelledby="updateContactsModalLabel"
	aria-hidden="true">
	<div class="modal-header">
		<h3 id="updateContactsModalLabel" class="displayInLine">
			<spring:message code="update" />
			&nbsp;
			<spring:message code="usuario" />
		</h3>
	</div>
	<div class="modal-body">
		<form name="updateContactForm" novalidate>
			<input type="hidden" ng-model="contact.id" name="id"
				value="{{contact.id}}" />

			<div style="width: 100%">
				<div class="pull-left">
					<!-- Nome -->
					<div style="float: left">
						<div class="input-append">
							<label>* <spring:message code="sindicantes.nome" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" required="required" autofocus
								ng-model="contact.nome" name="name"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='sindicantes.nome'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='usuario'/> " />
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.name.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>
					<!-- Login -->
					<div style="float: left">
						<div class="input-append">
							<label>* <spring:message code="sindicantes.login" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" required="required" ng-model="contact.login"
								name="name"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='sindicantes.login'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='usuario'/>" />
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.name.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>
					<!-- Senha -->
					<div style="float: left">
						<div class="input-append">
							<label>* <spring:message code="sindicantes.senha" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" required="required" ng-model="contact.senha"
								name="name"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='a'/>&nbsp;<spring:message code='sindicantes.senha'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='usuario'/>" />
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.name.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>
					<!-- Email -->
					<div style="float: left">
						<div class="input-append">
							<label>* <spring:message code="contacts.email" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" required="required" ng-model="contact.email"
								name="email"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='sindicantes.email'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='usuario'/>" />
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.email.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>
				</div>
				<div class="pull-left">
					<!-- Endereço -->
					<div style="float: left">
						<div class="input-append">
							<label> <spring:message code="sindicantes.endereco" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" ng-model="contact.endereco" name="endereco"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='sindicantes.endereco'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='usuario'/>" />
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.email.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>
					<!-- Perfil -->
					<div style="float: left; width: 100%" data-ng-init="getPerfis()">
						<div class="input-append">
							<label>* <spring:message code="sindicantes.perfil" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<select ng-model="contact.perfil.id">
								<option data-ng-repeat="item in listaPerfis" value="{{item.id}}"
									ng-selected="contact.perfil.id==item.id">{{item.nome}}</option>
							</select>
						</div>
					</div>
					<!-- Bairro -->
					<div style="float: left">
						<div class="input-append">
							<label> <spring:message code="sindicantes.bairro" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" ng-model="contact.bairro" name="endereco"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='sindicantes.bairro'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='usuario'/>" />
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.email.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>
					<!-- Cidade -->
					<div style="float: left">
						<div class="input-append">
							<label> <spring:message code="sindicantes.cidade" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" ng-model="contact.cidade" name="endereco"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='a'/>&nbsp;<spring:message code='sindicantes.cidade'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='usuario'/>" />
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.email.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>
					<!-- Estado -->
					<div style="float: left; width: 100%" data-ng-init="getEstados()">
						<div class="input-append">
							<label>* <spring:message code="sindicantes.estado" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<select ng-model="contact.estado.id">
								<option data-ng-repeat="item in listaEstados"
									value="{{item.id}}" ng-selected="contact.estado.id==item.id">{{item.nome}}</option>
							</select>
						</div>
					</div>
					<!-- RG -->
					<div style="float: left">
						<div class="input-append">
							<label> <spring:message code="sindicantes.rg" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" ng-model="contact.rg" name="endereco"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='sindicantes.rg'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='usuario'/>" />
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.email.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>
					<!-- CPF -->
					<div style="float: left">
						<div class="input-append">
							<label>* <spring:message code="sindicantes.cpf" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" required="required" ng-model="contact.cpf"
								name="endereco" onKeyPress="MascaraCPF(this);" maxlength="14"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='sindicantes.cpf'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='usuario'/>" />
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.email.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>
				</div>
				<div class="pull-left">
					<input type="submit" class="btn btn-inverse"
						ng-click="updateContact(updateContactForm);"
						value='<spring:message code="update"/>' />
					<button class="btn btn-inverse" data-dismiss="modal"
						ng-click="exit('#updateContactsModal');" aria-hidden="true">
						<spring:message code="cancel" />
					</button>
				</div>
			</div>
		</form>
	</div>
	<span class="alert alert-error dialogErrorMessage"
		ng-show="errorOnSubmit"> <spring:message code="request.error" />
	</span>
</div>

<!-- Exclusão de usuários -->
<div id="deleteContactsModal" class="modal hide fade in centering"
	role="dialog" aria-labelledby="searchContactsModalLabel"
	aria-hidden="true">
	<div class="modal-header">
		<h3 id="deleteContactsModalLabel" class="displayInLine">
			<spring:message code="delete" />
			&nbsp;
			<spring:message code="sindicante" />
		</h3>
	</div>
	<div class="modal-body">
		<form name="deleteContactForm" novalidate>
			<p>
				<spring:message code="delete.confirm" />
				:&nbsp;{{contact.nome}}?
			</p>

			<input type="submit" class="btn btn-inverse"
				ng-click="deleteContact();" value='<spring:message code="delete"/>' />
			<button class="btn btn-inverse" data-dismiss="modal"
				ng-click="exit('#deleteContactsModal');" aria-hidden="true">
				<spring:message code="cancel" />
			</button>
		</form>
	</div>
	<span class="alert alert-error dialogErrorMessage"
		ng-show="errorOnSubmit"> <spring:message code="request.error" />
	</span> <span class="alert alert-error dialogErrorMessage"
		ng-show="errorIllegalAccess"> <spring:message
			code="request.illegal.access" />
	</span>
</div>
<!-- Lista de Contas do usuário -->
<div id="searchContasModal" class="modal hide fade in centering"
	role="dialog" aria-labelledby="searchContasModal" aria-hidden="true">
	<div class="modal-header">
		<h3 id="searchContactsModalLabel" class="displayInLine">
			<spring:message code="search" />
		</h3>
	</div>
	<div class="modal-body">
		<form name="searchContactForm" novalidate>
			<label><spring:message code="search.for" /></label>

			<div>
				<br />
				<div class="input-append">
					<input type="text" autofocus ng-model="searchFor" name="searchFor"
						placeholder="<spring:message code='contact'/>&nbsp;<spring:message code='sindicantes.nome'/> " />
				</div>
				<div class="input-append displayInLine">
					<label class="displayInLine"> <span
						class="alert alert-error"
						ng-show="displayValidationError && searchContactForm.searchFor.$error.required">
							<spring:message code="required" />
					</span>
					</label>
				</div>
			</div>
			<input type="submit" class="btn btn-inverse"
				ng-click="searchContact(searchContactForm, false);"
				value='<spring:message code="search"/>' />
			<button class="btn btn-inverse" data-dismiss="modal"
				ng-click="exit('#searchContactsModal');" aria-hidden="true">
				<spring:message code="cancel" />
			</button>
		</form>
	</div>
	<span class="alert alert-error dialogErrorMessage"
		ng-show="errorOnSubmit"> <spring:message code="request.error" />
	</span>
</div>
<div id="contasContactsModal"
	class="modal hide fade in centering insertAndUpdateDialogs"
	role="dialog" aria-labelledby="searchContasModal" aria-hidden="true">
	<div class="modal-header">
		<h3 id="searchContactsModalLabel" class="displayInLine">
			<spring:message code="search" />
		</h3>
	</div>
	<div align="left">
		<br /> <a href="#addContaModal" role="button"
			ng-click="resetConta();"
			title="<spring:message code='create'/>&nbsp;<spring:message code='usuario'/>"
			class="btn btn-inverse" data-toggle="modal"> <i class="icon-plus"></i>
			&nbsp;&nbsp;<spring:message code="inserir" />&nbsp;<spring:message
				code="conta" />
		</a>
	</div>
	<div id="gridContainer"
		ng-class="{'': state == 'list', 'none': state != 'list'}">
		<table class="table table-bordered table-striped">
			<thead>
				<tr>
					<th scope="col"><spring:message code="conta.banco" /></th>
					<th scope="col"><spring:message code="conta.tipoConta" /></th>
					<th scope="col"><spring:message code="conta.tipoDocumento" /></th>
					<th scope="col"><spring:message code="conta.documento" /></th>
					<th scope="col"><spring:message code="conta.agencia" /></th>
					<th scope="col"><spring:message code="conta.conta" /></th>
					<th scope="col"></th>
				</tr>
			</thead>
			<tbody>
				<tr ng-repeat="conta in listaContas">
					<td class="tdContactsCentered">{{conta.banco.nome}}</td>
					<td class="tdContactsCentered">{{conta.tipoString}}</td>
					<td class="tdContactsCentered">{{conta.tipoDocumento}}</td>
					<td class="tdContactsCentered">{{conta.documento}}</td>
					<td class="tdContactsCentered">{{conta.agencia}}</td>
					<td class="tdContactsCentered">{{conta.conta}}</td>
					<td class="width15">
						<div class="text-center">
							<input type="hidden" value="{{conta.id}}" /> <a
								href="#updateContaModal" ng-click="selectedConta(conta);"
								role="button"
								title="<spring:message code="update"/>&nbsp;<spring:message code="usuario"/>"
								class="btn btn-inverse" data-toggle="modal"> <i
								class="icon-pencil"></i>
							</a> <a href="#" ng-click="selectedConta(conta);deleteConta();"
								role="button"
								title="<spring:message code="delete"/>&nbsp;<spring:message code="usuario"/>"
								class="btn btn-inverse" data-toggle="modal"> <i
								class="icon-minus"></i>
							</a>
						</div>
					</td>
				</tr>
			</tbody>
		</table>
	</div>
	<span class="alert alert-error dialogErrorMessage"
		ng-show="errorOnSubmit"> <spring:message code="request.error" />
	</span>
</div>
<!-- Adicionar Conta -->
<div id="addContaModal"
	class="modal hide fade in centering insertAndUpdateDialogsContas"
	role="dialog" aria-labelledby="addContactsModalLabel"
	aria-hidden="true">
	<div class="modal-header">
		<h3 id="addContactsModalLabel" class="displayInLine">
			<spring:message code="inserir" />
			&nbsp;
			<spring:message code="conta" />
		</h3>
	</div>
	<div class="modal-body">
		<form name="newContaForm" novalidate>
			<div style="width: 100%">
				<div class="pull-left">
					<!-- Tipo Conta -->
					<div style="float: left; width: 100%" data-ng-init="getBancos()">
						<div class="input-append">
							<label>* <spring:message code="conta.tipoConta" />:
							</label>
						</div>
						<br />
						<div data-toggle="buttons">
							<input type="radio" name="options" id="option1"
								ng-click="setTipoConta(true);"> Corrente <input
								type="radio" name="options" id="option2"
								ng-click="setTipoConta(false);"> Poupança
						</div>
					</div>
					<!-- Banco -->
					<div style="float: left; width: 100%" data-ng-init="getBancos()">
						<div class="input-append">
							<label>* <spring:message code="conta.banco" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" ng-model="conta.banco.nome" disabled></input 
							placeholder="<spring:message code='selecioneAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='conta.banco'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='usuario'/>"><select
								style="width: 3%" ng-model="conta.banco"
								ng-options="item as item.nome for item in listaBancos"
								class="form-control"></select> <br />
						</div>
					</div>
					<!-- Agência -->
					<div style="float: left">
						<div class="input-append">
							<label>* <spring:message code="conta.agencia" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" required="required" ng-model="conta.agencia"
								name="name"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='a'/>&nbsp;<spring:message code='conta.agencia'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='usuario'/> " />
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.name.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>
					<!-- Conta -->
					<div style="float: left">
						<div class="input-append">
							<label>* <spring:message code="conta.conta" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" required="required" ng-model="conta.conta"
								name="name"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='a'/>&nbsp;<spring:message code='conta.conta'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='usuario'/>" />
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.name.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>
					<!-- CPF -->
					<div style="float: left">
						<div class="input-append">
							<label><spring:message code="conta.cpf" />: </label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" ng-model="conta.cpf" name="name"
								onKeyPress="MascaraCPF(this);" maxlength="14"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='conta.cpf'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='dono'/>&nbsp;<spring:message code='da'/>&nbsp;<spring:message code='conta'/> " />
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.name.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>
					<!-- CNPJ -->
					<div style="float: left">
						<div class="input-append">
							<label><spring:message code="conta.cnpj" />: </label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" ng-model="conta.cnpj" name="name"
								class="form-control" onKeyPress="MascaraCNPJ(this);"
								maxlength="18"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='conta.cnpj'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='dono'/>&nbsp;<spring:message code='da'/>&nbsp;<spring:message code='conta'/> " />
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.name.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>
				</div>

				<div class="pull-left">
					<input type="submit" class="btn btn-inverse"
						ng-click="createConta(newContactForm);"
						value='<spring:message code="inserir"/>' />
					<button class="btn btn-inverse" data-dismiss="modal"
						ng-click="exit('#addContaModal');" aria-hidden="true">
						<spring:message code="cancel" />
					</button>
				</div>
			</div>
		</form>
	</div>
	<span class="alert alert-error dialogErrorMessage"
		ng-show="errorOnSubmit"> <spring:message code="request.error" />
	</span>
</div>
<!-- Editar Conta -->
<div id="updateContaModal"
	class="modal hide fade in centering insertAndUpdateDialogsContas"
	role="dialog" aria-labelledby="addContactsModalLabel"
	aria-hidden="true">
	<div class="modal-header">
		<h3 id="addContactsModalLabel" class="displayInLine">
			<spring:message code="edit" />
			&nbsp;
			<spring:message code="conta" />
		</h3>
	</div>
	<div class="modal-body">
		<form name="newContaForm" novalidate>
			<input type="hidden" ng-model="contact.id" name="id"
				value="{{conta.id}}" />
			<div style="width: 100%">
				<div class="pull-left">
					<!-- Tipo Conta -->
					<div style="float: left; width: 100%" data-ng-init="getBancos()">
						<div class="input-append">
							<label>* <spring:message code="conta.tipoConta" />:
							</label>
						</div>
						<br />
						<div data-toggle="buttons">
							<input type="radio" name="options" id="option1"
								ng-click="setTipoConta(true);"> Corrente <input
								type="radio" name="options" id="option2"
								ng-click="setTipoConta(false);"> Poupança
						</div>
					</div>
					<!-- Banco -->
					<div style="float: left; width: 100%" data-ng-init="getBancos()">
						<div class="input-append">
							<label>* <spring:message code="conta.banco" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" ng-model="conta.banco.nome" disabled></input 
							placeholder="<spring:message code='selecioneAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='conta.banco'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='usuario'/>"><select
								style="width: 3%" ng-model="conta.banco"
								ng-options="item as item.nome for item in listaBancos"
								class="form-control"></select> <br />
						</div>
					</div>
					<!-- Agência -->
					<div style="float: left">
						<div class="input-append">
							<label>* <spring:message code="conta.agencia" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" required="required" ng-model="conta.agencia"
								name="name"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='a'/>&nbsp;<spring:message code='conta.agencia'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='usuario'/> " />
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.name.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>
					<!-- Conta -->
					<div style="float: left">
						<div class="input-append">
							<label>* <spring:message code="conta.conta" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" required="required" ng-model="conta.conta"
								name="name"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='a'/>&nbsp;<spring:message code='conta.conta'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='usuario'/>" />
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.name.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>
					<!-- CPF -->
					<div style="float: left">
						<div class="input-append">
							<label>* <spring:message code="conta.cpf" />:
							</label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" ng-model="conta.cpf" name="name"
								onKeyPress="MascaraCPF(this);" maxlength="14"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='conta.cpf'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='dono'/>&nbsp;<spring:message code='da'/>&nbsp;<spring:message code='conta'/> " />
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.name.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>
					<!-- CNPJ -->
					<div style="float: left">
						<div class="input-append">
							<label><spring:message code="conta.cnpj" />: </label>
						</div>
						<br />
						<div class="input-append">
							<input type="text" ng-model="conta.cnpj" name="name"
								class="form-control" onKeyPress="MascaraCNPJ(this);"
								maxlength="18"
								placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='conta.cnpj'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='dono'/>&nbsp;<spring:message code='da'/>&nbsp;<spring:message code='conta'/> " />
						</div>
						<br />
						<div class="input-append">
							<label> <span class="alert alert-error"
								ng-show="displayValidationError && updateContactForm.name.$error.required">
									<spring:message code="required" />
							</span>
							</label>
						</div>
					</div>
				</div>
				<div class="pull-left">
					<input type="submit" class="btn btn-inverse"
						ng-click="updateConta(newContaForm);"
						value='<spring:message code="update"/>' />
					<button class="btn btn-inverse" data-dismiss="modal"
						ng-click="exit('#updateContaModal');" aria-hidden="true">
						<spring:message code="cancel" />
					</button>
				</div>
			</div>
		</form>
	</div>
	<span class="alert alert-error dialogErrorMessage"
		ng-show="errorOnSubmit"> <spring:message code="request.error" />
	</span>
</div>
