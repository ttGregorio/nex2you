<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<div id="addContactsModal"
	class="modal hide fade in centering insertAndUpdateDialogs"
	role="dialog" aria-labelledby="addContactsModalLabel"
	aria-hidden="true">
	<div class="modal-header">
		<h3 id="addContactsModalLabel" class="displayInLine">
			<spring:message code="create" />
			&nbsp;
			<spring:message code="produto" />
		</h3>
	</div>
	<div class="modal-body">
		<form name="newContactForm" novalidate>
			<div class="pull-left" style="width: 100%">
				<!-- Nome -->
				<div style="float: left">
					<div class="input-append">
						<label>* <spring:message code="produto.nome" />:
						</label>
					</div>
					<br />
					<div class="input-append">
						<input type="text" required="required" autofocus
							ng-model="contact.nome" name="name"
							placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='produto.nome'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='produto'/>  " />
					</div>
					<br />
					<div class="input-append">
						<label> <span class="alert alert-error"
							ng-show="displayValidationError && updateContactForm.name.$error.required">
								<spring:message code="required" />
						</span>
						</label>
					</div>
				</div>
				<!-- Categoria -->
				<div class="input-append" data-ng-init="getCategorias()">
					<div class="input-append">
						<label> <spring:message code="produto.categoria" />
						</label>
					</div>
					<br />
					<div class="input-append">
						<select ng-model="contact.categoria.id">
							<option data-ng-repeat="item in listaCategorias"
								value="{{item.id}}" ng-selected="contact.categoria.id==item.id">{{item.nome}}</option>
						</select>
					</div>
				</div>

				<!-- Estoque -->
				<div style="float: left">
					<div class="input-append">
						<label>* <spring:message code="produto.estoque" />:
						</label>
					</div>
					<br />
					<div class="input-append">
						<input type="number" required="required" autofocus
							ng-model="contact.estoque" name="name"
							placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='produto.nome'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='produto'/>  " />
					</div>
					<br />
					<div class="input-append">
						<label> <span class="alert alert-error"
							ng-show="displayValidationError && updateContactForm.name.$error.required">
								<spring:message code="required" />
						</span>
						</label>
					</div>
				</div>
				<!-- Valor de Compra -->
				<div style="float: left">
					<div class="input-append">
						<label>* <spring:message code="produto.precoCompra" />:
						</label>
					</div>
					<br />
					<div class="input-append">
						<input type="text" required="required" autofocus
							ng-model="contact.precoCompra" name="name"
							placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='produto.nome'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='produto'/>  " />
					</div>
					<br />
					<div class="input-append">
						<label> <span class="alert alert-error"
							ng-show="displayValidationError && updateContactForm.name.$error.required">
								<spring:message code="required" />
						</span>
						</label>
					</div>
				</div>
				<!-- Valor de Venda -->
				<div style="float: left">
					<div class="input-append">
						<label>* <spring:message code="produto.precoVenda" />:
						</label>
					</div>
					<br />
					<div class="input-append">
						<input type="text" required="required" autofocus
							ng-model="contact.precoVenda" name="name"
							placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='produto.nome'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='produto'/>  " />
					</div>
					<br />
					<div class="input-append">
						<label> <span class="alert alert-error"
							ng-show="displayValidationError && updateContactForm.name.$error.required">
								<spring:message code="required" />
						</span>
						</label>
					</div>
				</div>
			</div>
			<!-- Descrição -->
			<div style="float: left; width: 100%">
				<div class="input-append">
					<label>* <spring:message code="produto.descricao" />:
					</label>
				</div>
				<br />
				<div class="input-append" style="width: 95%">
					<textarea type="text" autofocus ng-model="contact.descricao"
						style="width: 100%" name="name" rows="5" required="required"
						placeholder="<spring:message code='contact'/>&nbsp;<spring:message code='sindicancia.solicitacao'/> "></textarea>
				</div>
				<br />
				<div class="input-append">
					<label> <span class="alert alert-error"
						ng-show="displayValidationError && updateContactForm.name.$error.required">
							<spring:message code="required" />
					</span>
					</label>
				</div>
			</div>

			<div style="width: 100%">
				<div class="pull-left">
					<input type="submit" class="btn btn-inverse"
						ng-click="createContact(newContactForm);"
						value='<spring:message code="create"/>' />
					<button class="btn btn-inverse" data-dismiss="modal"
						ng-click="exit('#addContactsModal');" aria-hidden="true">
						<spring:message code="cancel" />
					</button>
				</div>
			</div>
		</form>
	</div>
	<span class="alert alert-error dialogErrorMessage"
		ng-show="errorOnSubmit"> <spring:message code="request.error" />
	</span>
</div>

<div id="updateContactsModal"
	class="modal hide fade in centering insertAndUpdateDialogsBancos"
	role="dialog" aria-labelledby="updateContactsModalLabel"
	aria-hidden="true">
	<div class="modal-header">
		<h3 id="updateContactsModalLabel" class="displayInLine">
			<spring:message code="update" />
			&nbsp;
			<spring:message code="produto" />
		</h3>
	</div>
	<div class="modal-body">
		<form name="updateContactForm" novalidate>
			<input type="hidden" ng-model="contact.id" name="id"
				value="{{contact.id}}" />
			<div class="pull-left" style="width: 100%">
				<!-- Nome -->
				<div style="float: left">
					<div class="input-append">
						<label>* <spring:message code="produto.nome" />:
						</label>
					</div>
					<br />
					<div class="input-append">
						<input type="text" required="required" autofocus
							ng-model="contact.nome" name="name"
							placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='produto.nome'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='produto'/>  " />
					</div>
					<br />
					<div class="input-append">
						<label> <span class="alert alert-error"
							ng-show="displayValidationError && updateContactForm.name.$error.required">
								<spring:message code="required" />
						</span>
						</label>
					</div>
				</div>
				<!-- Categoria -->
				<div class="input-append" data-ng-init="getCategorias()">
					<div class="input-append">
						<label> <spring:message code="empresa.categoria" />
						</label>
					</div>
					<br />
					<div class="input-append">
						<select ng-model="contact.categoria.id">
							<option data-ng-repeat="item in listaCategorias"
								value="{{item.id}}" ng-selected="contact.categoria.id==item.id">{{item.nome}}</option>
						</select>
					</div>
				</div>

				<!-- Estoque -->
				<div style="float: left">
					<div class="input-append">
						<label>* <spring:message code="produto.estoque" />:
						</label>
					</div>
					<br />
					<div class="input-append">
						<input type="number" required="required" autofocus
							ng-model="contact.estoque" name="name"
							placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='produto.nome'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='produto'/>  " />
					</div>
					<br />
					<div class="input-append">
						<label> <span class="alert alert-error"
							ng-show="displayValidationError && updateContactForm.name.$error.required">
								<spring:message code="required" />
						</span>
						</label>
					</div>
				</div>
				<!-- Valor de Compra -->
				<div style="float: left">
					<div class="input-append">
						<label>* <spring:message code="produto.precoCompra" />:
						</label>
					</div>
					<br />
					<div class="input-append">
						<input type="text" required="required" autofocus
							ng-model="contact.precoCompra" name="name"
							placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='produto.nome'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='produto'/>  " />
					</div>
					<br />
					<div class="input-append">
						<label> <span class="alert alert-error"
							ng-show="displayValidationError && updateContactForm.name.$error.required">
								<spring:message code="required" />
						</span>
						</label>
					</div>
				</div>
				<!-- Valor de Venda -->
				<div style="float: left">
					<div class="input-append">
						<label>* <spring:message code="produto.precoVenda" />:
						</label>
					</div>
					<br />
					<div class="input-append">
						<input type="text" required="required" autofocus
							ng-model="contact.precoVenda" name="name"
							placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='produto.nome'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='produto'/>  " />
					</div>
					<br />
					<div class="input-append">
						<label> <span class="alert alert-error"
							ng-show="displayValidationError && updateContactForm.name.$error.required">
								<spring:message code="required" />
						</span>
						</label>
					</div>
				</div>
			</div>
			<!-- Descrição -->
			<div style="float: left; width: 100%">
				<div class="input-append">
					<label>* <spring:message code="produto.descricao" />:
					</label>
				</div>
				<br />
				<div class="input-append" style="width: 95%">
					<textarea type="text" autofocus ng-model="contact.descricao"
						style="width: 100%" name="name" rows="5" required="required"
						placeholder="<spring:message code='contact'/>&nbsp;<spring:message code='sindicancia.solicitacao'/> "></textarea>
				</div>
				<br />
				<div class="input-append">
					<label> <span class="alert alert-error"
						ng-show="displayValidationError && updateContactForm.name.$error.required">
							<spring:message code="required" />
					</span>
					</label>
				</div>
			</div>
			<div style="width: 100%">
				<div class="pull-left">
					<input type="submit" class="btn btn-inverse"
						ng-click="updateContact(updateContactForm);"
						value='<spring:message code="update"/>' />
					<button class="btn btn-inverse" data-dismiss="modal"
						ng-click="exit('#updateContactsModal');" aria-hidden="true">
						<spring:message code="cancel" />
					</button>
				</div>
			</div>
		</form>
	</div>
	<span class="alert alert-error dialogErrorMessage"
		ng-show="errorOnSubmit"> <spring:message code="request.error" />
	</span>
</div>

<div id="deleteContactsModal" class="modal hide fade in centering"
	role="dialog" aria-labelledby="searchContactsModalLabel"
	aria-hidden="true">
	<div class="modal-header">
		<h3 id="deleteContactsModalLabel" class="displayInLine">
			<spring:message code="delete" />
			&nbsp;
			<spring:message code="sindicante" />
		</h3>
	</div>
	<div class="modal-body">
		<form name="deleteContactForm" novalidate>
			<p>
				<spring:message code="delete.confirm" />
				:&nbsp;{{contact.nome}}?
			</p>

			<input type="submit" class="btn btn-inverse"
				ng-click="deleteContact();" value='<spring:message code="delete"/>' />
			<button class="btn btn-inverse" data-dismiss="modal"
				ng-click="exit('#deleteContactsModal');" aria-hidden="true">
				<spring:message code="cancel" />
			</button>
		</form>
	</div>
	<span class="alert alert-error dialogErrorMessage"
		ng-show="errorOnSubmit"> <spring:message code="request.error" />
	</span> <span class="alert alert-error dialogErrorMessage"
		ng-show="errorIllegalAccess"> <spring:message
			code="request.illegal.access" />
	</span>
</div>

<div id="searchContactsModal" class="modal hide fade in centering"
	role="dialog" aria-labelledby="searchContactsModalLabel"
	aria-hidden="true">
	<div class="modal-header">
		<h3 id="searchContactsModalLabel" class="displayInLine">
			<spring:message code="search" />
		</h3>
	</div>
	<div class="modal-body">
		<form name="searchContactForm" novalidate>
			<label><spring:message code="search.for" /></label>

			<div>
				<br />
				<div class="input-append">
					<input type="text" autofocus ng-model="searchFor" name="searchFor"
						placeholder="<spring:message code='insiraAqui'/>&nbsp;<spring:message code='o'/>&nbsp;<spring:message code='produto.nome'/>&nbsp;<spring:message code='do'/>&nbsp;<spring:message code='produto'/>  " />
				</div>
				<div class="input-append displayInLine">
					<label class="displayInLine"> <span
						class="alert alert-error"
						ng-show="displayValidationError && searchContactForm.searchFor.$error.required">
							<spring:message code="required" />
					</span>
					</label>
				</div>
			</div>
			<input type="submit" class="btn btn-inverse"
				ng-click="searchContact(searchContactForm, false);"
				value='<spring:message code="search"/>' />
			<button class="btn btn-inverse" data-dismiss="modal"
				ng-click="exit('#searchContactsModal');" aria-hidden="true">
				<spring:message code="cancel" />
			</button>
		</form>
	</div>
	<span class="alert alert-error dialogErrorMessage"
		ng-show="errorOnSubmit"> <spring:message code="request.error" />
	</span>
</div>
<!-- Upload de Imagens dos Produtos -->
<div id="uploadImagem" class="modal hide fade in centering"
	role="dialog" aria-labelledby="searchContactsModalLabel"
	aria-hidden="true">
	<div class="modal-header">
		<h3 id="deleteContactsModalLabel" class="displayInLine">
			<spring:message code="imagens" />
		</h3>
	</div>
	<div class="modal-body">
		<iframe width="0" height="0" border="0" name="dummyframe"
			id="dummyframe"></iframe>
		<form method="post" enctype="multipart/form-data" action="produtoSave"
			target="dummyframe">
			Imagem: <input type="file" name="file"> <br /> <br />
			Descrição: <input type="text" name="desc" style="width: 100%" /> <br />
			<br /> <input type="submit" value="Upload" class="btn btn-inverse"
				ng-click="exit('#uploadImagem');">
		</form>
	</div>
	<br> <label>Imagens Atuais</label>
	<div ng:repeat="item in listaImagensProduto">
		<div style="width: 20%; height: 20%; float: left" align="left">
			<button class="btn btn-inverse" ng-click="deleteImage(item.id);">X</button>
			<img style="width: 80%; height: 80%"
				ng-src="data:image/JPEG;base64,{{item.imagem}}">
		</div>
	</div>
	<span class="alert alert-error dialogErrorMessage"
		ng-show="errorOnSubmit"> <spring:message code="request.error" />
	</span> <span class="alert alert-error dialogErrorMessage"
		ng-show="errorIllegalAccess"> <spring:message
			code="request.illegal.access" />
	</span>
</div>
